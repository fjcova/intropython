#coding=utf-8

import requests
import simplejson
import time


class BitfinexPublicRequester:

    BFX_BASE_URL = "https://api.bitfinex.com/v1"


    def getTickers(self, ticker):
        url = self.BFX_BASE_URL + "/pubticker/{0}".format(ticker)
        response = requests.request("GET", url)
        return simplejson.loads(response.content)


    def getStats(self, ticker):
        url = self.BFX_BASE_URL + "/stats/{0}".format(ticker)
        response = requests.request("GET", url)
        return simplejson.loads(response.content)

    def getOrderBook(self, ticker, limit_bids = 10, limit_asks = 10, group = 0):
        params = {"limit_bids": limit_bids, "limit_asks": limit_asks, "group": group}
        url = self.BFX_BASE_URL + "/book/{0}".format(ticker)
        response = requests.request("GET", url, params=params)
        return simplejson.loads(response.content)

    def getTrades(self, ticker, timestamp="", limit_trades = 100):
        params = {"limit_trades": limit_trades, "timestamp": timestamp}
        url = self.BFX_BASE_URL + "/trades/{0}".format(ticker)
        response = requests.request("GET", url, params=params)
        return simplejson.loads(response.content)

if __name__ == "__main__":
    requester = BitfinexPublicRequester()
    ticker = requester.getTickers("BTCUSD")
    print(ticker)
    stats = requester.getStats("BTCUSD")
    print(stats)
    orderbook = requester.getOrderBook("BTCUSD", 1,1)
    print(orderbook)
    current_time = time.time()
    previous_minute = current_time -60
    trades = requester.getTrades("BTCUSD", previous_minute)
    print(trades)