#coding=utf-8

'''
​ ​ - ​ ​ Inversión​ ​ de​ ​ listas
a)​ ​ Realizar​ ​ una​ ​ función​ ​ que,​ ​ dada​ ​ una​ ​ lista,​ ​ devuelva​ ​ una​ ​ nueva​ ​ lista​ ​ cuyo​ ​ contenido
sea​ ​ igual​ ​ a ​ ​ la​ ​ original​ ​ pero​ ​ invertida.​ ​ Así,​ ​ dada​ ​ la​ ​ lista​ ​ ['Di',​ ​ 'buen',​ ​ 'día',​ ​ 'a',​ ​ 'papa'],
deberá​ ​ devolver​ ​ ['papa',​ ​ 'a',​ ​ 'día',​ ​ 'buen',​ ​ 'Di'].
b)​ ​ Realizar​ ​ otra​ ​ función​ ​ que​ ​ invierta​ ​ la​ ​ lista,​ ​ pero​ ​ en​ ​ lugar​ ​ de​ ​ devolver​ ​ una​ ​ nueva,
modifique​ ​ la​ ​ lista​ ​ dada​ ​ para​ ​ invertirla,​ ​ sin​ ​ usar​ ​ listas​ ​ auxiliares.
'''



def invertirLista(una_lista):
    nueva_lista = []
    nueva_lista = list(reversed(una_lista))
    #opcion2
    #nueva_lista = sorted(una_lista, reverse=True)
    return nueva_lista


def invertirListaModificando(una_lista):
    #una_lista.sort(reverse=True)
    #for indice,valor in enumerate(una_lista[:]):
    #    una_lista[-indice-1] = valor
    i = 0
    for x in reversed(una_lista[:]):
        una_lista[i] = x
        i+=1


a = ["Di", "Buen", "día", "a", "papá", "ahora"]

a_invertida = invertirLista(a)
print(a_invertida)
print(a)
invertirListaModificando(a)
print(a)
