# coding: utf-8

import numpy as np
import pandas as pd
import sys

#                           ******************************
#                           *********** Pandas ***********
#                           ******************************


# ** Clase DataFrame **
# *********************

# En la forma más básica un objeto DataFrame se caracteriza por
#  indice, columnas con nombres y datos tabulares

# Configura una semilla para el generador de valores aleatorios

np.random.seed(1000)
# creamos un objeto ndarray con números aleatorios
raw = np.random.standard_normal((10, 3))
raw = raw.cumsum(axis=0)
# Definimos una lista con fechas como str
index = ['2017-1-31', '2017-2-28', '2017-3-31',
         '2017-4-30', '2017-5-31', '2017-6-30',
         '2017-7-31', '2017-8-31', '2017-9-30',
         '2017-10-31']
# Definimos una lista con los nombres de las columnas
columns = ['no1', 'no2', 'no3']
# Creamos un objeto DataFrame
df = pd.DataFrame(raw, index=index, columns=columns)
# Mostramos su contenido
# print(df)
# # Los objetos DataFrame tienen funciones integradas
#
print(df.head(2)["no3"].values) # Las primeras 5 filas
sys.exit()
# print(df.tail())  # Las últimas 5 filas
# print(df.index)  # El objeto indice
# print(df.columns)  # Las columnas
# #df.info()  # Información del objeto
# print(df.describe())  # Estadisticas típicas

# ** Operaciones numéricas **
# ***************************

# Similar a las operaciones sobre objetos ndarray
print(df * 2 ) # múltiplicación vectorial
print(df.std())  # desvio estandar por columna
print(df.mean()) # media por columna
print(df.mean(axis=1))  # media por indice
print(np.mean(df))  # media por función universal de numpy


# ** Seleccion de datos **
# ************************
print(df)
# Seleccionar por nombre de columna
print("print df[no2",df['no2'])  # segunda columna
# Seleccionar una fila por la posición del indice
print(df.iloc[0])  # primera fila
# Seleccionar 2 filas por la posición del indice
print(df.iloc[2:4])  # tercera y cuarta fila
# Seleccionar 2 valores de filas de una columna por indice de posición
print(df.iloc[2:4, 1])  # tercera, cuarta fila, segunda columna
# Usar la notación de punto para seleccionar una columnas
print("df.no3.iloc")
print(df.no3.iloc[3:7])  # dot look-up for column name
# Seleccionar una fila por valor del indice
print(df.loc['2017-3-31'])  # row given index value
# Seleccionar un dato por valor del indice y nombre de columna
print(df.loc['2017-5-31', 'no3'])  # single data point
# Implementar una operación aritmética vectorizada
print(df['no1'] + 3 * df['no3'])  # vectorized arithmetic operations
print(df)
print(df.stack())

# ** Operaciones Booleanas **
# ***************************

# Que valor en la columna no3 es mayor a 0.5?
print(df['no3'] > 0.5)
# Seleccionar todas las filas que cumplan con la condición anterior
print(df[df['no3'] > 0.5])
# Combinar 2 condiciones con el operador & (and); | es el operador or
print(df[(df.no3 > 0.5) & (df.no2 > -0.25)])
# Seleccionar todas las filas con valores de indice mayores que '2017-4-30' (no basamos en ordenamiento de objetos str ).
print(df[df.index > '2017-4-30'])



#                   *****************************************
#                   ********** Graficos con pandas **********
#                   *****************************************
'''
pandas está integrado con el paquete matplotlib que nos permite graficar 
los datos almacenados en los objetos DataFrame. 

Por lo general, una sola llamada a un método permite grafiar los datos.
'''
from matplotlib import pyplot

# Grafico de lineas con los datos del DataFrame (column-wise)
#df.plot(figsize=(10, 6)) # fijamos el tamaño del gráfico
#pyplot.savefig('ejercicios/graficos/ejemplo_01.png');



# ** Mejorando indices **
# ***********************

# Generamos un objeto DatetimeIndex a partir del objeto original
df.index = pd.DatetimeIndex(df.index)
# El nuevo indice ahora es del tipo datetime64
print(df.index)
# Volvemos a graficar, esta vez mejora el formato de etiqutas del eje x
#df.plot(figsize=(10, 6))
# pyplot.savefig('../../images/chA/plot_07.png');

# Los histogramas son generados también de esta manera.
# Genera un histograma por cada columna
#df.hist(figsize=(10, 6))
# pyplot.savefig('../../images/chA/plot_08.png');
#pyplot.show()


# ** Operaciones de entrada y salida **
# *************************************

# Escribir en un archivo CSV los datos del objeto DataFrame
df.to_csv('data.csv')
# Abrir el mismo archivo e imprimir el contenido linea por linea
#with open('data.csv') as f:  # abrimos el archivo
#    for line in f.readlines():  # iteramos sobre todas las lineas
#        print(line)  # imprimimos la linea

# Leer los datos almacenados en el archivo CSV en un nuevo objeto DataFrame
# Definir la primer columna para que sea el indice
# La información de fechas en el indice debe ser transformada a un objeto Timestamp
from_csv = pd.read_csv('data.csv',  # nombre de archivo
                      index_col=0,  # indice
                      parse_dates=True)  # indice de fechcas

# Mostrar la primera 5 filas del nuevo objeto
from_csv.head()
#sys.exit()
# ** Consultar datos de diferentes fuentes **
# *******************************************

from pandas_datareader import data as web
import matplotlib.pyplot as plt

data_y = web.DataReader('BYMA', data_source='google', start='2016-09-01', end='2017-10-04')
print (data_y.tail())
data_y = pd.DataFrame(data_y['Close'])

data_y.plot(title='BMA stock price Yahoo', figsize=(10,6))
plt.show()

