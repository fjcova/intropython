# coding: utf-8

# Paso 1. Importar las librerías necesarias
import pandas as pd

# Paso 2. Importar el conjunto de datos desde el archivo .tsv y
# Asignar el  contenido a una variable de nombre chipo
filename = 'chipotle.tsv'
chipo = pd.read_csv(filename, sep='\t')

# Paso 3
# Transforamr la columna item_price a tipo float
prices = [float(value[1: -1]) for value in chipo.item_price]

# Reasignar la columna con los datos limpios
chipo.item_price = prices

# Hacer comparaciones entre precios
chipo10 = chipo[chipo['item_price'] > 10.00]
chipo10.head()
print(len(chipo10))

# Paso 4. Cual es el precio de cada item?
# Imprimir el dataframe con solo 2 columnas columns item_name y item_price

# Eliminarmos los duplicados eb item_name y quantity
chipo_filtered = chipo.drop_duplicates(['item_name', 'quantity'])
# Seleccionamos solo los productos con cantidad igual a 1
chipo_one_prod = chipo_filtered[chipo_filtered.quantity == 1]
# Seleccionar solo las columnas item_name y item_price
price_per_item = chipo_one_prod[['item_name', 'item_price']]
# Ordenar los valores desde el más caro al más barato
price_per_item.sort_values(by="item_price", ascending=False)


# Paso 5. Ordenar por nombre
chipo.item_name.sort_values()
# OR
chipo.sort_values(by="item_name")

# Paso 6. Cual es la cantidad del item más costoso?
chipo.sort_values(by="item_price", ascending=False).head(1)

# Paso 7. Cuántas veces fue ordenado un Veggie Salad Bowl?
chipo_salad = chipo[chipo.item_name == "Veggie Salad Bowl"]
print(len(chipo_salad))

# Paso 8. Cuántas veces las personas ordenaron más de una Canned Soda?
chipo_drink_steak_bowl = chipo[(chipo.item_name == "Canned Soda") & (chipo.quantity > 1)]
print (len(chipo_drink_steak_bowl))