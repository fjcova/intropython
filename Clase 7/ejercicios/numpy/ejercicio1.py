# -*- coding: utf-8 -*-

# 1-a) Importar el paquete numpy con el nombre np
import numpy as np

# 1-b) Mostrar la versión y la configuración del paquete
np.__version__
np.show_config()

# 1-c) Mostrar la documentación asociada a una funcion del paquete
np.info(np.add)

# 1-d) Crear un vector de ceros con tamaño 10
Z = np.zeros(10)

# 1-e) Crear un vector de ceros de tamaño 10 y asignarle al quinto elemento el valor 1
Z = np.zeros(10)
Z[4] = 1

# 1-f) Crear un vector con valores entre 10 y 49
Z = np.arange(10,50)

# 1-g) Dar vuelta un vector(primer elemento se convierte en el último)
Z = Z[::-1]

# 1-h) Crear una matriz 3x3 con valores entre 0 y 8
Z = np.arange(9).reshape(3,3)

# 1-i) Encontrar los indices de los elementos distintos a 0 de [1,2,0,0,4,0]
nz = np.nonzero([1,2,0,0,4,0])

# 1-j) Crear una matriz 3x3 de identidad (la matriz identidad contiene 1 en la diagonal principal y 0 en el resto)
Z = np.eye(3)

# 1-k) Crear un arreglo 3x3x3 con valores aleatorios
Z = np.random.random((3,3,3))

# 1-l) Crear un arreglo 10x10 con valores aleatorios y encontrar el mínimo y el máximo
Z = np.random.random((10,10))
Zmin, Zmax = Z.min(), Z.max()
print (Zmin, Zmax)

# 1-m) Crear un vector aleatorio de 30 elementos y calcular la media
Z = np.random.random(30)
m = Z.mean()

# 1-n) Crear un arreglo 2d con 1s en el borde y 0s dentro
Z = np.ones((10,10))
Z[1:-1,1:-1] = 0

# 1-o) Crear una matriz de 5x5 con valores 1,2,3,4 justo arriba de la diagonal
Z = np.diag(1+np.arange(4),k=-1)

# 1-p) Multiplicar una matriz 5x3 por una matriz 3x2
Z = np.dot(np.ones((5,3)), np.ones((3,2)))
Z

# 1-q) Dado un arreglo de 1 dimension, negar todos los elementos que estan entre 3 y 8
Z = np.arange(11)
Z[(3 < Z) & (Z <= 8)] *= -1

# 1-r) Crear una matriz 5x5 con filas de valores entre 0 to 4
Z = np.zeros((5,5))
Z += np.arange(5)
Z

# 1-s) Crear un vector de tamaño 10 con valores entre 0 to 1, ambos excluidos
Z = np.linspace(0,1,12,endpoint=True)[1:-1]
Z

# 1-t) Crear un vector aleatorio de tamaño 10 y ordenarlo
Z = np.random.random(10)
Z.sort()
Z

# 1-u) Considera un arreglo con la forma (6,7,8), cual es el indice (x,y,z) del elemento 100?
print(np.unravel_index(100,(6,7,8)))

