#coding=utf-8

# Imports
import numpy as np
import matplotlib.pyplot as plt

# Definimos el tamaño del gráfico y la cantidad de puntos por pulgada
plt.figure(figsize=(8,6), dpi=80)

angulos = np.linspace(-np.pi,np.pi, 256, endpoint=True)
coseno, seno = np.cos(angulos), np.sin(angulos)

# Dibujo el coseno en color rojo con una linea el doble de gruesa y punteada
plt.plot(angulos, coseno, color="red", linewidth=2.0, linestyle=":")

# Dibujo el seno en color verde con una linea el doble de fina y con el patrón -.
plt.plot(angulos, seno, color="green", linewidth=3.5, linestyle="-.")

# Defino los limites del eje X
plt.xlim(-4.0,4.0)

# Defino el rango de valores a marcar en el eje X
plt.xticks([-np.pi, -np.pi/2, 0, np.pi/2, np.pi])

# Defino los limites del eje Y
plt.ylim(-1.0,1.0)

# Defino el rango de valores a marcar en el eje Y
plt.yticks([-1,0,1])

# Guardo la imagen con el grafico
plt.savefig("ejemploSenoCoseno.png",dpi=144)

plt.show()