#coding=utf-8
import numpy as np
import matplotlib.pyplot as plt

num_intervalos = 12
eje_x = np.arange(num_intervalos)
eje_y_positivo = (1 - eje_x / float(num_intervalos)) * np.random.uniform(0.5, 1.0, num_intervalos)
eje_y_negativo = (1 - eje_x / float(num_intervalos)) * np.random.uniform(0.5, 1.0, num_intervalos)

plt.axes([0.05,0.05,0.95,0.95])
plt.bar(eje_x, +eje_y_positivo, facecolor='#9999ff', edgecolor='white')
plt.bar(eje_x, -eje_y_negativo, facecolor='#ff9999', edgecolor='white')

for x,y in zip(eje_x, eje_y_positivo):
    plt.text(x, y+0.05, '%.2f' % y, ha='center', va= 'bottom')

for x,y in zip(eje_x, eje_y_negativo):
    plt.text(x, -y-0.05, '%.2f' % y, ha='center', va= 'top')

plt.xlim(-.5, num_intervalos), plt.xticks([])
plt.ylim(-1.25,+1.25), plt.yticks([])

plt.show()