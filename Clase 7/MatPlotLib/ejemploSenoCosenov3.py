#coding=utf-8

# Imports
import numpy as np
import matplotlib.pyplot as plt

# Definimos el tamaño del gráfico y la cantidad de puntos por pulgada
plt.figure(figsize=(8,6), dpi=80)



#Mostrar los ejes centrados
#obtener ejes
ejes = plt.gca()
#oculto eje de arriba y de la derecha
ejes.spines['right'].set_visible(False)
ejes.spines['top'].set_visible(False)

ejes.xaxis.set_ticks_position('bottom')
ejes.spines['bottom'].set_position(('data', 0))
ejes.yaxis.set_ticks_position('left')
ejes.spines['left'].set_position(('data', 0))

angulos = np.linspace(-np.pi,np.pi, 256, endpoint=True)
coseno, seno = np.cos(angulos), np.sin(angulos)

# Dibujo el coseno en color rojo con una linea el doble de gruesa y punteada
plt.plot(angulos, coseno, color="red", linewidth=2.0, linestyle=":", label="Coseno")

# Dibujo el seno en color verde con una linea el doble de fina y con el patrón -.
plt.plot(angulos, seno, color="green", linewidth=0.5, linestyle="-.", label="Seno")


plt.legend(loc='upper left', frameon=False)

# Defino los limites del eje X
plt.xlim(-4.0,4.0)

# Defino el rango de valores a marcar en el eje X
plt.xticks([-np.pi, -np.pi/2, 0, np.pi/2, np.pi], [r'$-\pi$', r'$-\pi/2$', r'$0$', r'$+\pi/2$', r'$+\pi$'])

# Defino los limites del eje Y
plt.ylim(-1.0,1.0)

# Defino el rango de valores a marcar en el eje Y
plt.yticks([-1,0,1], [r'$-1$', r'$0$', r'$+1$'])

# Guardo la imagen con el grafico
plt.savefig("ejemploSenoCoseno.png",dpi=72)


plt.show()