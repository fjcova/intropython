#coding=utf-8

import numpy as np
import matplotlib.pyplot as plt

tamanio = 256
eje_x = np.linspace(-np.pi, np.pi, tamanio, endpoint=True)
eje_y = np.sin(2 * eje_x)

plt.axes([0.025,0.025,0.95,0.95])

plt.plot (eje_x, eje_y + 1, color='blue', alpha=1.00)
plt.fill_between(eje_x, 1, eje_y + 1, color='blue', alpha=.25)

plt.plot (eje_x, eje_y - 1, color='blue', alpha=1.00)
plt.fill_between(eje_x, -1, eje_y - 1, (eje_y - 1) > -1, color='blue', alpha=.25)
plt.fill_between(eje_x, -1, eje_y - 1, (eje_y - 1) < -1, color='red', alpha=.25)

plt.xlim(-np.pi,np.pi)
plt.ylim(-2.5,2.5)

plt.show()