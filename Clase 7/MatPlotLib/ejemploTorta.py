#coding=utf-8

import matplotlib.pyplot as plt

labels = 'Bonds', 'Stocks', 'Futures', 'Options'
sizes = [15, 30, 45, 98]
explode = (0, 0.05, 0, 0)  # only "explode" the 2nd slice (i.e. 'Hogs')

fig1, ax1 = plt.subplots()
ax1.pie(sizes, explode=explode, labels=labels, autopct='%1.2f%%',
        shadow=True, startangle=90, colors=["red", "blue", "black", "brown"])
ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.

plt.show()