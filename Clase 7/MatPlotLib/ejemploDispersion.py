#coding=utf-8

import numpy as np
import matplotlib.pyplot as plt
import sys

tamanio_muestra = 1024
eje_x = np.random.normal(0, 1, tamanio_muestra)
eje_y = np.random.normal(0, 1, tamanio_muestra)

z = eje_x - eje_y

arco_tangente = np.arctan2(eje_y, eje_x)

a = np.array([1,3,100])
b = np.array([1,6,10])
c = a + b

plt.scatter(a, b, s=c, c=c, alpha=0.5)

plt.show()
sys.exit()
plt.axes([0.025,0.025,0.95,0.95])
#s es el tamaño de los puntos y alpha la transparencia. c determina el color e intensidad de los puntos
plt.scatter(eje_x, eje_y, s=100, c=z, alpha=0.5)

plt.xlim(-1.5,1.5), plt.xticks([])
plt.ylim(-1.5,1.5), plt.yticks([])
plt.show()