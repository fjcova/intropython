#coding=utf-8


import numpy as np
import matplotlib.pyplot as plt

angulos = np.linspace(-np.pi,np.pi, 256, endpoint=True)
coseno, seno = np.cos(angulos), np.sin(angulos)


plt.plot(angulos, coseno)
plt.plot(angulos, seno)


plt.plot([-1,-1,1,1,-1],[-1,1,1,-1,-1])

plt.show()


