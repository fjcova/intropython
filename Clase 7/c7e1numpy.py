# -*- coding: utf-8 -*-


# ** Objetos ndarray regulares **
# *******************************

# Importamos NumPy con el alias np por convención.
import numpy as np
import sys
# Creación de un arreglo a partir de una lista
a = np.array(range(24))
print(a)
print(type(a))

# Atributos del objeto ndarray

print("Dimension array", a.ndim)      # Proporciona el número de dimensiones de nuestro array.
print("Shape", a.shape)     # Devuelve la dimensión del array, es decir, una tupla de enteros indicando
            # el tamaño del array en cada dimensión.
            # Para una matriz de n filas y m columnas obtendremos (n,m).

print("Tamaño", a.size)      # Es el número total de elementos del arreglo.
print("Tipo", a.dtype)     # Es un objeto que describe el tipo de elementos del array.
print("Tamaño en bytes", a.itemsize)  # Devuelve el tamaño de un elemento del array en bytes.
print("Data", a.data)      # El buffer contiene los elementos actuales del array.


b = a.reshape(4, 6)
print(b)
c = b.reshape(12, 2)
print(c)
print("Dimension array", b.ndim)      # Proporciona el número de dimensiones de nuestro array.
print("Shape", b.shape)     # Devuelve la dimensión del array, es decir, una tupla de enteros indicando
            # el tamaño del array en cada dimensión.
            # Para una matriz de n filas y m columnas obtendremos (n,m).
print("Tamaño", b.size)      # Es el número total de elementos del arreglo.
print("Tipo", b.dtype)     # Es un objeto que describe el tipo de elementos del array.
print("Tamaño en bytes", b.itemsize)  # Devuelve el tamaño de un elemento del array en bytes.
print("Data", b.data)      # El buffer contiene los elementos actuales del array.


c = a.reshape((2, 3, 2, 2))

b = np.array(b, dtype=np.float)
print (b.dtype)

print(c.shape)
print(c.ravel().shape)    # regresa el arreglo the array aplanado
print("ravel \n", c.ravel())
d = a.reshape(6,4)
print("Reshape de d", d)  # regresa el arreglo with a modified shape
print("Traspuesto", d.T)             # regresa el arreglo transpuesto
print("Forma del Traspuesto", d.T.shape)
print("Forma de d", d.shape)


# ** Operaciones con vectores **
# ******************************

print("Operaciones con b\n" , b ,'\n', 2 * b)
print("Operaciones con b - 2\n" , b ,'\n', b ** 2)

# Es posible pasar objetos ndarray a funciones lambda o estandar de Python.

f = lambda x: x ** 2 - 2 * x + 0.5
print("Funcion lambda", f(b))


# ndarray soportan operaciones básicas y avanzadas para seleccionar elementos
print("Seleccionar", a, a[2:6])  # elemnto 3 al 6

print("Unico elemento", b, '\n', b[2,0])  # fila 3, elemento 1

print ("Conjunto", b, '\n', b[1:3, 2:4] ) # cuadrado del medio

c = np.array( [[[  0,  1,  2],               # a 3D array (two stacked 2D arrays)
                 [ 10, 12, 13]],
                [[100,101,102],
                 [110,112,113]]])
print("Tamaño de c", c.shape)
print(c[1,...])                                   # igual a c[1,:,:] o c[1]
print(c[...,2])                                   # igual a c[:,:,2]



# ** Iterando sobre arrays **
# ***************************

#

# Iterando sobre un arreglo multidimensional sobre el primer eje
print ("Iterando sobre b")
for x in b:
    print("x= ", x)
    for y in x:
        print (y)

# Si queremos iterar sobre todos los elementos del arreglo, usamos el atributo flat
print ("Iterando sobre b plano")
for y in b.flat:
     print("Y = ", y)


# ** Operaciones Booleanas **
# ***************************

# Que números son mayores a 10?

print("Mayores", b, b > 10)
e = b > 10
print(b * e)
# Devolver solo los numeros que son mayores a 10
print(b[b > 10])


# ** Métodos de ndarray y funciones Universales **
# ************************************************

# Métodos de ndarray
print(a.sum())  # suma todos los elementos
print(b.mean())  # la media de todos los elementos
print(b.mean(axis=0))  # la media sobre el primer eje
print(b.mean(axis=1))  # la media sobre el segundo eje
print(c.std())  # el desvio estandar sobre todos los elementos

# Funciones Universales
np.sum(a)  # suma todos los elementos
np.mean(b, axis=0)  # la media sobre el primer eje
np.sin(b).round(2)  # aplica la funcion seno a todos los elementos y luego redondea
np.sin(4.5)  # función seno sobre un objeto float de Python

import timeit as tm
import math

#print(tm.Timer("[np.sin(x) for x in range(100)]", "import numpy as np").repeat(1))
#print(tm.Timer("[math.sin(x) for x in range(100)]", "import math").repeat(1))
#print(tm.Timer("np.sin(np.arange(100))", "import numpy as np").repeat(1))

# Sin embargo la velocidad cuesta memoria
a = np.sin(np.arange(100000))
b = [math.sin(x) for x in range(100000)]
print(type(a))
print(type(b))
print(sys.getsizeof(a))
print(sys.getsizeof(b))


# Creación de ndarray con el constructor arange
ai = np.arange(10)
ai
ai.dtype
af = np.arange(0.5, 9.5, 0.5)  # start, end, step size
af
af.dtype
al = np.linspace(0, 10, 12)  # start, end, number of elements
al
al.dtype


# ** Numero aleatorios **
# ***********************

# Generar 10 número aleatorios de una distribución normal estandar
np.random.standard_normal(10)
# Generar 10 número aleatorios de una distribución de Poisson.
np.random.poisson(0.5, 10)
# Configurar una semilla
#np.random.seed(1000)

# Generar un objeto ndarray de 2 dimesiones con números aleatorios.
data = np.random.standard_normal((5, 1000000))

# Mostrar una parte de los números
print (data[:, :3])
print(data)
# La media de todos los valores es cercana a 0
print (data.mean())
# El desvio estandar es cercano a 1
print (data.std())
