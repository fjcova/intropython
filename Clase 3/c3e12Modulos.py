# encoding: utf-8

#from c3e11Fib import fib, fib2
import c3e11Fib as ej11

ej11.fib(100)
ej11.fib2(100)

def func1():
    print ("funcion 1 en c3e12")


print("Resultado 1 de func1")
func1()

from  modulo_prueba import func1


print("Resultado 2 de func1 importado de otro modulo")
func1()

def func1():
    print("La vuelvo a definir")

print("Resultado de func1 redefinida")
func1()

def fun1():

    print ("funcion modulo 1")

a = 0

print ("variable name = " + __name__)

if __name__ == "__main__":
    print (__name__)
    import sys
    print (sys.argv[0])


import ejercicio3_1

from ejercicios.ejercicio3_1 import tupla_ordenada

from ejercicios.ejercicio3_1 import *


