# encoding: utf-8
from functools import reduce

# Calcular unos números primos
def f(x):
    return x % 2 != 0 and x % 3 != 0


def mayor15(x):
    return x > 15

b= [1, 15, 18, 45, 14, 20]

a = filter(mayor15, b)
print("Ejemplo Filter:", list(a))


# Calcular unos cubos
def cubo(x):
    return x*x*x


def polinomio_grado_2(a, b, c, x):
    return  a*(x**2)+b*x+c


a = map(polinomio_grado_2, [2,6], range(1, 3), range(1, 3), range(1, 3))

print("Ejemplo Map 1" , list(a))

print("Ejemplo Map 2", list(map(cubo, range(1, 11))))


# Calcular la suma de los números de 1 a 10
def sumar(x,y):
    return x+y

print ("Ejemplo reduce 1", reduce(sumar, range(1, 11)))

def multiply(x,y):
    return x*y

a = list(range(1,6))

print ("Ejemplo reduce 2",reduce(multiply, range(1,6)))

# Un tercer argumento puede pasarse para indicar el valor inicial
def sum(sec):
    return reduce(sumar, sec, 5)

print (sum(range(0, 11)))

print(sum([]))



sum([])
sum([1,2,3,4])


