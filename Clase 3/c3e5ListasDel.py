# encoding: utf-8

a = [-1, 1, 66.25, 333, 333, 1234.5]
del a[0]
print (a)

# Quitar secciones de una lista
del a[2:4]
print(a)

# Vaciar la lista completa
del a[:]
print(a)

# del para eliminar variables
del a
#Si trato de usar a luego del delete voy a obtener un error
# print(a)
