# encoding: utf-8
from collections import deque

queue = deque(["Eric", "John", "Michael"])

queue.append("Terry")         # llega Terry

queue.append("Graham")        # llega Graham
queue.appendleft("Albert")

queue.popleft()               # el primero en llegar ahora se va

queue.popleft()               # el segundo en llegar ahora se va

print(list(queue))                         # el resto de la cola en órden de llegada
