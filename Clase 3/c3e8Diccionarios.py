# encoding: utf-8
import sys

a = {35167038: {}}
print (a)



tel = {"jack": 4098, 'sape': 4139}
tel['guido'] = 4127
print (tel.keys())

for key in tel.keys():
    print (tel[key])

for key,value in tel.items():
    print ("Clave:", key)
    print ("Valor: ", value)

print(tel['jack'])

del tel['sape']
tel['irv'] = 4127
print(tel)

print(tel.keys())

print('guido' in tel)

# El constructor dict()
dict([("", 4139), ('guido', 4127), ('jack', 4098)])

a = dict([(x, x ** 2) for x in (2, 4, 6)])  # use a list por comprensión
print (a)
print (a[2])

print(dict(sape=4139, guido=4127, jack=4098))

