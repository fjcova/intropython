# encoding: utf-8

import sys


canasta = ['manzana', 'naranja', 'manzana', 'pera', 'naranja', 'banana']
fruta = set(canasta)               # crea un conjunto sin repetidos
print(fruta)
print('naranja' in fruta)                 # verificación de pertenencia rápida
print('yerba' in fruta)

# veamos las operaciones para las letras únicas de dos palabras
a = set('abracadabra')
print(a)
b = set('alacazam')
print(b)
a                                  # letras únicas en a

print("a menos b")
print(a - b)                              # letras en a pero no en b
print("union")
print(a | b)                              # letras en a o en b
print("interseccion")
print(a & b)                              # letras en a y en b
print("union exclusiva")
print(a ^ b)                              # letras en a o b pero no en ambos

# No es posible repetir elementos
print (a)
a.add("a")
print (a)
a.add("z")
print (a)
