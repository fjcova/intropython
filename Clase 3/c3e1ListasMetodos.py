# encoding: utf-8

# Ejemplo de la mayoría de los métodos de lista
a = [66.25, 333, 333, 1, 1234.5]


print (a.count(333), a.count(66.25), a.count('x'))

a.insert(2, -1)
a.append(333)
print (a)

print(a.index(66.25))

a.remove(333)
print (a)

b= [884, 214, 2.3]

a.extend(b)
print(a)

a.reverse()
print(a)

a.sort(reverse=True)
print(a)

#La lista tiene que contener tipos que puedan ser ordenables entre si
#Por ejemplo no puedo ordenar cadenas con numeros
x=2
a.append(x)
print(a)

a.sort()
print(a)
