# encoding: utf-8
a = 5,
print(a)
# Empaquetado de tupla
t = 12345, 54321, 'hola!'
print(t[0])
print (t)

# Las tuplas pueden anidarse
u = t, (1, 2, 3, 4, 5)
print(u)
z = (t, 1,2,3,4,5)
print(z)
y= t + (1,2,3,4,5)
print(y)
vacia = ()
print (type(vacia))
singleton = 'hola',    # <-- notar la coma al final

print(len(vacia))
print(len(singleton))
print(singleton)

# Desempaquetado de secuencias
x, y, z = t
print (t)
print (x, y, z)