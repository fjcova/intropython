# coding=utf-8
'''
Ejercicio 3.3 - Dada una lista de números enteros, escribir una función que:
a) Devuelva una lista con todos los que sean primos.
b) Devuelva la sumatoria y el promedio de los valores.
c) Devuelva una lista con el factorial de cada uno de esos números.
(el factorial de un número es el resultado de multiplicar todos los números enteros positivos que
hay entre ese número y el 1. )
'''

# Devuelva una lista con todos los que sean primos.
def solo_primos(lista_numeros):
    primos = []
    for num in lista_numeros:
        for div in range(2, num):
            if num%div == 0:
                break
        else:
            primos.append(num)

    return primos


lista_prueba = [1,2,3,4,5,6,7,8,9,10,11]
print (solo_primos(lista_prueba))

# Devuelva la sumatoria y el promedio de los valores.
def cal_suma_prom(lista_numers):
    sum = 0.0
    for i in range(0, len(lista_numers)):
        sum = sum + lista_numers[i]
    resultado = dict()
    resultado["sumatoria"] = sum
    resultado["promedio"] =  sum / len(lista_numers)
    return resultado

print (cal_suma_prom(lista_prueba))

# Devuelva una lista con el factorial de cada uno de esos números.
def factorial(lista_numers):
    resultado = dict()

    for num in lista_numers:
        factorial = 1
        for i in range(1,num+1):
            factorial *= i
        resultado[num] = factorial

    return resultado

print (factorial(lista_prueba))