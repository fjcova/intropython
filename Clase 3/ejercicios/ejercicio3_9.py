#coding=utf-8

'''​ Funciones​ ​ que​ ​ reciben​ ​ funciones.
a)​ ​ Escribir​ ​ una​ ​ función​ ​ llamada​ ​ map,​ ​ que​ ​ reciba​ ​ una​ ​ función​ ​ y ​ ​ una​ ​ lista​ ​ y ​ ​ devuelva​ ​ la
lista​ ​ que​ ​ resulta​ ​ de​ ​ aplicar​ ​ la​ ​ función​ ​ recibida​ ​ a ​ ​ cada​ ​ uno​ ​ de​ ​ los​ ​ elementos​ ​ de​ ​ la​ ​ lista
recibida.
b)​ ​ Escribir​ ​ una​ ​ función​ ​ llamada​ ​ filter,​ ​ que​ ​ reciba​ ​ una​ ​ función​ ​ y ​ ​ una​ ​ lista​ ​ y ​ ​ devuelva​ ​ una
lista​ ​ con​ ​ los​ ​ elementos​ ​ de​ ​ la​ ​ lista​ ​ recibida​ ​ para​ ​ los​ ​ cuales​ ​ la​ ​ función​ ​ recibida​ ​ devuelve
un​ ​ valor​ ​ verdadero.'''


def map(functionToApply, myList):
    for element in myList:
        functionToApply(element)


lista = [1,23,4,5,6,7,8,234,467]

def printValue(value):
    print(value)


map(printValue, lista)
#map(1, lista)
#map(printValue, 1)


lista = [1,23,4,5,6,7,8,234,467]

def filter(funcion, lista):
    result=[]
    for v in lista:
        if funcion(v):
            result.append(v)
    print(result)

def mayorque(a):
    return a>10

filter(mayorque, lista)
