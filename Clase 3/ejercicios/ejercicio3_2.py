#coding=utf-8
'''
Campaña electoral:
a) Escribir una función que reciba una tupla con nombres, y para cada nombre imprima el mensaje Estimado , vote por mí.
b) Escribir una función que reciba una tupla con nombres, una posición de origen p y una cantidad n, e imprima el mensaje anterior para los n nombres que se encuentran a partir de la posición p.
c) Modificar las funciones anteriores para que tengan en cuenta el género del destinatario, para ello, deberán recibir una tupla de tuplas, conteniendo el nombre y el género.

'''


def imprimir_mensaje(nombres):
    for nombre in nombres:
        print ("Estimad@ {0}, vote por mí".format(nombre))


nombres = ('Juan','Maria', 'Francesca','Pedro', 'Manuel', 'José', 'Alberto', 'Pablo')

imprimir_mensaje(nombres)


def imprimir_mensaje_segun_indice(nombres, start, cantidad):
    largo = len(nombres)
    end = start + cantidad
    if(largo >= start):
        if (end > largo):
            end = largo
    else:
        end=0
    imprimir_mensaje(nombres[start:end])

print ("\nParte 2")

imprimir_mensaje_segun_indice(nombres, 1, 3)

def imprimir_mensaje_genero(nombres):
    for nombre in nombres:
        genero = 'a'
        if  (nombre[1] == 'M'):
            genero = 'o'
        print ("Estimad{0} {1}, vote por mí".format(genero, nombre[0]))



def imprimir_mensaje_segun_genero(nombres, start, cantidad):
    largo = len(nombres)
    end = start + cantidad
    if (largo >= start):
        if (end > largo):
            end = largo
    else:
        end = 0
    imprimir_mensaje_genero(nombres[start:end])

print ("\nParte 3")


nombres = (('Juan', 'M'),('Maria','F'), ('Francesca','F'),('Pedro', 'M'), ('Manuel', 'M'), ('José', 'M'), ('Alberto', 'M'), ('Pablo', 'M'))

imprimir_mensaje_segun_genero(nombres, 1, 3)