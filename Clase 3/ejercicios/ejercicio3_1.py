# coding=utf-8
'''
Ejercicio 3.1- Escribir una función que reciba una tupla de elementos e indique si se encuentran
ordenados de menor a mayor o no.
'''


def tupla_ordenada(tupla):

    for i in range(1, len(tupla)):
        if tupla[i-1] > tupla[i]:
            return False

    return True



tupla_ord = (1, 2, 3, 4, 5, 6)

print (tupla_ordenada(tupla_ord))

tupla_des = (1, 2, 15, 4, 5, 6)

print (tupla_ordenada(tupla_des))

