#coding=utf-8

'''​ ​ Dada​ ​ una​ ​ lista​ ​ de​ ​ números​ ​ enteros​ ​ y ​ ​ un​ ​ entero​ ​ k,​ ​
escribir​ ​ una​ ​ función​ ​ que:
a)​ ​ Devuelva​ ​ tres​ ​ listas,​ ​ una​ ​ con​ ​ los​ ​ menores,​
​ otra​ ​ con​ ​ los​ ​ mayores​ ​ y ​ ​ otra​ ​ con​ ​ los​ ​ iguales
a​ ​ k.
b)​ ​ Devuelva​ ​ una​ ​ lista​ ​ con​ ​ aquellos​ ​ que​ ​ son​ ​
múltiplos​ ​ de​ ​ k'''

def obtenerListas(lista, k):
    a1 = [q for q in lista if q < k]
    a2 = [q for q in lista if q > k]
    a3 = [q for q in lista if q == k]
    return a1, a2, a3

def obtenerMltiplos(lista, k):
    aa = [q for q in lista if (q % k) == 0]
    return aa


listap =  [1,2,3,4,10,20,30]
k = 10

print(obtenerListas(listap, k))
print(obtenerMltiplos(listap, k))





