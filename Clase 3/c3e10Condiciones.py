
# encoding: utf-8
import sys
cadena1, cadena2, cadena3 = '', 'Trondheim', 'Paso Hammer'
non_nulo = cadena1 or cadena3 and cadena2
print(non_nulo)

a = 'A'
b= ''
c= 'abc'
print (a or b)
print (a or c)
print (a and b)
print (a and c)




print (0 < 0)

# Comparando secuencias
print((1, 2, 5) < (1, 2, 6))

print([1, 2, 3] < [1, 2, 3])
print('rbc' < 's' < 'Tascal' < 'Tascal4')

print ( '+' < 'T'<'r' < 's' )

print(('1', 2, 3, '4') < ('1', 2, 5, '2'))

print((1, 2) < (1, 2, -1))

print((1, 2, 3) == (1.0, 2.0, 3.0))

print((1, 2, ('aa', 'ab')) < (1, 2, ('abc', 'a'), 4))
sys.exit()