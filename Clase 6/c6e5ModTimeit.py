# -- coding: utf-8 --

import timeit, sys


# Empaquetamiento y desempaquetamiento de las tuplas vs solución tradicional

a = 1
b = 2
a = b
b = a

a, b = b, a

a = '''

def factorial_1(n):
    y = 1
    for x in range(n):
        y = y*(x+1);'''

b= '''def factorial_2(n):
    if n == 1:
        return 1
    else:
        return n * factorial_2(n -1)'''



c = timeit.timeit(a, 'n=100', number=10000000)
print(c)
print (timeit.timeit(b, 'n=100', number=10000000))







import profile as p

def mult_lista(lista_a_multi):
    if len(lista_a_multi) > 1:
        return lista_a_multi[0] * mult_lista(lista_a_multi[1:])
    elif len(lista_a_multi) == 1:
        return lista_a_multi[0]
    else:
        return 0

p.run("mult_lista(range(1,500))")

