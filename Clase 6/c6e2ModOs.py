import os
import sys

print (os.getcwd())      # devuelve el directorio de trabajo actual
os.chdir(os.getcwd() + '/ejercicios') # cambia el directorio actual de trabajo
print (os.getcwd())      # devuelve el directorio de trabajo actual

#dir_path(os)

# Crear archivo
file = open("ejemplo.py", "w")
file.close()

# mostrar archivos del directorio actual
print (str(os.listdir(os.getcwd())))

# Copiar y mover archivos
import shutil

shutil.copyfile('ejemplo.py', 'ejemplo_copia.py')
print (str(os.listdir(os.getcwd())))
shutil.move('ejemplo.py', '..')
print (str(os.listdir(os.getcwd())))


# Eliminar archivos
os.remove("ejemplo_copia.py")
print (str(os.listdir(os.getcwd())))
os.chdir("..")
print (str(os.listdir(os.getcwd())))
os.remove("ejemplo.py")
print (str(os.listdir(os.getcwd())))


