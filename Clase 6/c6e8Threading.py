import logging
import threading
import time
import sys

def worker():
    logging.debug('Starting worker')
    print('Starting w')
    time.sleep(5)
    logging.debug('Exiting worker')


def my_service():
    logging.debug('Starting service')
    print('Starting s')
    time.sleep(0.3)
    logging.debug('Exiting service')


def func3(*value):
    for x in value:
        logging.debug(x)


logging.basicConfig(
    level=logging.DEBUG,
    format='[%(levelname)s] (%(threadName)-10s) %(message)s',
)

a= [1,2,3,4]
k = {"b":18}

w = threading.Thread(name='worker', target=worker)
t = threading.Thread(name='my_service', target=my_service)
w2 = threading.Thread(target=func3, args=a)  # use default name

w.start()
t.start()
w2.start()

#Rehacer en forma sincronica
#worker()
#worker()
#my_service()
t.join()
w.join(timeout=2)
print("exit")
sys.exit()