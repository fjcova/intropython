#encoding: utf-8

import math

import random as rd

# función coseno
print(math.cos(math.pi / 4.0))

# Logaritmo en base 2 de 1024
print(math.log(1024, 2))
print(math.pow(2, math.log(1024, 2)))

# Devuelve un elemento aleatorio de la secuencia
print(rd.choice(['hola', 1, [1,2]]))

# Devuelve una lista de longitud k con elementos
# únicos de la secuencia
print (rd.sample(range(100), 10))   # elección sin reemplazo

a = list(range(100))
print (sorted(rd.sample(a, 25)))
print (sorted(rd.sample(a, 25)))
print (sorted(rd.sample(a, 25)))
print (sorted(rd.sample(a, 25)))

# Devuelve un float al azar entre 0.0 y 1.0
print (rd.random())

# Devuelve un entero al azar tomado de range(6)
print(rd.randrange(0.0, 10, 2))
