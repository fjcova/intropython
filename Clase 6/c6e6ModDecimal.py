#coding:utf-8

import decimal as de
import sys

# Ver configuración
print(de.getcontext())



# Cambiar la precisión
x = de.Decimal(1/7)
y=  de.Decimal(1) / de.Decimal(7)
print(x, y)

de.getcontext().prec = 6
x = de.Decimal(1) / de.Decimal(7)
print(x)

de.getcontext().prec = 35
x = de.Decimal(1) / de.Decimal(7)
print(x)
de.getcontext().prec = 6

# cambiar redondeo
de.getcontext().rounding = de.ROUND_UP
x = de.Decimal('3.1415926535') + de.Decimal('2.7182818285')
print(x)

x = de.Decimal(1)
print(x)
x = de.Decimal(2)
print(x)


# funciones matematicas en decimales
print(de.Decimal(2).sqrt())
print(de.Decimal(1).exp())
print(de.Decimal('10').ln())
print(de.Decimal('10').log10())


# quantize() para definir número fijo de decimales.
print(de.Decimal('7.325').quantize(de.Decimal('.000'), rounding=de.ROUND_DOWN))
print(de.Decimal('7.325').quantize(de.Decimal('1.11'), rounding=de.ROUND_FLOOR))

# Constructor Decimal con distintos tipos de datos

# Cadena de caracteres
print(de.Decimal('1.414'))
# Tupla
print(de.Decimal((1, (1, 4, 1, 4), -3)))

print(de.Decimal(float('1.1')))
print(de.Decimal('1.1'))


# El operador modulo toma el signo del dividendo en lugar del divisor
print((7) % -4)
print(de.Decimal(-7) % de.Decimal(4))

# el divisior // devuelve la parte entera pero trunca en vez de redondear
print(-7 // 4)
print(de.Decimal(-7) // de.Decimal(4))

# Calcular un impuesto del 5,5% de una tarifa de 70 centavos
# da resultados distintos con punto flotante decimal y punto flotante binario.



x = de.Decimal('0.70') * de.Decimal('1.05')
x = x.quantize(de.Decimal('0.000001'), rounding=de.ROUND_DOWN)  # redondea al centavo más cercano
print (x)

print (round(.70 * 1.05, 6))         # el mismo cálculo con floats
