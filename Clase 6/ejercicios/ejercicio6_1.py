#coding:utf-8
import os

# Imprima el directorio actual de trabajo
print ("Directorio Actual: " + os.getcwd())

# Cree un nuevo directorio con el nombre “pythonTmp”
if not os.access("pythonTmp", os.F_OK):
    os.mkdir("pythonTmp")

# Cambie el directorio de trabajo al directorio creado pythonTmp
os.chdir("pythonTmp")
print ("Directorio Actual: " + os.getcwd())

print ("Archivos en el directorio de trabajo: " + str(os.listdir(os.getcwd())))

# Cree un nuevo archivo llamado test_ejecicio2.py
file = open('test_ejecicio2.py', 'w+')
print ("Archivos en el directorio de trabajo: " + str(os.listdir(os.getcwd())))

# Cerrar el archivo y eliminarlo
file.close()
os.remove("test_ejecicio2.py")

print ("Archivos en el directorio de trabajo: " + str(os.listdir(os.getcwd())))

# Salir del directorio creado
os.chdir("..")
print ("Directorio Actual: " + os.getcwd())

# Eliminar el directorio creado
os.rmdir("pythonTmp")
print ("Directorio Actual: " + os.getcwd())
