# coding: utf-8

# 5.1- Importar el módulo math y después acceder a la función sin() mediante el nombre de la biblioteca.
import math
print (math.sin(2.0))

# 5.2- Importar la función del módulo y utilizarla directamente.

from math import sin
print (sin(2.0))

# 5.3- Importar todas las funciones del módulo y utilizarlas directamente.

from math import *
print (sin(2.0))

# 5.4- Importar los módulos sys y math en una sola sentencia, podemos separarlos por comas.
import sys, math
