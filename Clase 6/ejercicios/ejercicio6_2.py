#coding=utf-8

'''
2​ ​ - ​ ​ Utilizando​ ​ la​ ​ función​ ​ randrange​ ​ del​ ​ módulo​ ​ random​ ​ ,
 ​ ​ escribir​ ​ un​ ​ programa​ ​ que
obtenga​ ​ un​ ​ número​ ​ aleatorio​ ​ secreto,​ ​ y ​ ​
luego​ ​ permita​ ​ al​ ​ usuario​ ​ ingresar​ ​ números​ ​ y ​ ​ le​ ​ indique
si​ ​ son​ ​ menores o mayores que​ ​ el​ ​ número​ ​ a ​ ​ adivinar,​ ​ hasta​ ​ que​ ​ el​ ​ usuario​ ​ ingrese​ ​ el​ ​ número
correcto.
'''


import random

aleatorio = random.randrange(0,40)

def numRandom():
    condicion = True

    while condicion:
        a = int(input("Ingresa un numero entero: "))
        if a < aleatorio:
            print("es menor")
        elif a > aleatorio:
            print("es mayor")
        else:
            print("es igual")
            condicion = False

numRandom()



