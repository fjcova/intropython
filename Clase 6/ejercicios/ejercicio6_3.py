#coding=utf-8

'''3​ ​ - ​ ​ Crear​ ​ un​ ​ módulo​ ​ que​ ​ reciba​ ​ como​ ​ argumentos​ ​ una​ ​
 lista​ ​ de​ ​ números,​ ​ utilizar​ ​ el​ ​ módulo​ ​ sys
para​ ​ leer​ ​ los​ ​ argumentos​ ​ pasados​ ​ como​ ​ parámetros.​ ​
Luego​ ​ mostrar​ ​ el​ ​ resultado​ ​ de​ ​ la
sumatoria​ ​ de​ ​ dichos​ ​ números:
Salida​ ​ esperada:
 ​ Resultado:
 ​ 1 ​ ​ + ​ ​ 2 ​ ​ + ​ ​ 3 ​ ​ + ​ ​ 4 ​ ​ + ​ ​ 5 ​ ​ = ​ ​ 15'''


import sys


sys.argv
parametros = sys.argv
suma = 0

Cantidad = len(parametros) -1
if  Cantidad < 1:
    raise Exception("No se recibieron parámetros")

for J in range(1, Cantidad + 1):
    suma += int(parametros[J])

print(suma)