# coding:utf-8
import datetime

# las fechas son fácilmente construidas y formateadas
print (datetime.datetime.today())
date = datetime.date(2017, 9, 19)
print(date)
# nos aseguramos de tener la info de localización correcta
import locale
locale.setlocale(locale.LC_ALL, locale.getdefaultlocale())

print(datetime.date.today().strftime("%m-%d-%y. %d %b %Y es %A. hoy es %d de %B."))
print(datetime.date.today().strftime("%d/%m/%Y"))


# las fechas soportan aritmética de calendario
nacimiento = datetime.datetime(1988, 5, 14, 0, 0, 0, 0)
edad = datetime.datetime.today() - nacimiento

td = datetime.timedelta(1000000)
print(td + datetime.timedelta(seconds=1000))
print (edad)
print (edad.total_seconds())
seconds = (edad.total_seconds()/365)/24/3600
print(seconds)

date = datetime.date(2019, 9, 29)
date_hoy = datetime.datetime.today().date()
print(date)
print(date_hoy)
diff = date - date_hoy
print(diff.days)
print(type(diff))
print(date.isoweekday())