#coding:utf-8

import logging, sys


logger = logging.getLogger(__name__)

# De manera predeterminada los mensajes de depuración e informativos se suprimen
logger.debug(u'Información de depuración')
logger.info(u'Mensaje informativo')
logger.warning(u'Atención: archivo de configuración %s no se encuentra', 'server.conf')
logger.error(u'Ocurrió un error')
logger.critical(u'Error crítico -- cerrando')


logger.debug('This message should go to the log file')
logger.info('So should this')
logger.warning('And this, too')

#logger = logging.getLogger(__name__)

def log():
    logging.info('funcion log')

# Mostrar la fecha en los mensajes
#logging.basicConfig(format = '%(asctime)s %(message)s',
 #                   datefmt = '%m/%d/%Y %I:%M:%S %p',
  #                  filename = "myapp.log",
   #                 level=logging.DEBUG,
    #                filemode="w")



