# coding: utf-8

import sys
import argparse

print (sys.version )# Versión de Python

# imprimir los argumentos pasados al módulo
for arg in sys.argv:
    print (arg)
    #if arg == "Error":
     #   sys.exit(1)
#else:
    #sys.exit(0)

# Nombre del encoding usado para convertir caracteres unicode
print(sys.getfilesystemencoding())

# El entero positivo más grande posible soportado por Python.
print(sys.maxsize)

# Lista de strings que indica donde se buscan los módulos
print (sys.path)

# Identificador de la plataforma del sistema
print(sys.platform)



