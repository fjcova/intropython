#coding:utf-8

import logging

# Mostrar la fecha en los mensajes
logging.basicConfig(format = '%(asctime)s, %(name)s, %(message)s',datefmt = '%m/%d/%Y %I:%M:%S %p',
                    filename = "myapp.log",
                    level=logging.DEBUG,
                    filemode="w")

logger = logging.getLogger(__name__)

logger.debug('Arranca el módulo main')

import c6e7ModLogging as c6
c6.log()
