import requests
from bs4 import BeautifulSoup
from lxml import etree

'''
#Cosas a tener en cuenta
1) Permiso de utilizacion de la información para uso comercial
2) Tiempos entre requests (para evitar ser catalogado como ataque DDos)'''



url = "http://www.bna.com.ar/Personas"
response = requests.get(url)


soup = BeautifulSoup(response.text, "html.parser")
dom = etree.HTML(response.text)

#beautifulsoup funciona como un css selector
#traigo la primera aparición de un objeto table con el atributo class cuyo valor sea table cotizacion
#Si quiero traer todos uso findAll
a = soup.find('table', attrs={'class':'table cotizacion'})

##billetes > table > tbody > tr:nth-child(1) > td:nth-child(2)
#etree me permite buscar por el xpath

''' Xpath Syntax
a / b : /  represent the hierarchical relationship in xpath. a on the left is the parent node, b on the right is the child node, and b here is the direct child of a.
a // b : Double /  represents all b nodes under  a node should be selected ( no matter it is a direct child node or not ). So we can also write above example xpath as //div//a/text().
[@] : Select html nodes by tag’s attributes. //div[@classs] : Select div node with the class attribute, //a[@x] : Select a node with the x attribute, //div[@class=”container”] : Select div node which class’s attribute value is ‘container’.
//a[contains(text(), “love”)] : Select the a tag which text content contains string ‘love’.
//a[contains(@href, “user_name”)] : Select the a tag which href attribute’s value contains ‘user_name’.
//div[contains(@y, “x”)] : Select div tag that has y attribute and y attribute’s value contains ‘x’.'''
b = dom.xpath('//*[@id="billetes"]/table/tbody/tr[1]/td[2]')
c = dom.xpath('//*[@id="billetes"]/table/tbody/tr[1]/td[3]')


#print(a)
#con css selector
#traigo todos los td dentro de a
tds = a.select('td')
compra = tds[1]
venta = tds[2]
print("Compra", compra.text)
print("Venta", venta.text)

for i in b:
    print("Compra", i.text)

for i in c:
    print("Venta", i.text)

#print(response.text)

