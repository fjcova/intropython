import requests
import simplejson


url = 'https://bullmarketbrokers.com/Information/StockPrice/GetDollarPrice'

response = requests.get(url)
dollarPrice = simplejson.loads(response.content)

print("Ask", dollarPrice["askPrice"])
print("Bid", dollarPrice["bidPrice"])