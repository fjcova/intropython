import threading
import time

import requests
import simplejson
from websocket import WebSocketApp

s = requests.Session()
USER_API="fernandojcova2501"
PASSWORD_API="nmhjvU6)"
BASE_URL ="http://api.remarkets.primary.com.ar/auth/getToken"
WS_URL="ws://api.remarkets.primary.com.ar/"


def login(s):

    #Validamos que se inicializaron los parametros
    headers = {'X-Username': USER_API, 'X-Password': PASSWORD_API}
    loginResponse = s.post(BASE_URL, headers=headers, verify=False)
    # Checkeamos si la respuesta del request fue correcta, un ok va a ser un response code 200 (OK)
    if(loginResponse.ok):
        token = loginResponse.headers['X-Auth-Token']
    else:
        print("\nRequest Error.")
    return token


token = login(s)

headers = {"X-AUTH-TOKEN": token}

ay24_ci = 0
ay24D_ci = 0
ay24_t2 = 0
ay24D_t2 = 0

def on_message(ws, msg):
    global ay24_ci, ay24D_ci, ay24_t2, ay24D_t2
    ci = False
    t2 = False
    md = simplejson.loads(msg)
    symbol = md['instrumentId']["symbol"]
    if (symbol == "MERV - XMEV - AY24 - CI"):
        ay24_ci = md["marketData"]["BI"][0]["price"]
        print("AY24 - CI", ay24_ci)
        ci = True
    elif (symbol == "MERV - XMEV - AY24D - CI"):
        ay24D_ci = md["marketData"]["OF"][0]["price"]
        print("AY24D - CI", ay24D_ci)
        ci = True
    elif (symbol == "MERV - XMEV - AY24 - 48Hs"):
        ay24_t2 = md["marketData"]["BI"][0]["price"]
        print("AY24 - 48", ay24_t2)
        t2 = True
    else:
        ay24D_t2 = md["marketData"]["OF"][0]["price"]
        print("AY24D - 48", ay24D_t2)
        t2 = True
    if(ci and ay24_ci != 0 and ay24D_ci != 0):
        print("CI: " + str(ay24_ci / ay24D_ci))
    if (t2 and ay24_t2 != 0 and ay24D_t2 != 0):
        print("48Hs: " + str(ay24_t2 / ay24D_t2))

def on_error(ws, msg):
    print("Error")
    print(msg)

def on_close(ws, msg):
    print("Closed")
    print(msg)

def on_open(ws, msg):
    print("Opened")


ws = WebSocketApp(WS_URL,on_message=on_message,on_error=on_error,on_close=on_close,
                  on_open=on_open,header=headers)

wst = threading.Thread(target=ws.run_forever, kwargs={"ping_interval":30})
wst.start()
time.sleep(2)

MSG_MDSuscriptionAy24D = '{"type":"smd","level":1, "entries":["BI"],"products":[{"symbol":"MERV - XMEV - AY24 - CI","marketId":"ROFX"}]}'
MSG_MDSuscription = '{"type":"smd","level":1, "entries":["OF"],"products":[{"symbol":"MERV - XMEV - AY24D - CI","marketId":"ROFX"}]}'


MSG_MDSuscriptionAy24D_t2 = '{"type":"smd","level":1, "entries":["BI"],"products":[{"symbol":"MERV - XMEV - AY24 - 48Hs","marketId":"ROFX"}]}'
MSG_MDSuscription_t2 = '{"type":"smd","level":1, "entries":["OF"],"products":[{"symbol":"MERV - XMEV - AY24D - 48Hs","marketId":"ROFX"}]}'


ws.send(MSG_MDSuscriptionAy24D)
ws.send(MSG_MDSuscription)
ws.send(MSG_MDSuscriptionAy24D_t2)
ws.send(MSG_MDSuscription_t2)