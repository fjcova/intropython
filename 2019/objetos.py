

class Animal:

    def __init__(self, edad, nombre):
        self.edad = edad
        self.nombre = nombre

class Vaca(Animal):

    def __init__(self, edad, nombre):

        super().__init__(edad, nombre)
        self.clasificacion = "rumiante"


    def mugir(self):

        print ("Muuuuuuuuuuuu")


if __name__ == "__main__":

    a = Vaca
    b = Animal
    c = Animal(10, "Pepito")
    d = Vaca(11, "Josefa")
    e= Vaca(5, "Ramona")
    print(type(a))
    print(type(b))
    print(type(c))
    print(type(d))


