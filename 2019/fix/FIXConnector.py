import quickfix as fix
import quickfix50sp2 as fix50sp2

class FIXConnector(fix.Application):

    orderID = 0
    execID = 0

    def __init__(self,username,password):
        super().__init__()
        self.username = username
        self.password = password

    def gen_ord_id(self):
        global orderID
        orderID+=1
        return orderID

    def onCreate(self, sessionID):
        self.sessionID = sessionID
        logging.info("Created FIX Session")
        return

    def onLogon(self, sessionID):
        logging.info("Successful login to FIX Server")
        self.sessionID = sessionID
        secListRequest = fix.Message()
        secListRequest.getHeader().setField(fix.MsgType(fix.MsgType_SecurityListRequest));
        secListRequest.setField(fix.SecurityReqID("1"))
        secListRequest.setField(fix.SubscriptionRequestType(fix.SubscriptionRequestType_SNAPSHOT_PLUS_UPDATES))
        secListRequest.setField(fix.SecurityListRequestType(fix.SecurityListRequestType_ALL_SECURITIES))
        fix.Session.sendToTarget(secListRequest, self.sessionID)
        return

    def onLogout(self, sessionID):
        print("onLogout")
        return


    def toAdmin(self, message, sessionId):
        if message.getHeader().getField(fix.MsgType()).getString() == fix.MsgType_Logon:
            message.setField(fix.Username(self.username))
            message.setField(fix.Password(self.password))
        logging.debug(message.toString())
        return

    def fromAdmin(self, message, sessionId):
        #print("fromAdmin "+message.toString())
        parsed = None
        if message.getHeader().getField(fix.MsgType()).getString() == "3":#"9":
            logging.error(message.toString())
            #logging.error("Message Error {text} tag {tag}".format(text=parsed["text"],tag=parsed["tag"]))
        if message.getHeader().getField(fix.MsgType()).getString() == fix.MsgType_OrderCancelReject:#"9":
            logging.error("*****************Cancelacion Rechazada {0}**************".format(message.toString()))
        if parsed:
            logging.debug("Received FIX Message {fix}".format(fix=message.toString()))
            logging.debug("Created JSON Message {parsed}".format(parsed=parsed))
        return


    def toApp(self, message,sessionID):
        if message.getHeader().getField(fix.MsgType()).getString() == fix.MsgType_BusinessMessageReject:
            text = message.getField(fix.Text().getField())
            logging.error("Business Error {text}".format(text=text))
        if message.getHeader().getField(fix.MsgType()).getString() == "3":#"9":
            pass
            #logging.error("Message Error {text} tag {tag}".format(text=parsed["text"],tag=parsed["tag"]))

        logging.debug("Sending Fix Message {message}".format(message=message))
        #fix.Session.sendToTarget(message, sessionID)
        return

    def fromApp(self, message, sessionID):
        logging.info(message)
        parsed = None
        if message.getHeader().getField(fix.MsgType()).getString() in [fix.MsgType_MarketDataSnapshotFullRefresh,fix.MsgType_MarketDataRequest]:#"W": #MarketDataUpdate
            pass
            #parsear mensaje de marketdata
            #enviar a la cola
        if message.getHeader().getField(fix.MsgType()).getString() == "UALT":
            pass
            #parsear mensaje de UserAccounts
        if message.getHeader().getField(fix.MsgType()).getString() == fix.MsgType_SecurityList:
            pass
            #logging.debug("noentries =" +message.getField(fix.NoRelatedSym()).getString() if message.isSetField(fix.NoRelatedSym()) else 0)
            #logging.debug(message.toString())
            #parsed = SecurityListMessage.messageToDict(message)
            #logging.debug(parsed)

        #parsear mensaje de SecurityLyst
        if message.getHeader().getField(fix.MsgType()).getString() == fix.MsgType_ExecutionReport:#"W": #MarketDataUpdate
            pass
            #parsear mensaje de SecurityLyst
        if message.getHeader().getField(fix.MsgType()).getString() == fix.MsgType_BusinessMessageReject:#"9":
            pass
            logging.error("Mensaje Rechazada {0}".format(message.toString()))
        if message.getHeader().getField(fix.MsgType()).getString() == fix.MsgType_OrderCancelReject:#"9":
            logging.error("*****************Cancelacion Rechazada {0}**************".format(message.toString()))
        if message.getHeader().getField(fix.MsgType()).getString() == "3":#"9":
            pass
        if parsed:
           logging.debug("Received FIX Message {fix}".format(fix=message.toString()))
           logging.debug("Created JSON Message {parsed}".format(parsed=parsed))
        return

    def genOrderID(self):
        self.orderID = self.orderID+1
        return repr(self.orderID)

    def genExecID(self):
        self.execID = self.execID+1
        return repr(self.execID)




if __name__=='__main__':

    import threading
    import time
    import json
    import logging
    from pathlib import Path

    logging.basicConfig(
        level=logging.DEBUG,
        format='%(asctime)s %(levelname)s (%(threadName)-10s) %(message)s',
    )

    def startSocket():
        #while True:
        initiator.start()
        while not initiator.isLoggedOn():
            logging.info("Trying to login")
            time.sleep(5)
        waitEvent.wait()

    #dir = Path(__file__).parent.parent
    username = "fcova1398"
    sessionID = fix.SessionID("FIXT.1.1",username,"ROFX")
    settings = fix.SessionSettings("Rofex-demo.cfg")
    dictionary = settings.get(sessionID)
    password = dictionary.getString("Password")
    application = FIXConnector(username,password)#,mdQueue,orQueue)
    storeFactory = fix.FileStoreFactory(settings)
    logFactory = fix.FileLogFactory(settings)
    initiator = fix.SocketInitiator(application, storeFactory, settings, logFactory)
    t = threading.Thread(name="FIXThread",target=startSocket)
    waitEvent = threading.Event()
    t.start()


