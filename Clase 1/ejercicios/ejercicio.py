#coding=utf-8
'''
Escribir​ ​ un​ ​ programa​ ​ que​ ​ le​ ​ pregunte​ ​ al​ ​ usuario​ ​ una​ ​ cantidad​ ​ de​ ​ pesos,​ ​
una​ ​ tasa​ ​ de​ ​ interés
y​ ​ un​ ​ número​ ​ de​ ​ años​ ​ y ​ ​ muestre​ ​ como​ ​ resultado​ ​ el​ ​ monto​ ​ final​ ​ a ​ ​ obtener.​ ​
La​ ​ fórmula​ ​ a ​ ​ utilizar
es:
Cn​ ​ = ​ ​ C ​ ​ × ​ ​ (1​ ​ + ​ ​ x/100) ** n
Donde​ ​ C ​ ​ es​ ​ el​ ​ capital​ ​ inicial,​ ​ x ​ ​ es​ ​ la​ ​ tasa​ ​ de​ ​ interés​ ​ y ​ ​
n ​ ​ es​ ​ el​ ​ número​ ​ de​ ​ años​ ​ a ​ ​ calcular
'''

def preguntarAlUsuario(pregunta):
    respuesta = input(pregunta)
    return float(respuesta)


def capitalFinal():
    #preguntar cantidad de pesos
    cantidadPesos = preguntarAlUsuario("Cuantos pesos desea invertir?")
    #preguntar tasa de interes
    tasaInteres = preguntarAlUsuario("Cual es la tasa de interes")
    #preguntar cantidad de años
    años = preguntarAlUsuario("Que cantidad de años?")
    #resolver la formula

    resultado = cantidadPesos * (1 + tasaInteres/100) ** años
    return resultado


print(capitalFinal())