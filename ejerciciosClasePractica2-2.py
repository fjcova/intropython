'''4​ ​ - ​ ​ Escribir​ ​ una​ ​ función​ ​ que​ ​ reciba​ ​ una​ ​ cadena​ ​ que​ ​ contiene​ ​ un​ ​ largo​ ​ número​ ​ entero​ ​ y ​ ​ devuelva
una​ ​ cadena​ ​ con​ ​ el​ ​ número​ ​ y ​ ​ las​ ​ separaciones​ ​ de​ ​ miles.​ ​ Por​ ​ ejemplo,​ ​ si​ ​ recibe
'1234567890'​ ​ , ​ ​ debe​ ​ devolver​ ​ '1.234.567.890'​ ​ .'''



def invertir(cadena):
    n = cadena
    i = 0
    for indice in range(-3,-len(cadena)-1,-3):
        if indice != -len(cadena):
            n= cadena[:indice]+"."+n[indice+i:]
            i -= 1
    print(n)

invertir("123456789")




def agregar_separador_miles(cadena):
    nueva_cadena = ""
    for i in range(-1,-len(cadena)-1, -1):
        nueva_cadena = nueva_cadena + cadena[i]
        if i %3 == 0 and i != -len(cadena):
            nueva_cadena += "."

    cadena_final = ""
    for x in reversed(nueva_cadena):
        cadena_final += x
    return cadena_final


a = agregar_separador_miles('1234567890')

print(a)

a = agregar_separador_miles('123456789')

print(a)

a = agregar_separador_miles('12345678')

print(a)