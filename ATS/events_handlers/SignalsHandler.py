# coding: utf-8

from EventHandler import EventHandler


class SignalsHandler(EventHandler):

    def __init__(self):
        self.suscriptores = []

    def handle_event(self, signal_event):
        for suscriptor in self.suscriptores:
            suscriptor.on_event(signal_event)

    def suscribir_evento(self, suscriptor):
        if suscriptor not in self.suscriptores:
            self.suscriptores.append(suscriptor)

    def desuscribir_evento(self, suscriptor):
        if suscriptor in self.suscriptores:
            self.suscriptores.remove(suscriptor)
