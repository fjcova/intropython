# coding: utf-8

from EventHandler import EventHandler
import threading

class MarketDataHandler(EventHandler):

    def __init__(self, cliente_ws):
        self.cliente_ws = cliente_ws
        self.cliente_ws.add_md_handler(self)
        self.suscriptores = dict()
        self.md_actual = dict()

    def handle_event(self, md_event):
        self.md_actual[md_event.symbol][md_event.entry] = md_event.value
        if md_event.value is not None:
            for suscriptor in self.suscriptores[md_event.symbol][md_event.entry]:
                thread = threading.Thread(target=suscriptor.on_event, args=[md_event])
                thread.start()

    def suscribir_evento(self, suscriptor, symbols, entries):
        for symbol in symbols:
            if symbol not in self.suscriptores:
                self.suscriptores[symbol] = dict()
                self.md_actual[symbol] = dict()
                for entry in entries:
                    self.suscriptores[symbol][entry] = [suscriptor]
                    self.md_actual[symbol][entry] = 0
                    self.cliente_ws.md_subscription([symbol], [entry])
            else:
                for entry in entries:
                    if entry not in self.suscriptores[symbol]:
                        self.suscriptores[symbol][entry] = [suscriptor]
                        self.md_actual[symbol][entry] = 0
                        self.cliente_ws.md_subscription([symbol], [entry])
                    elif suscriptor not in self.suscriptores[symbol][entry]:
                        self.suscriptores[symbol][entry].append(suscriptor)

    def desuscribir_evento(self, suscriptor, symbols, entries):
        for symbol in symbols:
            if symbol in self.suscriptores:
                for entry in entries:
                    if entry in self.suscriptores[symbol]:
                        if suscriptor in self.suscriptores[symbol][entry]:
                            self.suscriptores[symbol][entry].remove(suscriptor)