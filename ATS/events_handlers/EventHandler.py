# coding: utf-8
import threading

class EventHandler(object):

    def __init__(self):
        pass

    def handle_event(self, event):
        pass

    def suscribir_evento(self, suscriptor):
        pass

    def desuscribir_evento(self, suscriptor):
        pass
