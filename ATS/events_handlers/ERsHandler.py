# coding: utf-8
import threading

from EventHandler import EventHandler


class ERsHandler(EventHandler):

    def __init__(self, cliente_ws):
        self.cliente_ws = cliente_ws
        self.cliente_ws.add_or_handler(self)
        self.suscriptores = dict()

    def handle_event(self, er_event):
        for suscriptor in self.suscriptores[er_event.account]:
            thread  = threading.Thread(target=suscriptor.on_event, args=[er_event])
            thread.start()
            #suscriptor.on_event(er_event)

    def suscribir_evento(self, suscriptor, accounts):
        for account in accounts:
            if account not in self.suscriptores:
                self.suscriptores[account] = [suscriptor]
                self.cliente_ws.or_subscription([account])
            elif suscriptor not in self.suscriptores[account]:
                self.suscriptores.append(suscriptor)

    def desuscribir_evento(self, suscriptor, account):
        if suscriptor in self.suscriptores[account]:
            self.suscriptores.remove(suscriptor)
