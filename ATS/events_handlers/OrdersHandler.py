# coding: utf-8

from EventHandler import EventHandler


class OrdersHandler(EventHandler):

    def __init__(self):
        self.suscriptores = []

    def handle_event(self, order_event):
        for suscriptor in self.suscriptores:
            suscriptor.on_event(order_event)

    def suscribir_evento(self, suscriptor):
        if suscriptor not in self.suscriptores:
            self.suscriptores.append(suscriptor)

    def desuscribir_evento(self, suscriptor):
        if suscriptor in self.suscriptores:
            self.suscriptores.remove(suscriptor)
