# Creamos un Cliente WebSocket

from flask import Flask, request
from jinja2 import Template

from conector_pmy.pmy_enums import Entorno
from conector_pmy.rest_api import RestClient
from conector_pmy.websocketclient import WebSocketClient
from estrategias.mi_estrategia import MiEstrategia
from events_handlers.ERsHandler import ERsHandler
from events_handlers.MarketDataHandler import MarketDataHandler
from events_handlers.OResponseHandler import OResponsesHandler
from events_handlers.OrdersHandler import OrdersHandler
from events_handlers.SignalsHandler import SignalsHandler
from execution_handler.rofex_execution_manager import ROFEXExecutionManager
from portafolio.portafolio import Portafolio

app=Flask(__name__)

cache = {}

template_file = open("template.html")
template_content = template_file.read()
template_file.close()

@app.route("/run")
def run_estrategia():
    cache["portfolio"] = correr_estrategia()

    return "Estrategia corriendo"


@app.route("/ivan")
def hola_ivan():
    return "Hola Ivan"

@app.route("/")
def hola2():
    return "Hola mundo"


@app.route("/portfolio")
def get_cache():
    t = Template(template_content)
    portfolio = cache["portfolio"].portfolio_actual
    return t.render(portfolio=portfolio)


def shutdown_server():
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()


@app.route('/shutdown')
def shutdown():
    cache["ws"].on_error(None, "Cerrar")
    shutdown_server()
    return 'Server shutting down...'

def correr_estrategia():

    pmy_user = "fernandojcova2501"
    pmy_pass = "nmhjvU6)"
    pmy_account = "REM2501"

    restClient = RestClient(pmy_user, pmy_pass, pmy_account, Entorno.demo)
    restClient.login()
    wsClient = WebSocketClient(restClient)
    wsClient.connect()
    cache["ws"] = wsClient

    # Inicializar handlers
    md_handler = MarketDataHandler(wsClient)
    er_handler = ERsHandler(wsClient)
    signal_handler = SignalsHandler()
    order_handler = OrdersHandler()
    oresponse_handler = OResponsesHandler()

    symbols = ["MERV - XMEV - AY24 - CI", "MERV - XMEV - AY24 - 48hs"]

    portfolio = Portafolio(symbols,
                           md_handler,
                           signal_handler,
                           oresponse_handler,
                           order_handler,
                           account=pmy_account,
                           capital_inicial=100000.0,
                           cliente_rest=restClient,
                           comision=0)

    rofex_exec_manager = ROFEXExecutionManager(restClient,
                                               order_handler,
                                               er_handler,
                                               oresponse_handler,
                                               pmy_account)

    estrategia = MiEstrategia(portfolio,
                              md_handler,
                              signal_handler,
                              symbol_ci=symbols[0],
                              symbol_48=symbols[1],
                              tasa_ref=0.3)
    print(portfolio)
    return portfolio


if __name__ == "__main__":
    app.run()