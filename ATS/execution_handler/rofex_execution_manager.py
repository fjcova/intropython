# coding=utf-8

import time as t
from ATS.eventos.evento_respuesta_orden import EventoRespuestaOrden
from execution_manager import ExecutionManager
from ATS.conector_pmy.pmy_enums import Side, OrderType, OrderStatus


class ROFEXExecutionManager(ExecutionManager):
    """
    Administra la ejecución de las ordenes utilizando la API de ROFEX
    """
    def __init__(self, cliente_rest, order_handler, er_handler, oresponse_handler, account):
        """
        """
        self.orders = dict()
        self.cliente_rest = cliente_rest
        self.oresponse_handler = oresponse_handler

        order_handler.suscribir_evento(self)
        er_handler.suscribir_evento(self, [account])

    def on_event(self, event):
        if event.type == "ORDEN":
            self.enviar_orden_al_mercado(event)
        if event.type == "EREPORT":
            self.procesar_er(event)

    def enviar_orden_al_mercado(self, order_event):

        if order_event.side == "SELL":
            order_side = Side.sell
        elif order_event.side == "BUY":
            order_side = Side.buy

        if order_event.tipo_orden == "Limit":
            order_type = OrderType.limit
        elif order_event.tipo_orden == "Market":
            order_type = OrderType.market
        elif order_event.tipo_orden == "MarketToLimit":
            order_type = OrderType.market_to_limit

        respuesta = self.cliente_rest.enviar_orden(
                                        order_event.symbol,
                                        order_event.cantidad,
                                        order_type,
                                        order_side,
                                        order_event.precio,
                                        account=order_event.account)

        self.orders[respuesta["order"]["clientId"]] = order_event.id

    def procesar_er(self, er_event):
        if er_event.cl_ord_id in self.orders:
            evento = EventoRespuestaOrden(ts=t.time(),
                                          order_id=self.orders[er_event.cl_ord_id],
                                          market=er_event.market,
                                          symbol=er_event.symbol,
                                          side=er_event.side,
                                          price=er_event.price,
                                          status=er_event.status,
                                          account=er_event.account,
                                          avg_px=er_event.avg_px,
                                          last_px=er_event.last_px,
                                          last_qty=er_event.last_qty,
                                          cum_qty=er_event.cum_qty,
                                          leaves_qty=er_event.leaves_qty,
                                          order_qty=er_event.order_qty
                                          )
            self.oresponse_handler.handle_event(evento)
