# coding=utf-8

import time as t


from ATS.eventos.evento_orden import EventoOrden


class Portafolio:
    """
    """

    def __init__(self, symbols, md_handler, signal_handler,
                 oresponse_handler, order_handler, account,
                 capital_inicial, cliente_rest, comision=0):
        """
        Inicializa el portfolio.
        Parameters:
        symbols - simbolos
        start_date - fecha inicial del portfolio.
        md_inicial - diccionario con los precios de referencia iniciales.
        comision - comision por operacion.
        capital_inicial - capital inicial de la estrategia.
        """

        self.account = account
        self.symbol_list = symbols
        self.capital_inicial = capital_inicial
        self.comision = comision
        self.order_handler = order_handler
        self.cliente_rest = cliente_rest

        # Inicializamos diccionarios de ordenes
        self.all_orders = dict()

        self.signal_estrategias = dict()

        # Inicializamos ids de ordenes
        self.last_id = 0

        # Inicializo precio inicial de los instrumentos
        self.posicion_actual = dict((k, v) for k, v in [(s, 0) for s in self.symbol_list])
        self.portfolio_actual = self.construir_portfolio_actual()

        # Me suscribo para recibir el last y close de los instrumentos del portaforlio
        # (para luego calcular el valor del portafolio)
        md_handler.suscribir_evento(self, symbols, ['LA', 'CL'])
        signal_handler.suscribir_evento(self)
        oresponse_handler.suscribir_evento(self)

    def construir_portfolio_actual(self):
        """
        Construimos el portfolio actual que almacena el valor de
        nuestra cartera durante el periodo.
        """
        d = dict((k, v) for k, v in [(s, [0.0, 0.0]) for s in self.symbol_list])

        posicion = self.cliente_rest.posicion(self.account)
        total_pos_val = 0
        for position in posicion["positions"]:
            self.posicion_actual[position["tradingSymbol"]] = position["buySize"] - position["sellSize"]
            p_val = position["buySize"]*position["buyPrice"] - position["sellSize"]*position["sellPrice"]
            d[position["tradingSymbol"]] = [0, p_val]
            total_pos_val += p_val

        d["cash"] = self.capital_inicial
        d["comisiones"] = 0.0
        d["total"] = self.capital_inicial + total_pos_val
        return d

    def on_event(self, event):
        if event.type == "MARKET":
            self.update_md(event)
        elif event.type == "SEÑAL":
            self.procesar_signal(event)
        elif event.type == "RESPUESTA_ORDEN":
            self.procesar_respuesta(event)

    def update_md(self, md_event):
        # Actualizamos el valor de mercado de la posición
        # junto con el total del portfolio
        value = md_event.value["price"]
        new_market_value = self.posicion_actual[md_event.symbol] * value
        self.portfolio_actual["total"] += new_market_value - self.portfolio_actual[md_event.symbol][1]
        self.portfolio_actual[md_event.symbol][1] = new_market_value

    def procesar_signal(self, signal_event):

        if signal_event.id_estrategia not in self.signal_estrategias:
            self.signal_estrategias[signal_event.id_estrategia] = []
        self.signal_estrategias[signal_event.id_estrategia].append(signal_event)

        # Generar una orden y avisar al manejador de ordenes
        if signal_event.tipo == "LONG":
            side = "BUY"
        elif signal_event.tipo == "SHORT":
            side = "SELL"
        elif signal_event.tipo == "EXIT":
            if self.posicion_actual[signal_event.symbol] > 0:
                side = "SELL"
            elif self.posicion_actual[signal_event.symbol] < 0:
                side = "BUY"

        cantidad = signal_event.cantidad_sugerida
        order_id = self.next_order_id()
        nueva_orden = EventoOrden(ts=t.time(),
                                  order_id=order_id,
                                  symbol=signal_event.symbol,
                                  side=side,
                                  tipo_orden="Limit",
                                  cantidad=cantidad,
                                  precio=signal_event.precio,
                                  account=self.account)

        self.all_orders[order_id] = ["PENDING", nueva_orden]
        self.order_handler.handle_event(nueva_orden)

    def procesar_respuesta(self, response_event):
        if response_event.order_id in self.all_orders:
            self.all_orders[response_event.order_id][0] = response_event.status
            self.all_orders[response_event.order_id].append(response_event)
            if response_event.status == "PENDING_NEW":
                self.procesar_pending_new(response_event)
            elif response_event.status == "NEW":
                self.procesar_new(response_event)
            elif response_event.status == "PARTIALLY_FILLED":
                self.procesar_partially_filled(response_event)
            elif response_event.status == "FILLED":
                self.procesar_filled(response_event)
            elif response_event.status == "PENDING_CANCEL":
                self.procesar_pending_cancel(response_event)
            elif response_event.status == "CANCELLED":
                self.procesar_cancelled(response_event)
            elif response_event.status == "REJECTED":
                self.procesar_rejected(response_event)

    def procesar_pending_new(self, evento_pending_new):
        pass

    def procesar_new(self, evento_new):
        pass

    def procesar_partially_filled(self, evento_filled):

        self.actualizar_posicion_por_trade(evento_filled.symbol,
                                           evento_filled.side,
                                           evento_filled.last_qty)
        self.actualizar_portfolio_por_trade(evento_filled.symbol,
                                            evento_filled.side,
                                            evento_filled.last_qty,
                                            evento_filled.last_px)

    def procesar_filled(self, evento_partially_filled):

        self.actualizar_posicion_por_trade(evento_partially_filled.symbol,
                                           evento_partially_filled.side,
                                           evento_partially_filled.last_qty)
        self.actualizar_portfolio_por_trade(evento_partially_filled.symbol,
                                            evento_partially_filled.side,
                                            evento_partially_filled.last_qty,
                                            evento_partially_filled.last_px)

    def procesar_pending_cancel(self, evento_pending_cancel):
        pass

    def procesar_cancelled(self, evento_cancelled):
        pass

    def procesar_rejected(self, evento_rejected):
        print("Orden: " + evento_rejected.order_id + " Rechazada.")
        print("Symbol: " + evento_rejected.symbol + " - " + "cantidad")

    def actualizar_posicion_por_trade(self, symbol, side, cantidad):
        """
        Actualiza la posicion actual en base a los datos del trade.
        Parametros:
        side - BUY o SELL
        symbol - simbolo
        cantidad - cantidad del trade
        """

        # Verificamos si el trade fue una compra o venta
        fill_dir = 0
        if side == "BUY":
            fill_dir = 1
        elif side == "SELL":
            fill_dir = -1
        # Actualizamos la posicion con la nueva cantidad
        self.posicion_actual[symbol] += fill_dir * cantidad

    def actualizar_portfolio_por_trade(self, symbol, side, cantidad, precio):
        """
        Actualiza el portfolio actual en base a los datos del trade.
        Parametros:
        side - BUY o SELL
        symbol - simbolo
        cantiad - cantidad del trade
        precio - precio del trade
        """

        # Check whether the fill is a buy or sell
        fill_dir = 0
        if side == "BUY":
            fill_dir = 1
        elif side == "SELL":
            fill_dir = -1
        # Actualizamos el portfolio con los nuevos datos
        monto = fill_dir * precio * cantidad

        ppp = (self.posicion_actual[symbol]*self.portfolio_actual[symbol][0] + precio*cantidad)/(cantidad+self.posicion_actual[symbol])

        self.portfolio_actual[symbol][0] += ppp
        self.portfolio_actual[symbol][1] += monto

        self.portfolio_actual["comisiones"] += self.comision
        self.portfolio_actual["cash"] -= (monto + self.comision)
        p_valorizada = 0
        for s in self.portfolio_actual:
            p_valorizada += self.portfolio_actual[s][1]
        self.portfolio_actual["total"] = self.portfolio_actual["cash"] + p_valorizada - self.portfolio_actual["comisiones"]

    def next_order_id(self):
        self.last_id += 1
        return self.last_id


    def __str__(self):
        return str(self.posicion_actual)
