# coding: utf-8

from enum import Enum


class Entorno(Enum):
    demo = 1
    produccion = 2


class TimeInForce(Enum):
    day = 'Day'


class Side(Enum):
    buy = "buy"
    sell = "sell"


class OrderType(Enum):
    limit = "limit"
    market = "market"
    market_to_limit = "market_to_limit"


class OrderStatus(Enum):
    pending_new = "PENDING_NEW"
    new = "NEW"
    partially_filled = "PARTIALLY_FILLED"
    filled = "FILLED"
    pending_cancelled = "PENDING_CANCEL"
    cancelled = "CANCELLED"
    rejected = "REJECTED"
