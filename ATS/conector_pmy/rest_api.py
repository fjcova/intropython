# coding: utf-8

import requests
import simplejson
from pmy_enums import Entorno, TimeInForce, OrderType
from pmy_exceptions import PMYAPIException


class RestClient:

    # Endpoints
    endpointRestDemo = "http://api.remarkets.primary.com.ar/"
    endpointRestProd = "https://api.primary.com.ar/"
    history_endpoint = "http://h-api.primary.com.ar/MHD/Trades/{s}/{fi}/{ff}"
    historyOHLC_endpoint = "http://h-api.primary.com.ar/MHD/TradesOHLC/{s}/{fi}/{ff}/{hi}/{hf}"

    def __init__(self, user, password, account, entorno):

        self.user = user
        self.password = password
        self.account = account
        self.token = None
        self.islogin = False
        self.initialized = True
        self.marketID = "ROFX"
        self.entorno = entorno
        if entorno.name == Entorno.demo.name:
            self.activeEndpoint = RestClient.endpointRestDemo
            self.verify_https = False
        elif entorno.name == Entorno.produccion.name:
            self.activeEndpoint = RestClient.endpointRestProd
            self.verify_https = True
        else:
            self.initialized = False
            print("Entorno incorrecto")

    def api_request(self, url):
        if not self.login:
            raise PMYAPIException("Usuario no Autenticado.")
        else:
            headers = {'X-Auth-Token': self.token}
            r = requests.get(url, headers=headers, verify=self.verify_https)
            if r.status_code == 401:
                raise PMYAPIException("Token Invalido.")
            else:
                return simplejson.loads(r.content)

    def md_historica(self, symbol, fechaini, fechafin):
        url = RestClient.history_endpoint.format(s=symbol,fi=fechaini,ff=fechafin)
        return self.api_request(url)

    def posicion(self, account):
        url = self.activeEndpoint + "rest/risk/position/getPositions/" + account
        return self.api_request(url)

    def segmentos(self):
        url = self.activeEndpoint + "rest/segment/all"
        return self.api_request(url)

    def instrumentos(self):
        url = self.activeEndpoint + "rest/instruments/all"
        return self.api_request(url)

    def market_data(self, symbol, entries):
        url = self.activeEndpoint + "rest/marketdata/get?marketId={m}&symbol={s}&entries={e}".format(m=self.marketID,s=symbol,e=entries)
        return self.api_request(url)
        
    def order_status(self, cl_ord_id, propritary):
        url = self.activeEndpoint + "rest/order/id?clOrdId={c}&proprietary={p}".format(c=cl_ord_id, p=propritary)
        return self.api_request(url)

    def enviar_orden(self, ticker, cantidad, tipo_orden, side, price=0, account=None, time_in_force=TimeInForce.day):
        if not account:
            account = self.account

        if tipo_orden == OrderType.market:
            url = self.activeEndpoint + "rest/order/newSingleOrder?marketId={m}&symbol={s}&orderQty={q}&ordType={t}&side={si}&timeInForce={tf}&account={a}".format(m=self.marketID,s=ticker,q=cantidad,t=tipo_orden,si=side,tf=time_in_force,a=account)
        elif tipo_orden == OrderType.limit:
            url = self.activeEndpoint + "rest/order/newSingleOrder?marketId={m}&symbol={s}&price={p}&orderQty={q}&ordType={t}&side={si}&timeInForce={tf}&account={a}".format(m=self.marketID, s=ticker, p=price, q=cantidad, t=tipo_orden, si=side, tf=time_in_force, a=account)
        return self.api_request(url)

    def login(self):

        # Validamos que se inicializaron los parametros
        if not self.initialized:
            raise PMYAPIException("Parametros no inicializados.")

        if not self.islogin:
            url = self.activeEndpoint + "auth/getToken"
            headers = {'X-Username': self.user, 'X-Password': self.password}
            login_response = requests.post(url, headers=headers, verify=self.verify_https)

            # Checkeamos si la respuesta del request fue correcta,
            # un ok va a ser un response code 200 (OK)
            if login_response.ok:
                self.token = login_response.headers['X-Auth-Token']
                success = True
            else:
                print("Error al autenticarnos...")
                success = False
            self.islogin = True
        else:
            print("Ya estamos logueados")
            success = True

        return success
        
if __name__ == "__main__":

    # Inicializamos con usuario/password/cuenta
    _r = requests.get("http://api.remarkets.primary.com.ar/rest/instruments/all",
                      headers={'X-Auth-Token': "BP500fvNpcoi5Ydha9DQIf2cyqk6xEkl/JvFKXByAcA="},
                      verify=False)
    if _r.status_code == 401:
        print("token invalido")
    else:
        print(simplejson.loads(_r.content))

    #cliente_rest = RestClient("primary", "pass", "20", Entorno.produccion)
    #if cliente_rest.login():
    #    print cliente_rest.segmentos()

