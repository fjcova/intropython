#!/usr/bin/python
# coding=utf-8

import threading
import websocketPackage
import simplejson
import time as t

from ATS.eventos.evento_er import EventoER
from ATS.eventos.evento_md import EventoMD
from pmy_exceptions import PMYAPIException
from pmy_enums import Entorno, OrderStatus
from rest_api import RestClient
import logging

logging.basicConfig()


class WebSocketClient():

    # Endpoints WS
    wsEndpointDemo = "ws://api.remarkets.primary.com.ar/"
    wsEndpointProd = "wss://api.primary.com.ar/"

    # Mensajes WebSocket
    MSG_MDSuscription = '{{"type":"smd","level":1, "entries":[{entries}],"products":[{symbols}]}}'
    MSG_Symbol = '{{"symbol":"{symbols}","marketId":"ROFX"}}'
    MSG_Double_Quotes = '"{symbols}"'
    MSG_OSuscription = '{{"type":"os","account":{{"id":"{a}"}},"snapshotOnlyActive":{snapshot}}}'

    def __init__(self, cliente_rest=None):

        # Nos autenticamos y pedimos el token
        self.cliente_rest = cliente_rest
        self.ws_connection = None
        self.wst = None
        self.activeEndpoint = None
        self.connected = False
        self.md_handlers = []
        self.or_handlers = []
        if cliente_rest:
            if self.cliente_rest.entorno == Entorno.demo:
                self.activeEndpoint = WebSocketClient.wsEndpointDemo
            elif self.cliente_rest.entorno == Entorno.produccion:
                self.activeEndpoint = WebSocketClient.wsEndpointProd
            else:
                raise PMYAPIException("Entorno incorrecto")

    def add_md_handler(self, handler):
        self.md_handlers.append(handler)

    def add_or_handler(self, handler):
        self.or_handlers.append(handler)

    def connect(self, user=None, password=None, entorno=None):

        # Inicializamos cliente Rest si no existe
        if not self.cliente_rest:
            if entorno == Entorno.demo:
                self.activeEndpoint = WebSocketClient.wsEndpointDemo
            elif entorno == Entorno.produccion:
                self.activeEndpoint = WebSocketClient.wsEndpointProd
            else:
                raise PMYAPIException("Entorno incorrecto")

            self.cliente_rest = RestClient(user, password, "", entorno)

        if self.cliente_rest.login():
            # Si el token es válido establecemos la conexión
            headers = {'X-Auth-Token:{token}'.format(token=self.cliente_rest.token)}
            self.ws_connection = websocketPackage.WebSocketApp(self.activeEndpoint,
                                                               on_message=self.on_message,
                                                               on_error=self.on_error,
                                                               on_close=self.on_close,
                                                               on_open=self.on_open,
                                                               header=headers)

            self.wst = threading.Thread(target=self.ws_connection.run_forever,
                                            kwargs={"ping_interval": 270})
            self.wst.start()

            # Esperamos 5 seg a que la conexion se establezca
            conn_timeout = 5
            t.sleep(1)
            while not self.ws_connection.sock.connected and conn_timeout:
                t.sleep(1)
                conn_timeout -= 1
        else:
            raise PMYAPIException("Error al autenticarnos...")

    def on_message(self, ws, message):
        try:
            # Valido Mensaje entrante
            print(message)
            msg = simplejson.loads(message)
            msg_type = msg['type'].upper()

            if msg_type == 'MD':
                # Creo EventoMD por cada entry que llego
                entries = msg['marketData']
                for entry in entries:
                    evento = EventoMD(ts=t.time(),
                                      market=msg['instrumentId']['marketId'],
                                      symbol=msg['instrumentId']['symbol'],
                                      entry=entry,
                                      value=entries[entry])

                    for handler in self.md_handlers:
                        handler.handle_event(evento)

            elif msg_type == 'OR':
                # Creo EventoER
                evento = EventoER(ts=t.time(),
                                  order_id=msg['orderReport']['orderId'],
                                  cl_ord_id=msg['orderReport']['clOrdId'],
                                  proprietary=msg['orderReport']['proprietary'],
                                  account=msg['orderReport']['accountId']['id'],
                                  market=msg['orderReport']['instrumentId']['marketId'],
                                  symbol=msg['orderReport']['instrumentId']['symbol'],
                                  price=msg['orderReport']['price'],
                                  order_qty=msg['orderReport']['orderQty'],
                                  side=msg['orderReport']['side'],
                                  time_in_force=msg['orderReport']['timeInForce'],
                                  transact_time=msg['orderReport']['transactTime'],
                                  status=msg['orderReport']['status']
                                  )

                if OrderStatus.pending_new == msg['orderReport']['status']:
                    evento.ord_type = msg['orderReport']['ordType']
                elif OrderStatus.rejected == msg['orderReport']['status']:
                    evento.avg_px = msg['orderReport']['avgPx']
                    evento.cum_qty = msg['orderReport']['cumQty']
                    evento.leaves_qty=msg['orderReport']['leavesQty']
                    evento.exec_id=msg['orderReport']['execId']
                else:
                    evento.ord_type = msg['orderReport']['ordType']
                    evento.avg_px = msg['orderReport']['avgPx']
                    evento.cum_qty = msg['orderReport']['cumQty']
                    evento.leaves_qty=msg['orderReport']['leavesQty']
                    evento.exec_id=msg['orderReport']['execId']
                    evento.last_px=msg['orderReport']['lastPx']
                    evento.last_qty=msg['orderReport']['lastQty']

                for handler in self.or_handlers:
                    handler.handle_event(evento)
            elif msg_type == 'ERROR':
                print("Error: " + str(msg["description"]))
                print("Mensaje: " + str(msg["message"]))
            else:
                print("Tipo de Mensaje Recibido No soportado: " + str(msg))
        except Exception as e:
            print("Error al procesar mensaje recibido: " + str(e.message))

    def on_error(self, ws, error):
        print(error)
        self.ws_connection.close()
        print("### connection error ###")

    def on_close(self, ws):
        self.connected = False
        print("### connection closed ###")

    def on_open(self, ws):
        self.connected = True
        print("On Conection Open...")

    def md_subscription(self, symbols_list, entries_list):
        if symbols_list and entries_list:
            # Prepare Symbol Message Data
            symbols_msg = self.MSG_Symbol.format(symbols=symbols_list.pop(0))
            for symbol in symbols_list:
                symbols_msg = symbols_msg + "," + self.MSG_Symbol.format(symbols=symbol)

            # Prepare Entries Message Data
            entries_msg = self.MSG_Double_Quotes.format(symbols=entries_list.pop(0))
            for entrie in entries_list:
                entries_msg = entries_msg + "," + self.MSG_Double_Quotes.format(symbols=entrie)

            msg_enviar = self.MSG_MDSuscription.format(entries=entries_msg, symbols=symbols_msg)

            print(msg_enviar)
            self.ws_connection.send(msg_enviar)

    def or_subscription(self, accounts, snapshot='true'):
        if accounts:
            for account in accounts:
                msg_enviar = self.MSG_OSuscription.format(a=account, snapshot=snapshot)
                self.ws_connection.send(msg_enviar)


if __name__ == "__main__":

    # Creamos un Cliente WebSocket
    wsClient = WebSocketClient()
    wsClient.connect("fzanuso", "pass", Entorno.demo)

    t.sleep(2)
    if wsClient.connected:

        # Nos suscribimos para recibir MD
        s = ['DODic17']
        entries = ['BI']

        t.sleep(1)
        s = ['PAT']
        entries = ['OF']
        wsClient.md_subscription(s, entries)


