# coding=utf-8

from evento import Evento


class EventoRespuestaOrden(Evento):
    """
    """
    def __init__(self, ts, order_id, account, market, symbol, side, price, order_qty,
                 avg_px, last_px, last_qty, cum_qty, leaves_qty, status):

        self.type = "RESPUESTA_ORDEN"
        self.ts = ts
        self.order_id = order_id
        self.market = market
        self.symbol = symbol
        self.side = side
        self.price = price
        self.status = status
        self.account = account
        self.avg_px = avg_px
        self.last_px= last_px
        self.last_qty = last_qty
        self.cum_qty = cum_qty
        self.leaves_qty = leaves_qty
        self.order_qty = order_qty
