# coding=utf-8

from evento import Evento


class EventoOrden(Evento):
    """
    Handles the event of sending a Signal from a strategy object.
    This is received by a portfolio object and acted upon.
    """
    def __init__(self, ts, order_id, symbol, side, tipo_orden, cantidad, precio, account):
        """
        Inicializamos la Orden.
        """
        self.type = "ORDEN"
        self.ts = ts
        self.id = order_id
        self.symbol = symbol
        self.side = side
        self.tipo_orden = tipo_orden
        self.cantidad = cantidad
        self.precio = precio
        self.account = account


