# coding=utf-8

from evento import Evento


class EventoMD(Evento):
    """
    Handles the event of receiving a new market update with
    corresponding bars.
    """
    def __init__(self, ts, market, symbol, entry, value):
        """
        Initialises the MarketEvent.
        """
        self.type = "MARKET"
        self.ts = ts
        self.market = market
        self.symbol = symbol
        self.entry = entry
        self.value = value
