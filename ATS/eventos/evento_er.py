# coding=utf-8

from evento import Evento


class EventoER(Evento):
    """
    """
    def __init__(self, ts, order_id, cl_ord_id, proprietary,
                 account, market, symbol, price, order_qty, side,
                 time_in_force, transact_time, status, exec_id="",
                 ord_type="", avg_px="", last_px="", last_qty="", cum_qty="",
                 leaves_qty=""):

        self.type = "EREPORT"
        self.ts = ts
        self.order_id = order_id
        self.cl_ord_id = cl_ord_id
        self.proprietary = proprietary
        self.exec_id = exec_id
        self.account = account
        self.market = market
        self.symbol = symbol
        self.price = price
        self.order_qty = order_qty
        self.ord_type = ord_type
        self.side = side
        self.time_in_force = time_in_force
        self.transact_time = transact_time
        self.avg_px = avg_px
        self.last_px= last_px
        self.last_qty = last_qty
        self.cum_qty = cum_qty
        self.leaves_qty = leaves_qty
        self.status = status
