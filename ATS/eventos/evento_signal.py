# coding=utf-8

from evento import Evento


class EventoSignal(Evento):
    """
    Handles the event of sending a Signal from a strategy object.
    This is received by a portfolio object and acted upon.
    """
    def __init__(self, ts, id_estrategia, symbol, tipo, precio, cantidad_sugerida=0):
        """
        Inicializamos la Señal.
        """
        self.type = "SEÑAL"
        self.ts = ts
        self.id_estrategia = id_estrategia
        self.symbol = symbol
        self.tipo = tipo # LONG / EXIT / SHORT
        self.precio = precio
        self.cantidad_sugerida = cantidad_sugerida

