# coding=utf-8

import time as t
from ATS.eventos.evento_signal import EventoSignal
from estrategia import Estrategia


class MiEstrategia(Estrategia):
    """
    Implementa una estrategia básica Moving Average Crossover.
    Por defecto las ventanas short/long son de 10/40 periodos respectivamente.
    """

    def __init__(self, portafolio, md_handler, signal_handler, symbol_ci, symbol_48, tasa_ref):

        Estrategia.__init__(self, portafolio, md_handler, signal_handler)

        self.symbols = [symbol_ci, symbol_48]
        self.tasa_ref = tasa_ref

        # Última MD recibida
        self.last_md = {symbol_ci: None, symbol_48: None}

        # Nos suscribimos al Ask CI y al Bid 48hs
        md_handler.suscribir_evento(self, [symbol_ci], ['OF'])
        md_handler.suscribir_evento(self, [symbol_48], ['BI'])

    def calculate_signals(self, md_event):
        # Actualizo con la nueva MD
        self.last_md[md_event.symbol] = md_event.value
        if self.last_md[self.symbols[0]] and self.last_md[self.symbols[1]]:
            # Calcular Nueva Tasa
            tasa = float(self.last_md[self.symbols[1]][0]['price']) / float(self.last_md[self.symbols[0]][0]['price']) - 1
            tasa_anual = (tasa/2)*365
            if tasa_anual > self.tasa_ref:

                self.portafolio.posicion_actual[self.symbols[0]]

                cantidad = min(self.last_md[self.symbols[0]][0]['size'],
                               self.last_md[self.symbols[1]][0]['size'])

                # Creamos Señal de Compra
                senial_compra = EventoSignal(ts=t.time(),
                                             id_estrategia=id(self),
                                             symbol=self.symbols[0],
                                             tipo="LONG",
                                             precio=self.last_md[self.symbols[0]][0]['price'],
                                             cantidad_sugerida=cantidad
                                             )
                self.signal_handler.handle_event(senial_compra)

                # Creamos Señal de Venta
                senial_venta = EventoSignal(ts=t.time(),
                                            id_estrategia=id(self),
                                            symbol=self.symbols[1],
                                            tipo="SHORT",
                                            precio=self.last_md[self.symbols[1]][0]['price'],
                                            cantidad_sugerida = cantidad
                                            )
                self.signal_handler.handle_event(senial_venta)

