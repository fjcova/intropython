# coding=utf-8


class Estrategia(object):
    """
    """
    def __init__(self, portafolio, md_handler, signal_handler):

        self.portafolio = portafolio
        self.signal_handler = signal_handler
        self.md_handler = md_handler

    def calculate_signals(self, md_event):
        pass

    def on_event(self, event):
        if event.type == "MARKET":
            self.calculate_signals(event)


