#coding=utf-8
import sys

class Complejo:

    def __init__(self, real, imaginario):
        self.real = real
        self.imaginario = imaginario

    #Sobrescribo el método que convierte a cadena
    def __str__(self):
       cadena = str(self.real)
       if self.imaginario > 0:
           cadena += '+'
       cadena += (str(self.imaginario) + 'i')
       return cadena


    #metodo que se ejecuta al usar el operador <
    def __lt__(self, other):
        modulo_self = self.calcular_modulo(self)
        modulo_other = self.calcular_modulo(other)
        return modulo_self < modulo_other

    def calcular_modulo(self, complejo):
        return (complejo.real ** 2 + complejo.imaginario ** 2) ** 0.5

    #metodo que se ejecuta al usar el operador >
    def __gt__(self, other):
        modulo_self = self.calcular_modulo(self)
        modulo_other = self.calcular_modulo(other)
        return modulo_self > modulo_other

    #metodo que se ejecuta al usar el operador <=
    def __le__(self, other):
        modulo_self = self.calcular_modulo(self)
        modulo_other = self.calcular_modulo(other)
        return modulo_self <= modulo_other

    #metodo que se ejecuta al usar el operador >=
    def __ge__(self, other):
        modulo_self = self.calcular_modulo(self)
        modulo_other = self.calcular_modulo(other)
        return modulo_self >= modulo_other
    
    #metodo que se ejecuta al usar el operador ==
    def __eq__(self, other):
        return self.real == other.real and self.imaginario == other.imaginario

    #metodo que se ejecuta al usar el operador !=
    def __ne__(self, other):
        modulo_self = self.calcular_modulo()
        modulo_other = (other.real ** 2 + other.imaginario ** 2) ** 0.5
        return modulo_self != modulo_other

    #metodo que se ejecuta al usar el operador +
    def __add__(self, other):
        nuevo = Complejo(self.real + other.real, self.imaginario + other.imaginario)
        return nuevo

if __name__== "__main__":
    complex = Complejo(3, -5)
    print (complex)
    complex2 = Complejo(-8, 2)
    print (complex2)

    complex3 = Complejo(-8, 3)
    print (complex > complex2)

    print (complex == complex2)
    print (complex2 == complex3)

    print (complex + complex2)
    print (complex)


class Rectangulo:

    def __init__(self, mayor= 0, menor = 0):
        self._lado_mayor = mayor
        self._lado_menor = menor

    def area(self):
        return self._lado_mayor * self._lado_menor

    def perimetro(self):
        return 2*(self._lado_menor + self._lado_mayor)

    def __abs__(self):
        return self.area()

    def __str__(self):
        return "Lado mayor {0}, lado menor {1}, area {2}, perimetro {3}".format(self._lado_mayor, self._lado_menor, self.area(), self.perimetro())

    #sobrescribo el metodo invert, para invertir los lados (simbolo ~)
    def __invert__(self):
        menor = self._lado_menor
        self._lado_menor = self._lado_mayor
        self._lado_mayor = menor
        return self

    def __eq__(self, other):
        return (self._lado_mayor == other._lado_mayor and self._lado_menor == other._lado_menor)





if __name__== "__main__":
    rectangulo = Rectangulo(4, 6)
    rectangulo2 = Rectangulo(6,4)
    print(rectangulo == rectangulo2)
    print (rectangulo)
    print (~(rectangulo))
    print(rectangulo == rectangulo2)

    print (abs(rectangulo))