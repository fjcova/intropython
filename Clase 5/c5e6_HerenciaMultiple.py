#coding=utf-8


class MiSuperClase1():
    def __init__(self):
        print("inicializando {clase}".format(clase="MiSuperClase1"))

    def metodo_1(self):
        print ("metodo_1 metodo llamado")


class MiSuperClase2():

    def __init__(self):
        print("inicializando {clase}".format(clase="MiSuperClase2"))

    def metodo_2(self):
        print("metodo_2 metodo llamado")

    def metodo_1(self):
        print ("metodo 1 de clase 2 llamado")


# Clase que hereda las 2 clases anteriormente definidas
class MiClase(MiSuperClase2, MiSuperClase1):

    def __init__(self):
        MiSuperClase1.__init__(self)
        MiSuperClase2.__init__(self)
        print("inicializando {clase}".format(clase=self.__class__.__name__))


    def metodo_3(self):
        print("metodo_3 metodo llamado")

    def metodo_1(self):
        MiSuperClase1.metodo_1(self)



if __name__ == "__main__":
    # Se crea el objeto "c" y luego se llama al metodo_1 y metodo_2 heredados de las 2 primeras clases
    c = MiClase()
    c.metodo_1()
    c.metodo_2()
    c.metodo_3()
    print(c.__class__)