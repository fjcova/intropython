#coding=utf-8

class Animal:


    IDENTIFICADOR = "12345677"


    #método que se ejecuta al instanciar una clase
    def __init__(self):
        self.nombre = ""
      #  self.edad = 0
        self._tipo_animal = ""

    def emitir_sonido(self):
        print("Estoy por emitir un sonido")

    def poner_nombre(self, nombre):
        self.nombre = nombre

    def obtener_nombre(self):
        return self.nombre

    def poner_edad(self, edad):
        self.edad = edad

    def obtener_edad(self):
        return self.edad

    def tipo_animal(self):
        return self._tipo_animal


if __name__ == "__main__":

    animal = Animal()
    animal_2 = Animal()
    animal.poner_edad(5)
    animal.poner_nombre("Bobby")
    animal_2.poner_edad(6)
    animal_2.poner_nombre("Mick")
    print(Animal.IDENTIFICADOR)

    print(animal.obtener_edad())
    print(animal_2.obtener_edad())
    print(animal.obtener_nombre())
    print(animal_2.obtener_nombre())


