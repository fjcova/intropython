#coding=utf-8


class ConstantesMatematicas:

    PI = 3.1415
    E = 2.71828
    AVOGADRO = 6.02 * (10**23)

a = ConstantesMatematicas()
b = ConstantesMatematicas()

a.AVOGADRO = 1
b.PI = 8

print(a.AVOGADRO)
print(ConstantesMatematicas.AVOGADRO)
print(b.AVOGADRO)
print(b.PI)
print(ConstantesMatematicas.PI)
print(ConstantesMatematicas.E)
