#coding=utf-8


'''
4.​​ ​ Juego​ ​ de​ ​ Rol
a)​ ​ Escribir​ ​ una​ ​ clase​ ​ Personaje​ ​ que​ ​ contenga​ ​ los​ ​ atributos​ ​ vida,​ ​ posicion​ ​
y ​ ​ velocidad,​ ​ y ​ ​ los
métodos​ ​ recibir_ataque,​ ​ que​ ​ reduzca​ ​ la​ ​ vida​ ​ según​ ​ una​ ​ cantidad​ ​ recibida​ ​
y ​ ​ lance​ ​ una
excepción​ ​ si​ ​ la​ ​ vida​ ​ pasa​ ​ a ​ ​ ser​ ​ menor​ ​ o ​ ​ igual​ ​ que​ ​ cero,​ ​
y ​ ​ mover​ ​ que​ ​ reciba​ ​ una​ ​ dirección​ ​ y ​ ​ se
mueva​ ​ en​ ​ esa​ ​ dirección​ ​ la​ ​ cantidad​ ​ indicada​ ​ por​ ​ velocidad.
b)​ ​ Escribir​ ​ una​ ​ clase​ ​ Soldado​ ​ que​ ​ herede​ ​ de​ ​ Personaje,​ ​
y ​ ​ agregue​ ​ el​ ​ atributo​ ​ ataque​ ​ y ​ ​ el
método​ ​ atacar,​ ​ que​ ​ reciba​ ​ otro​ ​ personaje,​ ​ al​ ​ que​ ​ le​ ​ debe​ ​ hacer​ ​ el​ ​ daño​ ​ indicado​ ​ por​ ​ el​ ​ atributo
ataque.
c)​ ​ Escribir​ ​ una​ ​ clase​ ​ Campesino​ ​ que​ ​ herede​ ​ de​ ​ Personaje,​ ​
y ​ ​ agregue​ ​ el​ ​ atributo​ ​ cosecha​ ​ y ​ ​ el
método​ ​ cosechar,​ ​ que​ ​ devuelva​ ​ la​ ​ cantidad​ ​ cosechada.


'''

class Personaje:

    def __init__(self, vida, posicion, velocidad):
        self.vida = vida
        #(x,y)
        self.posicion = posicion
        self.velocidad = velocidad


    def recibir_ataque(self, daño):
        self.vida -= daño
        if self.vida <= 0:
            raise Exception("El Personaje murió, pueden vender sus activos en cuenta")


    #direccion es izq, der, arr, abajo

    def mover(self, direccion):
        if ( direccion == "arriba"):
            self.posicion = self.posicion[0], self.posicion[1] + self.velocidad
        elif ( direccion == "abajo"):
            self.posicion = self.posicion[0], self.posicion[1] - self.velocidad
        elif (direccion == "izquierda"):
            self.posicion = self.posicion[0] - self.velocidad, self.posicion[1]
        elif (direccion == "derecha"):
            self.posicion = self.posicion[0] + self.velocidad, self.posicion[1]



class Soldado(Personaje):


    def __init__(self, vida, posicion, velocidad, ataque ):
        super().__init__(vida, posicion, velocidad)
        self.ataque = ataque


    #para atacar tengo que estar a una distancia de 1 o menos
    # digo que la distancia es 1 o menos cuando esta en x o en y a 1 o menos
    #si no esta a la distancia adecuada voy a mandar un error
    def atacar(self, personaje):
        posicion_enemigo  = personaje.posicion
        distancia = abs(self.posicion[0] -posicion_enemigo[0]) + abs(self.posicion[1]- posicion_enemigo[1])
        if (distancia <= 1):
            personaje.recibir_ataque(self.ataque)
        else:
            raise Exception("No podes atacar a alguien que esta tan lejos")


class Campesino(Personaje):

    def __init__(self, vida, posicion, velocidad):
        super().__init__(vida, posicion, velocidad)
        self.cosecha = 0


    def cosechar(self):
        return self.cosecha

if __name__ == "__main__":

    p = Campesino(10, (0, 0), 8)
    s = Soldado(5, (0,0), 10, 9)
    s.atacar(p)