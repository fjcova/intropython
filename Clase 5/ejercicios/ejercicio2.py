#coding=utf-8

class Mate:


   MARCA_PREFERIDA = "Playadito"

   def __init__(self, cebadas, mate_lleno = False):
       self.cebadas=cebadas
       self.mate_lleno = mate_lleno

   def cebar(self):
       if self.mate_lleno:
           raise Exception("Cuidado! Te quemaste!")
       else:
           self.mate_lleno=True

   def beber(self):
       if not self.mate_lleno:
           raise Exception("El mate está vacío!")
       else:
           self.mate_lleno= False
           if self.cebadas >= 1:
               self.cebadas -=1
           else:
               print("Advertencia: el mate está lavado")



if __name__ == "__main__":

    m1 = Mate(3)
    m2 = Mate(8)
    m1.cebar()
    print(m1.cebadas)
    m1.beber()
    print(m1.cebadas)
    m1.cebar()
    m2.cebar()
    m2.beber()
    print(m1.MARCA_PREFERIDA)
    Mate.MARCA_PREFERIDA = "La Merced"
    m3 = Mate(9)
    print(m3.MARCA_PREFERIDA)
















