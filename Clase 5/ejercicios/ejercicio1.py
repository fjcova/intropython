#coding:utf-8
'''1.​ ​ Botella​ ​ y ​ ​ Sacacorchos
a)​ ​ Escribir​ ​ una​ ​ clase​ ​ Corcho,​ ​ que​ ​ contenga​ ​ un​ ​ atributo​ ​ bodega​ ​ (cadena​ ​ con​ ​ el​ ​ nombre​ ​ de​ ​ la
bodega).
b)​ ​ Escribir​ ​ una​ ​ clase​ ​ Botella​ ​ que​ ​ contenga​ ​ un​ ​ atributo​ ​ corcho​ ​ con​ ​ una​ ​ referencia​ ​ al​ ​ corcho​ ​ que
la​ ​ tapa,​ ​ o ​ ​ None​ ​ si​ ​ está​ ​ destapada.
c)​ ​ Escribir​ ​ una​ ​ clase​ ​ Sacacorchos​ ​ que​ ​ tenga​ ​ un​ ​ método​ ​ destapar​ ​ que​ ​ le​ ​ reciba​ ​ una​ ​ botella,​ ​ le
saque​ ​ el​ ​ corcho​ ​ y ​ ​ se​ ​ guarde​ ​ una​ ​ referencia​ ​ al​ ​ corcho​ ​ sacado.​ ​ Debe​ ​ lanzar​ ​ una​ ​ excepción​ ​ en​ ​ el
caso​ ​ en​ ​ que​ ​ la​ ​ botella​ ​ ya​ ​ esté​ ​ destapada,​ ​ o ​ ​ si​ ​ el​ ​ sacacorchos​ ​ ya​ ​ contiene​ ​ un​ ​ corcho.
d)​ ​ Agregar​ ​ un​ ​ método​ ​ limpiar,​ ​ que​ ​ saque​ ​ el​ ​ corcho​ ​ del​ ​ sacacorchos,​ ​ o ​ ​ lance​ ​ una​ ​ excepción​ ​ en
el​ ​ caso​ ​ en​ ​ el​ ​ que​ ​ no​ ​ haya​ ​ un​ ​ corcho.'''




class Corcho:


    def __init__(self, nombre_bodega="Pepe"):
        self.bodega = nombre_bodega



    def get_bodega(self):
        return self.bodega


    def set_bodega(self, nuevo_nombre):
        self.bodega = nuevo_nombre



    def __str__(self):
        return "El nombre de la bodega es {0}".format(self.bodega)



class Botella:

    def __init__(self, el_corcho = None):
        self.corcho = el_corcho


    def esta_tapada(self):
        return self.corcho is not None



class Sacacorcho:

    def __init__(self):
        self.corcho = None


    def destapar(self, botella):
        if self.corcho != None:
            raise Exception("Ya tengo un corcho, no puedo destapar mientras no me limpien")

        if botella.esta_tapada():
            self.corcho = botella.corcho
            botella.corcho = None
        else:
            raise Exception("No hay ningun corcho para sacar")


    def limpiar(self):
        if self.corcho is None:
            raise Exception("No puedo limpiar un corcho que no tengo")
        a = self.corcho
        self.corcho = None
        return a







'''




class Corcho:


    def __init__(self, bodega):
      self.bodega = bodega




class Botella:

    def __init__(self, bodega):
        self.corcho = Corcho(bodega)
        self.esta_tapada = True




class Sacacorcho:

    def destapar(self, botella):
        if(botella.esta_tapada):
            self.corcho_sacado = botella.corcho
            botella.esta_tapada = False
            botella.corcho = None
        else:
            print ("No tiene corcho!!!!!")

    def limpiar(self):
        if not self.corcho_sacado:
            print ("No hay corcho para limpiar")
        else:
            self.corcho_sacado = None
'''


corcho_lopez = Corcho("Lopez")
botella_lopez = Botella(corcho_lopez)
print (botella_lopez.corcho.bodega)
print (botella_lopez.esta_tapada())

sacacorcho = Sacacorcho()
sacacorcho.destapar(botella_lopez)
print (botella_lopez.esta_tapada())
print (sacacorcho.corcho.bodega)
#sacacorcho.destapar(botella_lopez)
corchito = sacacorcho.limpiar()
print(corchito)
#sacacorcho.limpiar()


