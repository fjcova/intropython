#coding=utf-8

'''3​ . ​ ​ Papel,​ ​ Birome,​ ​ Marcador
a)​ ​ Escribir​ ​ una​ ​ clase​ ​ Papel​ ​ que​ ​ contenga​ ​ un​ ​ texto,​ ​
un​ ​ método​ ​ escribir,​ ​ que​ ​ reciba​ ​ una​ ​ cadena
para​ ​ agregar​ ​ al​ ​ texto,​ ​ y ​ ​ el​ ​ método​ ​ __str__​ ​
que​ ​ imprima​ ​ el​ ​ contenido​ ​ del​ ​ texto.
b)​ ​ Escribir​ ​ una​ ​ clase​ ​ Birome​ ​ que​ ​ contenga​ ​ una​ ​ cantidad​ ​
de​ ​ tinta,​ ​ y ​ ​ un​ ​ método​ ​ escribir,​ ​ que
reciba​ ​ un​ ​ texto​ ​ y ​ ​ un​ ​ papel​ ​ sobre​ ​ el​ ​ cual​ ​ escribir.​ ​
Cada​ ​ letra​ ​ escrita​ ​ debe​ ​ reducir​ ​ la​ ​ cantidad​ ​ de
tinta​ ​ contenida.​ ​ Cuando​ ​ la​ ​ tinta​ ​ se​ ​ acabe,​ ​ debe​ ​ lanzar​ ​
una​ ​ excepción.
c)​ ​ Escribir​ ​ una​ ​ clase​ ​ Marcador​ ​ que​ ​ herede​ ​ de​ ​ Birome,​ ​ y ​ ​ agregue​ ​ el​ ​ método​ ​ recargar,​ ​ que
reciba​ ​ la​ ​ cantidad​ ​ de​ ​ tinta​ ​ a ​ ​ agrega'''

class Papel:

    def __init__(self, texto):
        self.texto=texto

    def escribir(self, cadena):
        self.texto+=cadena

    def __str__(self):
        return self.texto



class Birome:

    def __init__(self, tinta):
        self.tinta = tinta

    def escribir(self, texto, papel):
        papel.escribir (texto)

        self.tinta -= len(texto)

        if self.tinta <= 0:
            raise Exception("la tinta no alcanza")



class Marcador(Birome):

    def recargar(self, recarga):
        self.tinta += recarga



if __name__ == "__main__":

    try:
        p = Papel("")
        b = Birome(10)
        m = Marcador(12)
        m.recargar(10)
        m.escribir("Los hermanos sean unidos, pues esa es la ley primera", p)
        print(m.tinta)
        b.escribir("Feliz", p)
        print(p.texto)
        b.escribir(" Cumpleaños", p)
    except:
        print (p.texto)
        raise

