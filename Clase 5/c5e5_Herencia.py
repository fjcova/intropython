#coding=utf-8

from c5e1_DefinirClase import Animal
import logging

class Perro(Animal):
    logger = logging.getLogger("Perro")
    def __init__(self, raza):
        self._raza = raza
        self._tipo_animal = 'Perro'
        Perro.logger.warning("Se creo un Perro de la raza {raza}".format(raza=raza))

    def emitir_sonido(self):
        self.ladrar()

    def ladrar(self):
        print ('Guau')


class Gato(Animal):
    logger = logging.getLogger("Gato")

    def __init__(self, raza):
        self._raza = raza
        self._tipo_animal = 'Gato'
        Gato.logger.warning("Se creo un gato de la raza {raza}".format(raza=raza))

    def emitir_sonido(self):
        super().emitir_sonido()
        self.maullar()

    def maullar(self):
        print ('Miau')

if __name__== "__main__":
    # Mostrar la fecha en los mensajes
    logging.basicConfig(format='%(asctime)s, %(name)s, %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p',
                        filename="animales.log",
                        level=logging.DEBUG,
                        filemode="w")

    perro = Perro('Rott')
    gato = Gato('Siames')

    perro.poner_nombre('Athos')
    gato.poner_nombre('Agata')

    print(perro.tipo_animal())
    print(perro.obtener_nombre())
    perro.emitir_sonido()


    print(gato.tipo_animal())
    print(gato.obtener_nombre())
    gato.emitir_sonido()
