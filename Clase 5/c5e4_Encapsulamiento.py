#coding=utf-8


class MiLista:

    def __init__(self):
        #atributo privado
        self._datos = []
        self.largo = 0

    #metodo publico
    def agregar_dato(self, dato):
        self._datos.append(dato)
        self.largo += 1
        self._ordenar()

    #metodo publico
    def obtener_indice(self, indice):
        return self._datos[indice]

    #metodo privado
    def _ordenar(self):
        self._datos.sort()

    def __len__(self):
        return self.largo

    def __str__(self):
        return str(self._datos)


if __name__== "__main__":
    mi_lista = MiLista()
    print(mi_lista._datos)
    mi_lista.agregar_dato(5)
    print(mi_lista)
    mi_lista.agregar_dato(8)
    print(mi_lista)
    mi_lista.agregar_dato(1)
    print(mi_lista)
    print(len(mi_lista))

    print(mi_lista.obtener_indice(mi_lista.largo-1))