# Data Classes


class Orden():

    def __init__(self, fecha, account, side, order_type, symbol, price, size):
        self.id = id
        self.fecha = fecha
        self.account = account
        self.side = side
        self.order_type = order_type
        self.symbol = symbol
        self.size = size
        self.price = price
        self.leavesQty = size               # Cantidad Disponible para Operar


    def matches(self, otra_orden):
        pass


class OrdenCompra(Orden):

    def __init__(self, fecha, account, order_type, symbol, price, size):
        Orden.__init__(self, fecha, account, 0, order_type, symbol, price, size)


    def matches(self, otra_orden):
        if (self.side != otra_orden.side and self.symbol == otra_orden.symbol and self.account != otra_orden.account):
            return self.price >= otra_orden.price
        else:
            return False

    def __eq__(self, other):
        return  (self.price == other.price and self.fecha == other.fecha)

    def __le__(self, other):
        if (self.price == other.price):
            return self.fecha >= other.fecha
        return self.price <= other.price

    def __lt__(self, other):
        if (self.price == other.price):
            return self.fecha > other.fecha
        return self.price < other.price

    def __ge__(self, other):
        if (self.price == other.price):
            return self.fecha <= other.fecha
        return self.price >= other.price

    def __gt__(self, other):
        if (self.price == other.price):
            return self.fecha < other.fecha
        return self.price > other.price


class OrdenVenta(Orden):

    def __init__(self, fecha, account, order_type, symbol, price, size ):
        Orden.__init__(self, fecha, account, 1, order_type, symbol, price, size )


    def matches(self, otra_orden):
        if (self.side != otra_orden.side and self.symbol == otra_orden.symbol and self.account != otra_orden.account):
            return self.price >= otra_orden.price

    def __eq__(self, other):
        return  (self.price == other.price and self.fecha == other.fecha)

    def __le__(self, other):
        if (self.price == other.price):
            return self.fecha >= other.fecha
        return self.price >= other.price

    def __lt__(self, other):
        if (self.price == other.price):
            return self.fecha > other.fecha
        return self.price > other.price

    def __ge__(self, other):
        if (self.price == other.price):
            return self.fecha <= other.fecha
        return self.price <= other.price

    def __gt__(self, other):
        if (self.price == other.price):
            return self.fecha < other.fecha
        return self.price < other.price



class Trade():

    def __init__(self, timestamp, sell_account, buy_account, size, symbol, price):
        self.id = id(self)
        self.fecha = timestamp
        self.sell_account = sell_account
        self.buy_account = buy_account
        self.size = size
        self.symbol = symbol
        self.price = price

    def __str__(self):
        return str(self.id) + "," + \
                   str(self.fecha) + "," + \
                   str(self.sell_account) + "," + \
                   str(self.buy_account) + "," + \
                   str(self.size) + "," + \
                   str(self.symbol) + "," + \
                   str(self.price)


class OrderBook():

    def __init__(self):
        self.bids = []
        self.offers = []
        self.last = None

    def agregar_bid(self, orden):
        # Si el precio de la orden no esta en el book
       self.bids.append(orden)
       self.bids.sort(reverse=True)

    def agregar_offer(self, orden):
        # Si el precio de la orden no esta en el book
        self.offers.append(orden)
        self.offers.sort(reverse=True)

    def obtener_top_bid(self):
        return self.bids[0]

    def obtener_top_offer(self):
        return self.offers[0]

    def hay_match(self):
        if len(self.bids) != 0 and len(self.offers) != 0:
            top_bid = self.obtener_top_bid()
            top_offer = self.obtener_top_offer()
            return top_bid.matches(top_offer)
        return False

    def hay_bids(self):
        return len(self.bids) != 0

    def hay_offers(self):
        return len(self.offers) != 0

    def remover_ordenes_vacias(self):
        if self.obtener_top_bid().leavesQty == 0:
            self.bids.remove(self.obtener_top_bid())
        if self.obtener_top_offer().leavesQty == 0:
            self.offers.remove(self.obtener_top_offer())

