import market_object as mo
import datetime as dt

class MatchingEngine():

    def __init__(self):
        self.market_book = dict()
        self.trade_book = []

    def procesador_orden(self, orden):
        if not orden.symbol in self.market_book:
            # creamos el book para este simbolo
            self.market_book[orden.symbol] = mo.OrderBook()
        self.agregar_orden_al_book(orden)

    def agregar_orden_al_book(self, orden):
        book_symbol = self.market_book[orden.symbol]
        if orden.side == 0:
            book_symbol.agregar_bid(orden)
        else:
            book_symbol.agregar_offer(orden)
        self.generar_trades(book_symbol)


    def generar_trades(self, book):
        if book.hay_match():
            top_bid = book.obtener_top_bid()
            top_offer = book.obtener_top_offer()
            size = min(top_bid.leavesQty, top_offer.leavesQty)
            if top_bid.fecha < top_offer.fecha:
                price = max(top_bid.price, top_offer.price)
            else:
                price = min(top_bid.price, top_offer.price)
            trade  = mo.Trade(dt.datetime.now(), top_offer.account, top_bid.account, size, top_bid.symbol, price)
            book.last = price
            self.trade_book.append(trade)
            top_bid.leavesQty -= size
            top_offer.leavesQty -= size
            book.remover_ordenes_vacias()
            self.generar_trades(book)

    def registrar_trades(self):
        trade_book = open("trades.csv", 'w');
        for trade in self.trade_book:
            trade_book.write(trade.__str__() + "\n")


    def obtener_puntas_symbol(self, symbol):
        book = self.market_book[symbol]
        top_bid = None
        top_offer = None
        if book.hay_bids:
            top_bid = book.obtener_top_bid().price
        if book.hay_offers:
            top_offer = book.obtener_top_offer().price
        return top_bid, book.last, top_offer



if __name__ == '__main__':
    me = MatchingEngine()


    file = open("archivosDePrueba/prueba4.csv", "r")

    i = 0
    for line in file.readlines():
        if i == 0:
            i+=1
            continue
        if line != '\n':
            parsedLine = line.split(",")
            #id, server_ts, account, side, price, order_type, symbol, size
            ts = parsedLine[1]
            account = parsedLine[2]
            side = parsedLine[3]
            price = float(parsedLine[4])
            order_type = parsedLine[5]
            symbol = parsedLine[7]
            size = float(parsedLine[8])
            if side == '0':
                orden = mo.OrdenCompra(ts, account, order_type,symbol, price, size )
            else:
                orden = mo.OrdenVenta(ts, account, order_type,symbol, price, size )
            me.procesador_orden(orden)
   # ordenc1 = mo.OrdenCompra(dt.datetime.now(), '10',                     0, 'test_01',                     10, 1)
    #ordenc2 = mo.OrdenCompra(dt.datetime.now(), '10',                            0, 'test_01',                            10, 1)

    #ordenv1 = mo.OrdenVenta(dt.datetime.now(), '11',                      0, 'test_01',                      10, 3)

    #me.procesador_orden(ordenc1)
    #me.procesador_orden(ordenc2)
    #me.procesador_orden(ordenv1)

    me.registrar_trades()