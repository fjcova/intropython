# coding=utf-8

import os

# Esto nos permite obtener la ruta donde esta este archivo
dir = os.path.dirname(__file__)

r = open(dir + '/pruebaRead.txt', 'r')
#print (r.read())
r.seek(10,0)
print(r.read(5))




print ('\n')

# Lectura y Escritura sin sobre archivo existente
r = open(dir + '/pruebaRead.txt', 'r')

# leo la primera linea
print (r.readline())
# leo la segunda
print (r.readline())
print ('\n')
# leo las lineas por separado
r = open(dir + '/pruebaRead.txt', 'r')
print (r.readlines())
print ('\n')
# Usando un for
r = open(dir + '/pruebaRead.txt', 'r')
for linea in r:
    print (linea)

r.close()
