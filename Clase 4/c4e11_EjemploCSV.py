# coding=utf-8

import os

# Esto nos permite obtener la ruta donde esta este archivo
dir = os.path.dirname(__file__)

# Abro el archivo en modo escritura
r = open(dir + '/ejemplo.csv', 'r')

# De esta forma no agrega el \n (o fin de linea)
filas = r.read().splitlines()


nombres = []
apellidos = []
dnis = []
print (filas)
for fila in filas:
    separados = fila.split(',')
    print (separados)
    nombres.append(separados[0])
    apellidos.append(separados[1])
    dnis.append(separados[2])

print (nombres)
print (apellidos)
print (dnis)
