#coding=utf-8
'''
Modificar las funciones anteriores del ejercicio 2, para que reciban un parámetro que indique la
cantidad máxima de reemplazos o inserciones a realizar.

'''


def separar_caracteres(cadena, caracter, max_replacements):
    nueva_cadena = ""
    for x in range(len(cadena)):
        if(x != 0 and max_replacements > 0):
            nueva_cadena += caracter
            max_replacements -= 1
        nueva_cadena += cadena[x]
    return nueva_cadena

print (separar_caracteres("separar", ",", 3))
print (separar_caracteres("123456789", ",", 6))
print (separar_caracteres("8", ",", 2))

def reemplazar_espacios(cadena, caracter, max_replacements):
    nueva_cadena = ""
    for x in cadena:
        if(x == " " and max_replacements > 0):
            nueva_cadena += caracter
            max_replacements -= 1
        else:
            nueva_cadena += x
    return nueva_cadena

print (reemplazar_espacios("mi archivo de texto.txt", ",", 1))
print (reemplazar_espacios("mi archivo de texto.txt", "_", 5))
print (reemplazar_espacios("8", ",", 6))

def reemplazar_toda_cadena(cadena, caracter, max_replacements):
    nueva_cadena = ""
    for x in cadena:
        if(max_replacements > 0):
            nueva_cadena += caracter
            max_replacements -= 1
        else:
            nueva_cadena += x
    return nueva_cadena

print (reemplazar_toda_cadena('1234', 'X', 2))
print (reemplazar_toda_cadena('1234', '*', 4))
print (reemplazar_toda_cadena('XXXXXX', 'X', 6))


def agregar_en_multiplos_de_tres(cadena, caracter, max_replacements):
    nueva_cadena = ""
    for x in range(len(cadena)):
        nueva_cadena += cadena[x]
        if(x%3 == 2 and x != len(cadena) -1 and max_replacements > 0):
            nueva_cadena += caracter
            max_replacements -= 1
    return nueva_cadena

print (agregar_en_multiplos_de_tres("2552552550", '.', 1))
print (agregar_en_multiplos_de_tres("192168154124", '.', 3))
print (agregar_en_multiplos_de_tres("12", 'l', 4))