#coding=utf-8

'''
 Escribir funciones que dada una cadena y un caracter:
a) Inserte el caracter entre cada letra de la cadena. Ej: 'separar' y ',' debería devolver
's,e,p,a,r,a,r'
b) Reemplace todos los espacios por el caracter. Ej: 'mi archivo de texto.txt' y '_'
debería devolver 'mi_archivo_de_texto.txt'
c) Reemplace todos los dígitos en la cadena por el caracter. Ej: 'su clave es: 1540' y
'X' debería devolver 'su clave es: XXXX'
d) Inserte el caracter cada 3 dígitos en la cadena. Ej. '2552552550' y '.' debería devolver
'255.255.255.0'
'''



def separar_caracteres(cadena, caracter):
    nueva_cadena = ""
    for x in range(len(cadena)):
        if(x != 0):
            nueva_cadena += caracter
        nueva_cadena += cadena[x]
    return nueva_cadena

print (separar_caracteres("separar", ","))
print (separar_caracteres("123456789", ","))
print (separar_caracteres("8", ","))

def reemplazar_espacios(cadena, caracter):
    nueva_cadena = ""
    for x in cadena:
        if(x == " "):
            nueva_cadena += caracter
        else:
            nueva_cadena += x
    return nueva_cadena

print (reemplazar_espacios("mi archivo de texto.txt", ","))
print (reemplazar_espacios("mi archivo de texto.txt", "_"))
print (reemplazar_espacios("8", ","))

def reemplazar_toda_cadena(cadena, caracter):
    return caracter*len(cadena)

print (reemplazar_toda_cadena('1234', 'X'))
print (reemplazar_toda_cadena('1234', '*'))
print (reemplazar_toda_cadena('XXXXXX', 'X'))


def agregar_en_multiplos_de_tres(cadena, caracter):
    nueva_cadena = ""
    for x in range(len(cadena)):
        nueva_cadena += cadena[x]
        if(x%3 == 2 and x != len(cadena) -1):
            nueva_cadena += caracter
    return nueva_cadena

print (agregar_en_multiplos_de_tres("2552552550", '.'))
print (agregar_en_multiplos_de_tres("192168154124", '.'))
print (agregar_en_multiplos_de_tres("12", 'l'))