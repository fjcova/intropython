#coding=utf-8
'''
Escribir funciones que dada una cadena de caracteres:
a) Imprima los dos primeros caracteres.
b) Imprima los tres últimos caracteres.
c) Imprima dicha cadena cada dos caracteres. Ej.: 'recta' debería imprimir 'rca'
d) Dicha cadena en sentido inverso. Ej.: 'hola mundo!' debe imprimir '!odnum aloh'
e) Imprima la cadena en un sentido y en sentido inverso. Ej: 'reflejo' imprime
'reflejoojelfer' .
'''

def obtener_primeros_dos_caracteres(cadena):
    return cadena[:2]

print (obtener_primeros_dos_caracteres('Hola'))
print (obtener_primeros_dos_caracteres('Prueba'))
print (obtener_primeros_dos_caracteres('1'))

def obtener_ultimos_tres_caracteres(cadena):
    return cadena[-3:]

print (obtener_ultimos_tres_caracteres('Hola'))
print (obtener_ultimos_tres_caracteres('Prueba'))
print (obtener_ultimos_tres_caracteres('1'))

def obtener_indices_pares(cadena):
    nueva_cadena = ""
    for x in range(len(cadena)):
        if(x%2 == 0):
            nueva_cadena += cadena[x]
    return nueva_cadena

print (obtener_indices_pares("recta"))
print (obtener_indices_pares("Hola Mundo"))
print (obtener_indices_pares("B"))

def invertir_cadena(cadena):
    nueva_cadena = ""
    for i in range(1, len(cadena)+ 1):
        nueva_cadena += cadena[-i]
    return nueva_cadena

print (invertir_cadena("recta"))
print (invertir_cadena("Hola Mundo"))
print (invertir_cadena("B"))

def reflejar_cadena(cadena):
    reflejo = invertir_cadena(cadena)
    return cadena + reflejo

print (reflejar_cadena("recta"))
print (reflejar_cadena("Hola Mundo"))
print (reflejar_cadena("B"))