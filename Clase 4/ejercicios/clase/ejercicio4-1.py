#coding=utf-8

'''
Escribir un programa, llamado head que reciba un archivo y un número N e imprima las primeras N líneas del archivo.
'''

def mostrar_n_lineas_archivo(nombre_archivo, cantidad_lineas):
    archivo = open(nombre_archivo)
    lines = archivo.read().splitlines()
    for linea in range(cantidad_lineas):
        print (lines[linea])

mostrar_n_lineas_archivo('ejemplo.txt', 5)

