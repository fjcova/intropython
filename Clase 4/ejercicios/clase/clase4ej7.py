#coding=utf-8

'''
7.​ ​ Persistencia​ ​ de​ ​ un​ ​ diccionario
a)​ ​ Escribir​ ​ una​ ​ función​ ​ cargar_datos​ ​ que​ ​ reciba​ ​ un​ ​ nombre​
​ de​ ​ archivo,​ ​ cuyo​ ​ contenido
tiene​ ​ el​ ​ formato​ ​ clave,​ ​ valor​ ​ y ​ ​ devuelva​ ​ un​ ​ diccionario​ ​
con​ ​ el​ ​ primer​ ​ campo​ ​ como​ ​ clave
y​ ​ el​ ​ segundo​ ​ como​ ​ valor.
b)​ ​ Escribir​ ​ una​ ​ función​ ​ guardar_datos​ ​ que​ ​ reciba​ ​ un​ ​
diccionario​ ​ y ​ ​ un​ ​ nombre​ ​ de​ ​ archivo,
y​ ​ guarde​ ​ el​ ​ contenido​ ​ del​ ​ diccionario​ ​ en​ ​ el​ ​ archivo,​ ​
con​ ​ el​ ​ formato​ ​ clave,​ ​ valor​ ​ .
'''


def almacenar(datos, nombre_archivo):
    nuevo_archivo = open(nombre_archivo, "w")
    for clave, valor in datos.items():
        linea = str(clave) + "," + str(valor)
        nuevo_archivo.write(linea)
        nuevo_archivo.write('\n')
    nuevo_archivo.close()

almacenar({34:"Uruguay", 89:"Belgica", 12:"Panama"}, "Paises.txt")


def cargar_datos(nombre_archivo):
    archivo = open(nombre_archivo, "r")
    lineas = archivo.read().splitlines()
    datos = {}
    for linea in lineas:
        clave = linea.split(",")[0]
        valor = linea.split(",")[1]
        datos[clave] = valor
    archivo.close()
    return datos

mis_datos = cargar_datos("Paises.txt")
print(mis_datos)



