#coding=utf-8
'''
6​ . ​ ​ Escribir​ ​ un​ ​ programa,​ ​ llamado​ ​ rot13.py​ ​ que​ ​ reciba​ ​ un​
 ​ archivo​ ​ de​ ​ texto​ ​ de​ ​ origen
y​ ​ uno​ ​ de​ ​ destino,​ ​ de​ ​ modo​ ​ que​ ​ para​ ​ cada​ ​ línea​ ​ del​ ​
archivo​ ​ origen,​ ​ se​ ​ guarde​ ​ una​ ​ línea​ ​ cifrada
en​ ​ el​ ​ archivo​ ​ destino.​ ​ El​ ​ algoritmo​ ​ de​ ​ cifrado​ ​ a ​ ​
 utilizar​ ​ será​ ​ muy​ ​ sencillo:​ ​ a ​ ​ cada​ ​ caracter
comprendido​ ​ entre​ ​ la​ ​ a ​ ​ y ​ ​ la​ ​ z,​ ​ se​ ​ le​ ​ suma​ ​ 13​ ​ y ​ ​
luego​ ​ se​ ​ aplica​ ​ el​ ​ módulo​ ​ 26,​ ​ para​ ​ obtener​ ​ un
nuevo​ ​ caracter.
Ayuda:​ ​ Usar​ ​ funcion​ ​ ord(caracter)​ ​ para​ ​ obtener​ ​ el​ ​ ordinal​ ​ del​ ​ caracter​ ​ y ​ ​ chr(numero)​ ​ para
convertir​ ​ el​ ​ ordinal​ ​ a ​ ​ char​ ​ nuevamente.

'''

def rot13(texto_origen,texto_destino):
   archivoRecibido = open(texto_origen, "r")
   destinoArchivo = open(texto_destino, "w")
   contenido_archivo = archivoRecibido.read().splitlines()
 #  print((contenido_archivo))
   for linea in contenido_archivo:
       texto_linea = ""
       for letra in linea:
           a =chr((ord(str(letra))+ 13)%26)
           #print (a)
           texto_linea= texto_linea +  a
       destinoArchivo.write(texto_linea)
       #print(texto_linea)

rot13('ejemplo5.txt','resultej6.txt')