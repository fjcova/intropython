#coding=utf-8
"""
Escribir un programa, llamado cp.py, que copie todo el contenido de un archivo
(sea de texto o binario) a otro, de modo que quede exactamente igual.
Nota: utilizar archivo.read(bytes) para leer como máximo una cantidad de bytes.
"""

def copiar_archivo(origen, destino):
    archivo_origen = open(origen)
    archivo_destino = open(destino, 'w')
    data = archivo_origen.read(15)
    while data != '':
        print (data)
        archivo_destino.write(data)
        data = archivo_origen.read(15)


copiar_archivo('ejemplo.txt', 'ejemplo2.txt')