#coding= utf-8

import os

# Esto nos permite obtener la ruta donde esta este archivo
dir = os.path.dirname(__file__)

archivo_1 = open(dir + '/archivo_1.txt', "r")

archivo_2 = open('archivo_2.txt', 'w')

caracter = archivo_1.read(1)

while caracter:
    archivo_2.write(caracter)
    archivo_2.write(' ')
    caracter = archivo_1.read(1)

archivo_2.close()
archivo_1.close()