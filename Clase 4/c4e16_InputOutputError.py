#coding=utf-8
import sys




def funcion():
    try:
        f = open('miarchivo5.txt')
        s = f.readline()
        i = int(s.strip())
    except IOError as error:
        # Puedo mostrar el código de error y la descripcion del mismo
        print(error)
        return 2
        raise
    except ValueError:
        print("No pude convertir el dato a un entero.")
    except:
        print("Error inesperado:", sys.exc_info()[0])
        raise

try:
    print(funcion())
except:
    print("Fallo")