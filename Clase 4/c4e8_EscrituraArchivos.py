# coding=utf-8

import os

# Esto nos permite obtener la ruta donde esta este archivo
dir = os.path.dirname(__file__)

# Abro el archivo en modo escritura
r = open(dir + '/pruebaWrite.txt', 'w')
# escribo en el archivo
print (r.tell())
r.write('Estoy escribiendo un archivo')
print (r.tell())
r.write('Escribo algo mas')
# Miro en que posición quedó el puntero de escritura
print (r.tell())
# muevo el puntero a 6 lugares despues del inicio
r.seek(6, 0)
# miro en que posicion quedo el puntero
print (r.tell())
# Muevo dos lugares desde donde está el puntero actual
r.seek(10, 0)
# Miro en que posicion quedó el puntero
print (r.tell())
r.write("Agrego caracteres desde el caracter 10")
# Muevo el puntero hasta 5 lugares antes del final del archivo
r.seek(30, 0)
print (r.tell())
# Miro si el archivo está cerrado
print (r.closed)
# Cierro el archivo
r.close()
print (r.closed)

r = open(dir + '/pruebaWrite.txt', 'r')
print (r.read())
r.close()
