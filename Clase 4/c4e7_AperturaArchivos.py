# coding=utf-8

import os

# Esto nos permite obtener la ruta donde esta este archivo
dir_path = os.path.dirname(__file__)
print(dir_path)
# Aca poner una ruta válida a una carpeta o usar dir_path + / + nombre archivo
# solo escritura
f = open(dir_path + '/pruebaWrite.txt', 'w')
f.write("el archivo")


print (f)

# Solo lectura
r = open(dir_path + '/pruebaRead.txt', "r")
contenido = r.read()
print (contenido)
print (r)
r.close()
r.readlines()

# Agregar al final
r = open(dir_path + '/pruebaRead.txt', 'a+')
print (r)
contenido = r.read()
print (contenido)

# Lectura y Escritura sin sobre archivo existente
r = open(dir_path + '/pruebaRead.txt', 'r+')

