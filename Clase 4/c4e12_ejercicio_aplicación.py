#coding=utf-8


def obtener_indicadores(trades):
    volumen_operado = 0
    precio_promedio = 0
    primero = float(trades[0].split(',')[2])
    ultimo = float(trades[-1].split(',')[2])
    for trade in trades:
        hora, volumen, precio = trade.split(',')
        volumen_operado += int(volumen)
        precio_promedio += float(precio)
    precio_promedio /= len(trades)
    variacion = ((ultimo - primero)/primero)*100

    return volumen_operado, precio_promedio, variacion


trades = open('trades.csv')

trades_list = trades.read().splitlines()

volumen_operado, precio_promedio, variacion = obtener_indicadores(trades_list)

print ("El volumen operado fue de : {volumen_operado},el precio promedio fue de {precio_promedio}, y la variacion fue de {variacion}"
       .format(volumen_operado=volumen_operado, precio_promedio=precio_promedio, variacion=variacion))