#coding=utf-8
import pandas as pd
import time

from websocketclient_v3 import WebSocketClient

urlLogin = "http://api.remarkets.primary.com.ar/auth/getToken"
urlWebSocket = "ws://api.remarkets.primary.com.ar/"
account = "REM2501"
symbol = 'DOSep19'
entries = ['LA']



wsClient = WebSocketClient(urlLogin, "fernandojcova2501", "nmhjvU6)", "REM2501", urlWebSocket)


class recept_msg:
    def __init__(self):
        self.datos = pd.DataFrame(columns=["price", "size"])


    def agregar_nuevo_last(self,mensaje):

        self.datos = self.datos.append(mensaje["marketData"]["LA"] , ignore_index=True)


class VWAP_Calc:

    def __init__(self):
        self.datos = pd.DataFrame(columns=["Open","High","Low","Close","Vol","VTP"]
                                  ,dtype=float)

    def agregar_datos(self,nueva_data):
        self.datos = self.datos.append(nueva_data, ignore_index=True)

    def calculo_VTP(self):
        self.datos["VTP"] = ((self.datos["High"]+self.datos["Low"]+self.datos["Close"])/3)*self.datos["Vol"]

    def calculo_VWAP(self):
        self.VWAP = self.datos["VTP"].sum()/self.datos["Vol"].sum()

intervalo_tiempo = 60

vwap = VWAP_Calc()
mensaje = recept_msg()



wsClient.md_subscription(mensaje.agregar_nuevo_last, [symbol], entries)

while True:
    time.sleep(intervalo_tiempo)

    size = len(mensaje.datos.index)
    if( size > 0):
        high = mensaje.datos["price"].max()
        low = mensaje.datos["price"].min()
        openPrice = mensaje.datos["price"].iloc[0]
        closePrice = mensaje.datos["price"][len(mensaje.datos["price"])-1]
        volume = mensaje.datos["size"].sum()

        vwap.agregar_datos({"Open":openPrice,"High":high,"Low":low,"Close":closePrice,"Vol":volume})
        mensaje.datos = pd.DataFrame(columns=["price", "size"])
        vwap.calculo_VTP()
        vwap.calculo_VWAP()
        print(vwap.VWAP)




