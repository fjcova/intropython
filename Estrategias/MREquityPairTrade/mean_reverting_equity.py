# coding: utf-8

import datetime

import math
import numpy as np
import requests
import simplejson
import statsmodels.api as sm
from PrimaryApi.websocketPackage.websocketclient_v3 import WebSocketClient
from PrimaryApi.websocketPackage import primaryAPI
from primaryAPI import Entorno


class IntradayOLSMRStrategy:
    """
    Utiliza ordinary least squares (OLS) para realizar una regresión lineal
    con movimiento, para determinar el ratio de hedge entre las dos acciones.

    El z-score de la serie de tiempo de los residuos es luego calculado rolleando y
    si execede un intervalo de límites (por default [0.5, 3.0]) se emiten
    señales para ir long/short al mercado dependiendo el limite que se supera.
    """

    def __init__(self, symbol_list, ols_window=100, zscore_low=0.5, zscore_high=3.0):
        """
        Inicializa la estrategia
        Parametros:
        symbol_list - la lista de simbolos con las 2 acciones.
        ols_window - la ventana a tener en cuenta para datos hitoricos
        zscore_low - limite inferior del z-score que generaria una señal
        zscore_high - limite superior del z-score que generaria una señal
        """

        self.symbol_list = symbol_list
        # diccionario con la md historica (LAST) de cada instrumento
        self.md_historica = dict()
        self.md_historica[symbol_list[0]] = []
        self.md_historica[symbol_list[1]] = []
        self.ols_window = ols_window
        self.zscore_low = zscore_low
        self.zscore_high = zscore_high
        self.pair = (symbol_list[0], symbol_list[1])
        self.datetime = datetime.datetime.utcnow()
        self.long_market = False
        self.short_market = False
        self.last_x = None
        self.last_y = None

        # diccionario con la posición tomada para cada instrumento
        self.posicion = dict()
        self.posicion[symbol_list[0]] = 0
        self.posicion[symbol_list[1]] = 0

    def calculate_xy_signals(self, zscore_last):
        """
        calculamos las señales para los x, y actuales
        que luego enviaremos como ordenes al mercado.
        Parameters
        zscore_last - El zscore actual contra el que vamos a testear
        """

        y_signal = None
        x_signal = None
        #instrumento_0 = self.pair[0]
        #instrumento_1 = self.pair[1]
        #hora = self.datetime
        #hedge_ratio = abs(self.hedge_ratio)

        # Si no estamos long en el mercado y el zscore se encuentra debajo
        # del limite superior negativo (-3)
        if zscore_last <= -self.zscore_high and not self.long_market:
            self.long_market = True
            y_signal = 'LONG'
            x_signal = 'SHORT'

        # Si estamos long en el mercado y el zscore se encuentra entre
        # el valor absoluto del limite zscore inferior (+/-0.5)
        if abs(zscore_last) <= self.zscore_low and self.long_market:
            self.long_market = False
            y_signal = 'EXIT'
            x_signal = 'EXIT'

        # Si no estamos short en el mercado y el zscore se encuentra sobre
        # el limite superior
        if zscore_last >= self.zscore_high and not self.short_market:
            self.short_market = True
            y_signal = 'SHORT'
            x_signal = 'LONG'

        # Si estamos short en el mercado y el zscore se encuentra entre
        # el valor absoluto del limite zscore inferior (+/-0.5)
        if abs(zscore_last) <= self.zscore_low and self.short_market:
            self.short_market = False
            y_signal = 'EXIT'
            x_signal = 'EXIT'

        return y_signal, x_signal

    def calculate_signals_for_pairs(self, symbol, last):
        """
        Genera un nuevo conjunto de señales basadas en la estrategia
        de reversión a la media.
        Calcula el ratio de hedgeo entre ambos ticker.
        Utilizamos OLS para esto.
        """

        # Buscamos la última ventana de datos históricos para cada
        # componente del par de acciones
        index = self.pair.index(symbol)
        x_observado = self.md_historica[self.pair[0]][-1]
        y_observado = self.md_historica[self.pair[1]][-1]

        if(index == 0):
            x_observado = last
        else:
            y_observado =(last)
        y = np.array(self.md_historica[self.pair[1]][-self.ols_window:], dtype=float)
        x = np.array(self.md_historica[self.pair[0]][-self.ols_window:], dtype=float)

        if y is not None and x is not None:
            # Chequeamos que todas las ventanas tengan los datos necesarios
            if len(y) >= self.ols_window and len(x) >= self.ols_window:
                # Calculamos el hedge ratio actual utilizando OLS
                self.hedge_ratio = sm.OLS(y, x).fit().params[0]
                print (self.hedge_ratio)
                # Calculamos el z-score actual de los residuos
                spread = y - self.hedge_ratio * x
                observation = float(y_observado) - self.hedge_ratio * x_observado
                zscore_last = ((observation - spread.mean())/spread.std())
                # Calculamos las señales
                y_signal, x_signal = self.calculate_xy_signals(zscore_last)

                print (y_signal, x_signal)

        # Si la estrategia emitió señales
        if y_signal is not None and x_signal is not None:

            # Caso 1: estabamos long y queremos salir
            if not self.long_market and x_signal == "EXIT" and y_signal == "EXIT":
                self.enviar_orden(self.pair[0], self.posicion[0], "market", "sell", None)
                self.enviar_orden(self.pair[1], self.posicion[1], "market", "buy", None)
                self.posicion[0] = 0
                self.posicion[1] = 0
            # Caso 2: estabamos short y queremos salir
            elif not self.short_market and x_signal == "EXIT" and y_signal == "EXIT":
                self.enviar_orden(self.pair[0], self.posicion[0], "market", "buy", None)
                self.enviar_orden(self.pair[1], self.posicion[1], "market", "sell", None)
                self.posicion[0] = 0
                self.posicion[1] = 0
            # Caso 3: queremos ir long al mercado
            elif self.long_market and y_signal == "LONG" and x_signal == "SHORT":
                self.enviar_orden(self.pair[0], 1, "market", "sell", None)
                self.enviar_orden(self.pair[1], self.hedge_ratio, "market", "buy", None)
                self.posicion[0] = 1
                self.posicion[1] = self.hedge_ratio
            # Caso 4: queremos ir short al mercado
            elif self.short_market and y_signal == "SHORT" and x_signal == "LONG":
                self.enviar_orden(self.pair[0], 1, "market", "buy", None)
                self.enviar_orden(self.pair[1], math.ceil(self.hedge_ratio), "market", "sell", None)
                self.posicion[0] = 1
                self.posicion[1] = self.hedge_ratio



    def calculate_signals(self, message):
        """
        Agrega la market data al historico y calcula las señales.
        """

        if message["marketData"]["LA"]:
            last_price = message["marketData"]["LA"]["price"]
            symbol = message["instrumentId"]["symbol"]

            self.calculate_signals_for_pairs(symbol, last_price)

    def enviar_orden(self, ticker, cantidad, type, side, price):
        primaryAPI.enviar_Orden(ticker, cantidad, type, side, price)


if __name__ == "__main__":

    # Inicializamos el cliente WebSocket
    urlLogin = "http://api.remarkets.primary.com.ar/auth/getToken"
    urlWebSocket = "ws://api.remarkets.primary.com.ar/"
    wsClient = WebSocketClient(urlLogin, "fernandojcova2501", "nmhjvU6)", "REM2501", urlWebSocket)
    primaryAPI.init(userParam="fernandojcova2501", passwordParam="nmhjvU6)", accountParam="REM2501",entornoParam=Entorno.demo)
    primaryAPI.login()
    symbols = ['MERV - XMEV - GGAL - 48hs', 'MERV - XMEV - BMA - 48hs']
    # Creamos la estrategia
    estrategia_mr = IntradayOLSMRStrategy(symbols, zscore_high=2, zscore_low=0.3)

    session = requests.session();
    url_alpha_vantage = 'https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol={symbol}&outputsize=compact&apikey=9VSB693EOW5IOZ6V'
    url_GGAL = url_alpha_vantage.format(symbol="GGAL.BA")
    url_BMA = url_alpha_vantage.format(symbol="BMA.BA")

    responseGGAL = session.get(url_GGAL)
    responseBMA = session.get(url_BMA)
    daily_ggal = simplejson.loads(responseGGAL.content)["Time Series (Daily)"]

    daily_bma = simplejson.loads(responseBMA.content)["Time Series (Daily)"]
    closeGGAL = []
    closeBMA = []

    for key in sorted(daily_ggal.keys()):
        closeGGAL.append(daily_ggal[key]["4. close"])

    for key in sorted(daily_bma.keys()):
        closeBMA.append(daily_bma[key]["4. close"])

    print(closeGGAL)
    print(closeBMA)
    # Inicializamos la estrategia con datos historicos

    estrategia_mr.md_historica[symbols[0]] = closeGGAL
    estrategia_mr.md_historica[symbols[1]] = closeBMA

    # Nos suscribimos para recibir MD de ambos instrumentos
    entries = ['LA']
    wsClient.md_subscription(estrategia_mr.calculate_signals, symbols, entries)
