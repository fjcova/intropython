#coding=utf-8
import requests
import simplejson

from MovingAverageCrossOver.medias_moviles import MediasMoviles
from PrimaryApi.websocketPackage.websocketclient_v3 import WebSocketClient

estadoOrdenes={}

def login(user, password):
    session = requests.session()
    loginUrl = 'http://api.remarkets.primary.com.ar/auth/getToken'
    headers = {'X-Username': user, 'X-Password': password}
    loginResponse = session.post(loginUrl, headers=headers)
    # Chequeamos si la respuesta del request fue correcta, un ok va a ser un response code 200 (OK)
    if loginResponse.ok:
        # Checkeamos si nos pudimos loguear correctamente
        token = loginResponse.headers["X-Auth-Token"]
        print ("Login correcto")
        return session
    else:
        print ("Fallo el login")
        raise Exception


base_market_url = "http://api.remarkets.primary.com.ar/rest/order/newSingleOrder?marketId={marketId}&price={price}&symbol={symbol}&account={account}&orderQty={orderQty}&ordType=market_to_limit&side={side}&timeInForce={timeInForce}&cancelPrevious={cancelPrevious}"


session = login("fernandojcova2501", "nmhjvU6)")


urlLogin = "http://api.remarkets.primary.com.ar/auth/getToken"
urlWebSocket = "ws://api.remarkets.primary.com.ar/"

cantidad = 10

account = "REM2501"
msj_new_order = '{{"type": "no","product":{{"symbol": "DONov19","marketId": "ROFX"}},"price": "{p}","quantity": "{c}","side": "{side}","account": "{a}"}}'

datos = [71.2000,74.2000,75.2000,78.0000,79.0000,72.2500,70.0000,71,70.5, 68,67]

medias = MediasMoviles(datos, 10, 5)


wsClient = WebSocketClient(urlLogin, "fernandojcova2501", "nmhjvU6)", "REM2501", urlWebSocket)


ordenesEnviadas={}

def actualizar_medias(message):
    print (message)
    market_data = message["marketData"]
    if(market_data["LA"] !=  None):
        last_price = market_data["LA"]["price"]
        resultado = medias.agregar_valor(last_price)
        print ("Media corta: {0}, Media larga: {1}".format(medias.media_corta, medias.media_larga))
        if resultado == "COMPRAR":
           url_ordenes = base_market_url.format(marketId="ROFX", symbol="DONov19", account=account, orderQty="5", side="Buy",
                                                timeInForce="DAY", cancelPrevious="false", price=last_price)
           response = session.get(url_ordenes)
           content = simplejson.loads(response.content)
           client_id = content['order']['clientId']
           ordenesEnviadas[client_id] = 'PENDING'
           print (content)

        elif resultado == "VENDER":
            #enviar orden venta
            url_ordenes = base_market_url.format(marketId="ROFX", symbol="DONov19", account=account, orderQty="5", side="Sell",
                                                 timeInForce="DAY", cancelPrevious="false", price=last_price)
            response = session.get(url_ordenes)
            content = simplejson.loads(response.content)
            client_id = content['order']['clientId']
            ordenesEnviadas[client_id] = 'PENDING'
            print (response.content)
        print (resultado)


symbols = ['DONov19']
entries = ['LA']
wsClient.md_subscription(actualizar_medias, symbols, entries)

def actualizar_ER(message):
    clOrdId = message["orderReport"]['clOrdId']

    if  clOrdId in ordenesEnviadas:
        estado = message["orderReport"]['status']
        print (estado)
        if estado == "FILLED" or estado == "PARTIALLY_FILLED":
            medias.cambiar_lado()
            print(ordenesEnviadas)
        elif estado == "CANCELLED" or estado == "REJECTED":
            print(ordenesEnviadas)
            del ordenesEnviadas[clOrdId]

wsClient.or_subscription(actualizar_ER,['REM2501'])