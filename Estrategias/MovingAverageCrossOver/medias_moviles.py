#coding=utf-8

import numpy as np


class MediasMoviles:

    def __init__(self, datos = [], tamanio_plazo_largo = 12, tamanio_plazo_corto = 6):
        self._datos = datos
        self._tamanio_corta = tamanio_plazo_corto
        self._tamanio_larga = tamanio_plazo_largo
        self.calcular_media_corta()
        self.calcular_media_larga()
        if(self.media_corta < self.media_larga):
            self.ultima_accion = "VENDER"
        else:
            self.ultima_accion = "COMPRAR"
        print("Arranca la estrategia. Media Corta: {short}, Media Larga: {long} . Ultima Accion: {action}".format(short=self.media_corta,
                                                                                                                  long=self.media_larga,
                                                                                                                  action=self.ultima_accion))

    def agregar_valor(self, valor):
        self._datos.append(valor)
        self.calcular_media_corta()
        self.calcular_media_larga()
        accion_a_ejecutar = "NADA"

        if self.media_corta > self.media_larga and self.ultima_accion == "VENDER":
            print("Compraaaaaa!")
            #enviar compra
            accion_a_ejecutar = "COMPRAR"
        elif self.media_corta < self.media_larga and self.ultima_accion == "COMPRAR":
            print("Vendeeeeeeee")
            accion_a_ejecutar = "VENDER"
        return accion_a_ejecutar

    def calcular_media_corta(self):
        self.media_corta = np.mean(self._datos[-self._tamanio_corta:])

    def calcular_media_larga(self):
        self.media_larga = np.mean(self._datos[-self._tamanio_larga:])


    def cambiar_lado(self):
        if (self.ultima_accion == "VENDER"):
            self.ultima_accion = "COMPRAR"
        else:
            self.ultima_accion = "VENDER"


