#coding=utf-8
from ContadoLiqui.ccl import InstrumentoCCL
from websocketclient_v3 import WebSocketClient

urlLogin = "http://api.remarkets.primary.com.ar/auth/getToken"
urlWebSocket = "ws://api.remarkets.primary.com.ar/"
account = "REM2501"
msj_new_order = '{{"type": "no","product":{{"symbol": "{symbol}","marketId": "ROFX"}},"price": "{p}","quantity": "{c}","side": "{side}","account": "{a}"}}'
symbolBYMA = 'MERV - XMEV - {symbol} - CI'
symbols = [symbolBYMA.format(symbol='AA22'), symbolBYMA.format(symbol='A2E2'), symbolBYMA.format(symbol='AA25'), symbolBYMA.format(symbol='AA37')]
entries = ['BI', 'OF']

parA2E2AA22 = InstrumentoCCL(symbolBYMA.format(symbol='AA22'), symbolBYMA.format(symbol='A2E2'))

parAA25AA37 = InstrumentoCCL(symbolBYMA.format(symbol='AA25'), symbolBYMA.format(symbol='AA37'))

instrumentos_x_doble_clave = {}
instrumentos_x_doble_clave[symbolBYMA.format(symbol='AA22')] = parA2E2AA22
instrumentos_x_doble_clave[symbolBYMA.format(symbol='A2E2')] = parA2E2AA22
instrumentos_x_doble_clave[symbolBYMA.format(symbol='AA25')] = parAA25AA37
instrumentos_x_doble_clave[symbolBYMA.format(symbol='AA37')] = parAA25AA37


wsClient = WebSocketClient(urlLogin, "fernandojcova2501", "nmhjvU6)", "REM2501", urlWebSocket)

def on_market_data_update(message):
    print (message)
    symbol = message['instrumentId']['symbol']
    instrumento_ccl = instrumentos_x_doble_clave[symbol]
    market_data = message["marketData"]
    bids = market_data["BI"]
    offers = market_data["OF"]
    if (symbol == instrumento_ccl.symbol_pesos):
        instrumento_ccl.offersPesos = offers
    elif (symbol == instrumento_ccl.symbol_dolares):
        instrumento_ccl.bidsDolares = bids
    instrumento_ccl.calcular_cotizacion()
    validarCondicion()


def validarCondicion():
    values = list(instrumentos_x_doble_clave.values());
    values = sorted(values)
    primer_valor = values[0]
    if(primer_valor.cotizacion < 27):
        instrumento_en_pesos = primer_valor.symbol_pesos
        instrumento_en_dolares = primer_valor.symbol_dolares
        size = primer_valor.sizeMejorCotizacion
        precioCompra = primer_valor.offersPesos[0]['price']
        precioVenta = primer_valor.bidsDolares[0]['price']
        #"price": "{p}","quantity": "{c}","side": "{side}","account": "{a}"}}'
        msg_compra = msj_new_order.format(symbol=instrumento_en_pesos, p=precioCompra, c=size
                                          , side="BUY", a=account)
        msg_venta = msj_new_order.format(symbol=instrumento_en_dolares, p=precioVenta, c=size
                                          , side="SELL", a=account)
        wsClient.enviar_msg(msg_compra)
        wsClient.enviar_msg(msg_venta)
        print("Deberia hacer mi operatoria")


def message(msg):
    print(msg)

wsClient.md_subscription(on_market_data_update, symbols, entries)
wsClient.or_subscription(message, accounts=["REM2501"])


