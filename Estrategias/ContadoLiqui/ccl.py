#coding=utf-8

class InstrumentoCCL:

    def __init__(self, symbol_offers, symbol_bids, bids=[], offers=[]):
        self.symbol_offers = symbol_offers
        self.symbol_bids = symbol_bids
        self.bids = bids
        self.offers = offers
        self.calcular_cotizacion()


    def calcular_cotizacion(self):
        pass


    def __lt__(self, other):
        if(self.cotizacion < other.cotizacion):
            return True
        elif(self.cotizacion == other.cotizacion):
            return self.sizeMejorCotizacion >= other.sizeMejorCotizacion
        return False

    def __eq__(self, other):
        return self.cotizacion == other.cotizacion and self.sizeMejorCotizacion == other.sizeMejorCotizacion

    def __str__(self):
        return "Especies:{0},{1}, Cotizacion: {cot}, Size={size}".format(self.symbol_offers, self.symbol_bids, cot=self.cotizacion, size=self.sizeMejorCotizacion)



class InstrumentoCCLVenta(InstrumentoCCL):


    #bids serian dolares y offers serian pesos
    def calcular_cotizacion(self):
        #offer/bid
        top_offer = self.offers[0]
        top_bid = self.bids[0]
        self.cotizacion = top_offer['price']/top_bid['price']
        self.sizeMejorCotizacion = min(top_offer['size'], top_bid['size'])


class InstrumentoCCLCompra(InstrumentoCCL):

    # bids serian pesos y offers serian dolares
    def calcular_cotizacion(self):
        # offer/bid
        top_offer = self.offers[0]
        top_bid = self.bids[0]
        self.cotizacion = top_bid['price'] / top_offer['price']
        self.sizeMejorCotizacion = min(top_offer['size'], top_bid['size'])


if __name__ == "__main__":

    symbols = [('AY24', 'AY24D'), ('DICA', 'DICAD')]

    bidsAY24D = [{"price": 2, "size":10},{"price":1.9, "size":3}]

    bidsDICAD = [{"price": 5, "size":4},{"price":4, "size":7}]
    offersAY24 = [{"price": 61, "size":3},{"price":62, "size":3}]
    offersDICA = [{"price": 140, "size":10},{"price":142, "size":5}]


    offersAY24D = [{"price": 2.2, "size":10},{"price":1.9, "size":3}]
    offersDICAD = [{"price": 5, "size":4},{"price":4, "size":7}]
    bidsAY24 = [{"price": 60, "size":6},{"price":62, "size":3}]
    bidsDICA = [{"price": 140, "size":10},{"price":142, "size":5}]

    parAY24AY24D = InstrumentoCCLVenta('AY24', 'AY24D', bidsAY24D, offersAY24)

    parDICADICAD = InstrumentoCCLVenta('DICA', 'DICAD', bidsDICAD, offersDICA)

    parAY24AY24D_c= InstrumentoCCLCompra('AY24', 'AY24D', bidsAY24, offersAY24D)

    parDICADICAD_c = InstrumentoCCLCompra('DICA', 'DICAD', bidsDICA, offersDICAD)

    lista = [parAY24AY24D, parDICADICAD]
    lista_c = [parAY24AY24D_c, parDICADICAD_c]


    lista.sort(reverse=True)
    lista_c.sort()
    for element in lista:
        print (element)

    for element in lista_c:
        print (element)