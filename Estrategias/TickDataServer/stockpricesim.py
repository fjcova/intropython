# coding: utf-8

import time
import math
import random
import datetime
import primaryAPI as pmy
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np


class StockPrice(object):


    def __init__(self, symbol, short_window, long_window):

        self.symbol = symbol
        self.t = time.time()
        self.value = 50.
        self.sigma = 5.6
        self.r = 0.45

        self.short_window = short_window
        self.long_window = long_window
        self.historico = pd.DataFrame(columns=["price", "sma-corta", "sma-larga"])


    def simulate_value(self):
        '''
        Genera un nuevo precio aleatorio que simula el comportamiento de una acción.
        '''

        t = time.time()
        dt = (t - self.t) / (252 * 8 * 60 * 60)
        dt *= 100
        self.t = t
        self.value *= math.exp((self.r - 0.5 * self.sigma ** 2) * dt +
                               self.sigma * math.sqrt(dt) * random.gauss(0, 1))

        self.value = float("{0:.2f}".format(self.value))

        self.historico.loc[datetime.datetime.now()] = [self.value, np.mean(self.historico.price[-self.short_window:]), np.mean(self.historico.price[-self.long_window:])]
        return self.value


    def generar_trade(self):
        for row in self.historico:
            # Orden Compra
            print (pmy.enviar_Orden(self.symbol, row["price"], 1, pmy.OrderType.limit, pmy.Side.buy, "10"))
            # Orden Venta
            print (pmy.enviar_Orden(self.symbol, row["price"], 1, pmy.OrderType.limit, pmy.Side.sell, "20"))

    def graficar_trades(self):
        pd.DatetimeIndex(sp.historico.index)
        sp.historico.plot(figsize=(10, 6))
        plt.show()


if __name__ == "__main__":

    # Inicializamos Conexión
    pmy.init("fernandojcova2501", "nmhjvU6)", "REM2501", pmy.Entorno.demo)
    pmy.login()

    sp = StockPrice("MERV - XMEV - GGAL - 48Hs", 5, 15)
    for val in range(1,100):
        price = sp.simulate_value()
        msg = '%s %s' % (sp.symbol, price)
        print (msg)
        #interval = random.random()
        #time.sleep(interval)

    sp.graficar_trades()
    #sp.generar_trade()



