import random
import time

import requests

from pbcpconnector.OrderManager import OrderManager
from pbcpconnector.configuration import UserConfiguration
from pbcpconnector.environmentConfigurationManager import getDemoApiConfiguration
from pbcpconnector.login import loginPBCP
from rimaconnector.apiRiskIntegration import loginRisk, addPosition, addMovements, addCredit, \
    addCollateral, emptyAll

defaultUserRima = "algoTraderBroker"
defaultPasswordRima = "Trading1+"
defaultUsernamePBCP = "fernandojcova2501"
defaultPasswordPBCP = "nmhjvU6)"
defaultEnvironment = "http://api.remarkets.primary.com.ar"
account = "REM2501"

userPBCP = UserConfiguration(defaultUsernamePBCP, defaultPasswordPBCP)
configPBCP = getDemoApiConfiguration()
sessionPBCP = requests.Session()
loginResult = loginPBCP(configPBCP, userPBCP, sessionPBCP)

sessionRima = loginRisk(defaultUserRima, defaultPasswordRima)
emptyAll(sessionRima, account)
addCredit(sessionRima, account, 2000, 0, "ARS")
addCredit(sessionRima, account, 3000, 0, "U$S")
addCredit(sessionRima, account, -3000, 0, "U$S")
addMovements(sessionRima, account, 5000, 2, "ARS")
addMovements(sessionRima, account, 7000, 3, "ARS")
addPosition(sessionRima, account, "GGAL", 0, 10, 0, 0)
addPosition(sessionRima, account, "GGAL", 1, 10, 0, 0)
addPosition(sessionRima, account, "GGAL", 3, 10, 0, 0)
addCollateral(sessionRima, account, "ARS", 10000)

orderManager = OrderManager(sessionPBCP, configPBCP)
quantity = 2
initialPrice = 17.514
maxPrice = 18.514
minPrice = 16.514
ticker = 0.01

response = orderManager.sendSimpleBuyOrder("MERV - XMEV - GGAL - CI", account, 70, 2)
clientId = response['order']['clientId']
time.sleep(2)
orderManager.cancelOrder(clientId)


def sendOrderBasedOnNumberOfOrder(x):
    global ticker, initialPrice, quantity
    quantity = random.randint(1, 20)
    if x % 2 == 0:
        orderManager.sendSimpleBuyOrder("MERV - XMEV - GGAL - CI", account, 70, 2)
    elif x % 2 == 1:
        orderManager.sendSimpleSellOrder("DOAgo17", account, initialPrice, quantity)
    if maxPrice - initialPrice < 0.009:
        ticker = -1
    elif initialPrice - minPrice < 0.009:
        ticker = 1
    initialPrice += ticker


'''
for x in range(100):
    t = threading.Thread(target=sendOrderBasedOnNumberOfOrder(x))
    t.start()
'''
