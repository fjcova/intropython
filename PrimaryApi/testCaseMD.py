import requests

from pbcpconnector.MarketDataManager import MarketDataManager
from pbcpconnector.configuration import UserConfiguration
from pbcpconnector.environmentConfigurationManager import getTowerGenericConfiguration
from pbcpconnector.login import loginPBCP

defaultUsernamePBCP = "fzanuso"
defaultPasswordPBCP = "pass"
defaultEnvironment = "newrisk.dev.primary"

userPBCP = UserConfiguration(defaultUsernamePBCP, defaultPasswordPBCP)
configPBCP = getTowerGenericConfiguration(defaultEnvironment)
sessionPBCP = requests.Session()
loginResult = loginPBCP(configPBCP, userPBCP, sessionPBCP)

mdManager = MarketDataManager(sessionPBCP, configPBCP)
marketDataDolar = mdManager.getMarketDataForSymbol("DOAgo17")
print(marketDataDolar.getBidPrice())
print(marketDataDolar.getLast())
print(marketDataDolar.getOfferPrice())
print(marketDataDolar.getBidSize())
print(marketDataDolar.getOfferSize())
