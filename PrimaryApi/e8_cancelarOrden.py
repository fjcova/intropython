#coding=utf-8

from e3_login_trading_api import login
import simplejson
import sys
import time

session = login("fernandojcova2501", "nmhjvU6)")


send_order_base_url = "http://api.remarkets.primary.com.ar/rest/order/newSingleOrder?marketId={marketId}&symbol={symbol}&account={account}&price={price}&orderQty={orderQty}&ordType={ordType}&side={side}&timeInForce={timeInForce}&cancelPrevious={cancelPrevious}"

cancel_order_url = "http://api.remarkets.primary.com.ar/rest/order/cancelById?clOrdId={clientId}&proprietary={proprietary}"

#Orden comun de compra
url_ordenes = send_order_base_url.format(marketId ="ROFX", symbol="DoNov19",
                                         account="REM2501", price="78.30", orderQty="1",
                                         ordType="limit",
                                         side="Buy", timeInForce="Day", cancelPrevious="true")

response = session.get(url_ordenes)
response_obj = simplejson.loads(response.content)["order"]
print(response.content)
client_id = response_obj["clientId"]
proprietary = response_obj["proprietary"]

cancel_response = session.get(cancel_order_url.format(clientId=client_id, proprietary=proprietary))
print(cancel_response.content)
#orden inmediata
url_ordenes = send_order_base_url.format(marketId ="ROFX", symbol="DoNov19", account="REM2501", price="30", orderQty="5", ordType="limit",
                                         side="Sell", timeInForce="IOC", cancelPrevious="false")

response = session.get(url_ordenes)
response_obj = simplejson.loads(response.content)["order"]
client_id = response_obj["clientId"]
proprietary = response_obj["proprietary"]
print(response.content)

cancel_response = session.get(cancel_order_url.format(clientId=client_id, proprietary=proprietary))
print(cancel_response.content)


