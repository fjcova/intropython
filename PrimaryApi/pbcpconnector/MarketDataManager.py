import simplejson

from MarketData import MarketData
from pbcpconnector.configuration import PBCPApiEndpoints


class MarketDataManager:
    def __init__(self, session, config):
        self.session = session
        self.config = config

    def getMarketDataForSymbol(self, symbol):
        url = self.config.getPbcpUrl() + PBCPApiEndpoints.MARKET_DATA + "?marketId={}&symbol={}&entries=BI,OF,LA&depth={}".format(
            "ROFX", symbol, "8")
        marketData = MarketData(symbol)
        response = simplejson.loads(self.session.get(url).content)
        if response['status'] == 'OK':
            responseMarketData = response["marketData"]
            if (responseMarketData["LA"]):
                last = responseMarketData["LA"]["price"]
                marketData.setLastPrice(last)
            bids = responseMarketData["BI"]
            if (len(bids) > 0):
                bestBid = bids[0]
                marketData.setBidPrice(bestBid["price"])
                marketData.setBidSize(bestBid["size"])
                marketData.setBids(bids)
            offers = responseMarketData["OF"]
            if (len(offers) > 0):
                bestOffer = offers[0]
                marketData.setOfferPrice(bestOffer["price"])
                marketData.setOfferSize(bestOffer["size"])
                marketData.setBids(offers)
        return marketData
