from marketConstants import OrderType, Side, TimeInForce, MarketId


class Order:
    def __init__(self):
        self.marketId = MarketId.ROFEX
        self.symbol = ""
        self.orderQty = 1
        self.price = ""
        self.ordType = OrderType.LIMIT
        self.side = Side.BUY
        self.timeInForce = TimeInForce.DAY
        self.account = ""
        self.cancelPrevious = True
        self.iceberg = False
        self.expireDate = ""
        self.displayQty = ""

    def setMarketId(self, marketId):
        self.marketId = marketId

    def setSymbol(self, symbol):
        self.symbol = symbol

    def setOrderQuantity(self, quantity):
        self.orderQty = quantity

    def setPrice(self, price):
        self.price = price

    def setOrderType(self, orderType):
        self.ordType = orderType

    def setSide(self, side):
        self.side = side

    def setTimeInForce(self, timeInForce):
        self.timeInForce = timeInForce

    def setAccount(self, account):
        self.account = account

    def setCancelPrevious(self, cancelPrevious):
        self.cancelPrevious = cancelPrevious

    def setIceberg(self, iceberg):
        self.iceberg = iceberg

    def setExpireDate(self, date):
        self.expireDate = date

    def setDisplayQty(self, displayQty):
        self.displayQty = displayQty

    def getFormatBoolean(self, value):
        if (value == True):
            return "true"
        elif (value == False):
            return "false"
        return ""

    def toUrlPathValues(self):
        url = "marketId=" + self.marketId + "&symbol=" + self.symbol + "&price=" + str(self.price) + "&orderQty=" + str(
            self.orderQty) + "&ordType="
        url = url + self.ordType + "&side=" + self.side + "&timeInForce=" + self.timeInForce + "&account=" + self.account
        url = url + "&cancelPrevious=" + self.getFormatBoolean(
            self.cancelPrevious) + "&iceberg=" + self.getFormatBoolean(self.iceberg)
        url = url + "&expireDate=" + self.expireDate + "&displayQty=" + self.displayQty
        return url


class NewOrderBuilder:
    def __init__(self):
        self.marketId = MarketId.ROFEX
        self.symbol = ""
        self.orderQty = 1
        self.price = ""
        self.ordType = OrderType.LIMIT
        self.side = Side.BUY
        self.timeInForce = TimeInForce.DAY
        self.account = ""
        self.cancelPrevious = True
        self.iceberg = False
        self.expireDate = ""
        self.displayQty = ""

    def setMarketId(self, marketId):
        self.marketId = marketId
        return self

    def setSymbol(self, symbol):
        self.symbol = symbol
        return self

    def setPrice(self, price):
        self.price = price
        return self

    def setOrderQty(self, orderQty):
        self.orderQty = orderQty
        return self

    def setOrderType(self, ordType):
        self.ordType = ordType
        return self

    def setSide(self, side):
        self.side = side
        return self

    def setAccount(self, account):
        self.account = account
        return self

    def setCancelPrevious(self, cancel):
        self.cancelPrevious = cancel
        return self

    def makeIceberg(self, displayQty):
        self.iceberg = True
        self.displayQty = displayQty
        return self

    def removeIcerberg(self):
        self.iceberg = False
        self.displayQty = 1
        return self

    def setExpireDate(self, date):
        if (self.ordType == TimeInForce.GTD):
            self.expireDate = date
        else:
            print ("Expire Date only available for GTD orders")
        return self

    def setTimeInForce(self, timeInForce):
        self.timeInForce = timeInForce
        return self

    def build(self):
        order = Order()
        order.setMarketId(self.marketId)
        order.setSymbol(self.symbol)
        order.setPrice(self.price)
        order.setOrderQuantity(self.orderQty)
        order.setOrderType(self.ordType)
        order.setSide(self.side)
        order.setTimeInForce(self.timeInForce)
        order.setAccount(self.account)
        order.setCancelPrevious(self.cancelPrevious)
        order.setIceberg(self.iceberg)
        order.setExpireDate(self.expireDate)
        order.setDisplayQty(self.displayQty)
        return order
