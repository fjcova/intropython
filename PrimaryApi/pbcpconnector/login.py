from configuration import PBCPApiEndpoints


def loginPBCP(envConfig, userConfig, session):
    loginUrl = envConfig.getPbcpUrl() + PBCPApiEndpoints.TOKEN_ENDPOINT.value
    headers = {'X-Username': userConfig.getUsername(), 'X-Password': userConfig.getPassword()}
    loginResponse = session.post(loginUrl, headers=headers)
    # Chequeamos si la respuesta del request fue correcta, un ok va a ser un response code 200 (OK)
    if (loginResponse.ok):
        # Checkeamos si nos pudimos loguear correctamente
        token = loginResponse.headers["X-Auth-Token"]
        userConfig.setToken(token)
        return 0
    else:
        # Si el response no es un codigo 200, mostramos un msj de error y devolvemos un false
        print("\nRequest Error.")
        # print ('Login Exitoso')
        return -1
