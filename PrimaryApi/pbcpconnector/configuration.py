from enum import Enum


class EnvironmentConfiguration:
    def __init__(self, pbcpEndpointUrl, historicalMarketDataEndpointUrl, historyOHLCEndpoint):
        self.pbcpEndpointUrl = pbcpEndpointUrl
        self.historicalMarketDataEndpointUrl = historicalMarketDataEndpointUrl
        self.historyOHLCEndpoint = historyOHLCEndpoint

    def getPbcpUrl(self):
        return self.pbcpEndpointUrl

    def getHistorialMDEndpoint(self):
        return self.historicalMarketDataEndpointUrl

    def getHistoryOHLCEndpoint(self):
        return self.historyOHLCEndpoint


class UserConfiguration:
    def __init__(self, username, password):
        self.username = username
        self.password = password

    def getUsername(self):
        return self.username

    def getPassword(self):
        return self.password

    def setToken(self, token):
        self.token = token


class PBCPApiEndpoints(Enum):
    LOGIN_ENDPOINT = "pbcp/rest/users/login"
    TOKEN_ENDPOINT = "auth/getToken"
    INSTRUMENTS_ENDPOINT = "rest/instruments/all"
    NEW_SINGLE_ORDER = "rest/order/newSingleOrder?"
    CANCEL_ORDER = "rest/order/cancelById"
    MARKET_DATA = "rest/marketdata/get"
