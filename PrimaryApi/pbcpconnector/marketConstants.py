from enum import Enum


class MarketId(Enum):
    ROFEX = "ROFX"


class OrderType(Enum):
    LIMIT = "limit"
    MARKET_TO_LIMIT = "market_to_limit"


class Side(Enum):
    BUY = "Buy"
    SELL = "Sell"


class TimeInForce(Enum):
    DAY = "Day"
    IOC = "IOC"
    FOK = "FOK"
    GTD = "GTD"
