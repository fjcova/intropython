class MarketData:
    def __init__(self, symbol, last=None, bidPrice=None, bidSize=None, offerSize=None, offerPrice=None):
        self.symbol = symbol
        self.lastPrice = last
        self.bidSize = bidSize
        self.offerSize = offerSize
        self.bidPrice = bidPrice
        self.offerPrice = offerPrice
        self.bids = []
        self.offers = []

    def getLast(self):
        return self.lastPrice

    def getBidPrice(self):
        return self.bidPrice

    def getOfferPrice(self):
        return self.offerPrice

    def getBidSize(self):
        return self.bidSize

    def getOfferSize(self):
        return self.offerSize

    def setLastPrice(self, last):
        self.lastPrice = last

    def setBidPrice(self, param):
        self.bidPrice = param

    def setOffers(self, offers):
        self.offers = offers

    def setOfferSize(self, param):
        self.offerSize = param

    def setOfferPrice(self, param):
        self.offerPrice = param

    def setBids(self, bids):
        self.bids = bids

    def setBidSize(self, param):
        self.bidSize = param
