import simplejson
from pbcpconnector.marketConstants import Side

from Order import NewOrderBuilder
from configuration import PBCPApiEndpoints


class OrderManager:
    def __init__(self, session, config):
        self.session = session
        self.config = config

    def sendSimpleBuyOrder(self, symbol, account, price, quantity):
        orderBuilder = NewOrderBuilder()
        orderBuilder = orderBuilder.setSymbol(symbol).setAccount(account)\
            .setPrice(price).setOrderQty(quantity)
        return self.buildAndSendOrder(orderBuilder)

    def buildAndSendOrder(self, orderBuilder):
        order = orderBuilder.build()
        urlForSingleOrder = self.config.getPbcpUrl() + PBCPApiEndpoints.NEW_SINGLE_ORDER.value + order.toUrlPathValues()
        response = simplejson.loads(self.session.get(urlForSingleOrder).content)
        print (response)
        return response

    def sendSimpleSellOrder(self, symbol, account, price, quantity):
        orderBuilder = NewOrderBuilder()
        orderBuilder = orderBuilder.setSymbol(symbol).setAccount(account)\
            .setPrice(price).setOrderQty(quantity).setSide(Side.SELL)
        return self.buildAndSendOrder(orderBuilder)

    def cancelOrder(self, clientOrderId):
        urlForCancelOrder = self.config.getPbcpUrl() + PBCPApiEndpoints.CANCEL_ORDER.value + "?clOrdId={}&proprietary=PBCP".format(
            clientOrderId)
        result = self.session.get(urlForCancelOrder)
        print (result)
        return result
