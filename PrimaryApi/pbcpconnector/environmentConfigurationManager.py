from configuration import EnvironmentConfiguration


def getDemoApiConfiguration():
    config = EnvironmentConfiguration("http://api.remarkets.primary.com.ar/",
                                      "http://h-api.primary.com.ar/MHD/Trades/{s}/{fi}/{ff}",
                                      "http://h-api.primary.com.ar/MHD/TradesOHLC/{s}/{fi}/{ff}/{hi}/{hf}")
    return config


def getProductionApiConfiguration():
    config = EnvironmentConfiguration("https://api.primary.com.ar/",
                                      "http://h-api.primary.com.ar/MHD/Trades/{s}/{fi}/{ff}",
                                      "http://h-api.primary.com.ar/MHD/TradesOHLC/{s}/{fi}/{ff}/{hi}/{hf}")
    return config

