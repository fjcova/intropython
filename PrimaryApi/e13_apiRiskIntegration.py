from base64 import b64encode

import requests
import simplejson


def loginRisk(user, password):
    url = "http://api.remarkets.primary.com.ar:8444/ptp-oms-webservices/rest/login"
    session = requests.Session()
    data = {}
    loginResponse = session.post(url, data=simplejson.dumps(data), auth=(user, password))
    print (loginResponse)
    return session


def getAccountReport(session):
    url = "http://api.remarkets.primary.com.ar:8444/ptp-oms-webservices/rest/accountReport"
    accountReport = session.get(url)
    print (accountReport.content)


def addCredit(session, account, amount, settlType, currency):
    url = "http://api.remarkets.primary.com.ar:8444/ptp-oms-webservices/rest/credit/add?accountName={accountName}&currency={currency}&amount={amount}&period={period}".format(
        accountName=account, amount=amount, period=settlType, currency=currency)
    response = session.get(url)
    print (response.status_code)


def emptyCredit(session, account):
    url = "http://api.remarkets.primary.com.ar:8444/ptp-oms-webservices/rest/import/credits"
    data = {"account": account, "credits": []}
    headers = {'Content-type': 'application/json', 'Accept': 'application/json'}
    response = session.post(url, data=simplejson.dumps(data), headers=headers)
    print (response.status_code)


def addMovements(session, account, amount, settlType, currency):
    url = "http://api.remarkets.primary.com.ar:8444/ptp-oms-webservices/rest/import/movements"
    data = {"account": account, "keepPrevious": True,
            "movements": [{"account": account, "currency": currency, "cash": amount, "settlType": settlType}]}
    headers = {'Content-type': 'application/json', 'Accept': 'application/json'}
    response = session.post(url, data=simplejson.dumps(data), headers=headers)
    print (response.status_code)


def emptyMovements(session, account):
    url = "http://api.remarkets.primary.com.ar:8444/ptp-oms-webservices/rest/import/movements"
    data = {"account": account, "movements": []}
    headers = {'Content-type': 'application/json', 'Accept': 'application/json'}
    response = session.post(url, data=simplejson.dumps(data), headers=headers)
    print (response.status_code)


def addPosition(session, account, symbol, settlType, size, price, originalPrice):
    url = "http://api.remarkets.primary.com.ar:8444/ptp-oms-webservices/rest/import/initialpositions"
    data = {"account": account, "keepPrevious": True,
            "positions": [
                {"account": account, "symbolReference": symbol, "size": size, "settlType": settlType, "price": price,
                 "originalPrice": originalPrice}]}
    headers = {'Content-type': 'application/json', 'Accept': 'application/json'}
    response = session.post(url, data=simplejson.dumps(data), headers=headers)
    print(response.status_code)


def emptyInitialPositions(session, account):
    url = "http://api.remarkets.primary.com.ar:8444/ptp-oms-webservices/rest/import/initialpositions"
    data = {"account": account, "positions": []}
    headers = {'Content-type': 'application/json', 'Accept': 'application/json'}
    response = session.post(url, data=simplejson.dumps(data), headers=headers)
    print(response.status_code)


def addCollateral(session, account, currency, amount):
    url = "http://api.remarkets.primary.com.ar:8444/ptp-oms-webservices/rest/import/collaterals"
    data = {"account": account, "keepPrevious": True,
            "collaterals": [{"account": account, "currency": currency, "amount": amount}]}
    headers = {'Content-type': 'application/json', 'Accept': 'application/json'}
    response = session.post(url, data=simplejson.dumps(data), headers=headers)
    print(response.status_code)


def emptyCollaterals(session, account):
    url = "http://api.remarkets.primary.com.ar:8444/ptp-oms-webservices/rest/import/collaterals"
    data = {"account": account, "collaterals": []}
    headers = {'Content-type': 'application/json', 'Accept': 'application/json'}
    response = session.post(url, data=simplejson.dumps(data), headers=headers)
    print(response.status_code)


def emptyAll(session, account):
    emptyCredit(session, account)
    emptyCollaterals(session, account)
    emptyInitialPositions(session, account)
    emptyMovements(session, account)

session = loginRisk("algoTraderBroker", "qK7Kj1bO")

addPosition(session, "REM2501", "GGAL", "CASH", 10, 70, 70)