import threading
import time
import requests
import websocket
import json
import queue


def getTokenLogin(user, password):
    session = requests.session()
    loginUrl = 'http://api.remarkets.primary.com.ar/auth/getToken'
    headers = {'X-Username': user, 'X-Password': password}
    loginResponse = session.post(loginUrl, headers=headers)
    # Chequeamos si la respuesta del request fue correcta, un ok va a ser un response code 200 (OK)
    if loginResponse.ok:
        # Checkeamos si nos pudimos loguear correctamente
        token = loginResponse.headers["X-Auth-Token"]
        return token

queue = queue.Queue()

token = getTokenLogin("fernandojcova2501", "nmhjvU6)")

headers = {'X-Auth-Token:{token}'.format(token=token)}

def on_message(ws, msg):
    #print("Llegó un mensaje: " + msg)
    mensaje = json.loads(msg)

    if mensaje["type"] == "Md":
        queue.put(mensaje)
        print("Llego un mensaje de Marketdata "+msg)
    elif mensaje["type"] == "or":
        print("Llego un mensaje de Execution Report "+msg)
    else:
        print("Llego un mensaje de tipo desconocido")


def on_error(ws, error):
    print("Hubo un error: " + error)


def on_close(ws):
    print("Se cerró el websocket")

def on_open(ws):
    print("Abrimos la conexión")



urlWS = "ws://api.remarkets.primary.com.ar/"

ws = websocket.WebSocketApp(urlWS, on_message=on_message,
                                                    on_error=on_error,
                                                    on_close=on_close,
                                                    on_open=on_open,
                                                    header=headers)


#ws.run_forever(ping_interval= 30)
wst = threading.Thread(target=ws.run_forever, kwargs={"ping_interval": 1})
wst.start()
time.sleep(3)
#msj = '{"type": "os", "snapshotOnlyActive":true}'
#msj_md = '{"type":"smd","level":1, "entries":["BI", "LA", "OF"],"products":[{"symbol":"DOSep19","marketId":"ROFX"}]}'

msg_or = {"type":"os","snapshotOnlyActive":True}
msg_marketdata = {"type":"smd","level":1,"depth":3,"entries":["BI","LA","OF"],"products":[{"symbol":"DOSep19","marketId":"ROFX"}]}
#ws.send(msj)
#ws.send(msj_md)

msg_str = json.dumps(msg_marketdata)
ws.send(msg_str)
msg_str = json.dumps(msg_or)
#ws.send(msg_str)
contador = 0
while True:
    mensaje = queue.get()
    bid_size = mensaje["marketData"]["BI"][0]["size"]
    bid_price = mensaje["marketData"]["BI"][0]["price"]
    contador = bid_size
    if contador >=10:
        print("Hay {contador} contratos en el bid".format(contador=contador))
        nueva_orden = {"type":"no", "product":{
                                     "symbol":"DOSep19",
                                     "marketId":"ROFX"
                                     },"price":bid_price,
                                 "quantity":1,
                                 "side":"SELL",
                                 "account":"REM2501",
                                 "wsClOrdId":"REM2501-1"}
        msg_no = json.dumps(nueva_orden)
        ws.send(msg_no)

