# coding: utf-8

import threading
import websocketPackage
import simplejson
import requests
from time import sleep


def on_message(ws, message):
    # Valido Mensaje entrante
    msg = simplejson.loads(message)

    # Valido el tipo de msj, si es un ER lo agrego a la lista
    print(msg)



class WebSocketClient:

    def __init__(self, urlLogin, user, password, urlWS):

        self.ERs = []

        # Nos autenticamos y pedimos el token
        token = self.login(urlLogin, user, password)

        # Si el token es válido establecemos la conexión
        if token is not None:
            headers = {'X-Auth-Token:{token}'.format(token=token)}

            self.ws = websocketPackage.WebSocketApp(urlWS,
                                                    on_message=self.on_message,
                                                    on_error=self.on_error,
                                                    on_close=self.on_close,
                                                    on_open=self.on_open,
                                                    header=headers)

            self.wst = threading.Thread(target=self.ws.run_forever, kwargs={"ping_interval": 30})
            self.wst.start()

            # Esperamos a que la conexion ws se establezca
            conn_timeout = 5
            sleep(1)
            while not self.ws.sock.connected and conn_timeout:
                sleep(1)
                conn_timeout -= 1
        else:
            print("Error al autenticarse...")

        self.on_message = on_message

    def login(self, url, user, password):

        headers = {'X-Username': user, 'X-Password': password}
        loginResponse = requests.post(url, headers=headers, verify=False)

        # Checkeamos si la respuesta del request fue correcta,
        # un ok va a ser un response code 200 (OK)
        if (loginResponse.ok):
            return loginResponse.headers['X-Auth-Token'];
        else:
            return None




    def on_error(self, ws, error):
        print(error)
        ws.close()
        print("### connection closed ###")

    def on_close(self, ws):
        print("### connection closed ###")

    def on_open(self, ws):
        print("On Conection Open...")

    def enviar_msg(self, msg_enviar):
        self.ws.send(msg_enviar)


if __name__ == "__main__":
    pass
    # 1- Creamos un Cliente WebSocket
    # 2- Nos sucribimos para recibir ER de la cuenta ...
    # 3- Enviamos una orden al mercado
    # 4- Si la orden fue aceptada y esta en estado NEW la cancelamos
