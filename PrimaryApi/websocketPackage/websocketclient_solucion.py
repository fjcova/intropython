# coding: utf-8

import threading

from websocket import WebSocketApp

import simplejson
import requests
from time import sleep



class WebSocketClient():

    def __init__(self, urlLogin, user, password, urlWS):

        self.ERActivas = []

        # Nos autenticamos y pedimos el token
        token = self.login(urlLogin, user, password)

        # Si el token es válido establecemos la conexión
        if token is not None:
            headers = {'X-Auth-Token:{token}'.format(token=token)}

            self.ws = WebSocketApp(urlWS,
                                                    on_message=self.on_message,
                                                    on_error=self.on_error,
                                                    on_close=self.on_close,
                                                    on_open=self.on_open,
                                                    header=headers)

            self.wst = threading.Thread(target=self.ws.run_forever)
            self.wst.start()

            # Esperamos a que la conexion ws se establezca
            conn_timeout = 5
            sleep(1)
            while not self.ws.sock.connected and conn_timeout:
                sleep(1)
                conn_timeout -= 1
        else:
            print ("Error al autenticarse...")

    def login(self, url, user, password):

        headers = {'X-Username': user, 'X-Password': password}
        loginResponse = requests.post(url, headers=headers, verify=False)

        # Checkeamos si la respuesta del request fue correcta,
        # un ok va a ser un response code 200 (OK)
        if (loginResponse.ok):
            return loginResponse.headers['X-Auth-Token'];
        else:
            return None

    def on_message(self, ws, message):
        # Valido Mensaje entrante
        print (message)
        msg = simplejson.loads(message)
        msgType = msg['type'].upper()

        if msgType == 'OR':
            self.ERActivas.append(msg)

        print (msg)

    def on_error(self, ws, error):
        print (error)
        ws.close()
        print ("### connection closed ###")

    def on_close(self, ws):
        print ("### connection closed ###")

    def on_open(self, ws):
        print ("On Conection Open...")

    def enviar_msg(self, msg_enviar):
        self.ws.send(msg_enviar)

if __name__ == "__main__":

    # 1- Creamos un Cliente WebSocket
    urlLogin = "http://api.remarkets.primary.com.ar/auth/getToken"
    urlWebSocket = "ws://api.remarkets.primary.com.ar/"
    wsClient = WebSocketClient(urlLogin, "fernandojcova2501", "nmhjvU6)", urlWebSocket)

    # 2- Nos sucribimos para recibir ER de la cuenta 20
    account = "20"
 #   msj_suscripcion_a_OR = '{{"type":"os","account":{{"id":"{a}"}},"snapshotOnlyActive":true}}'
  #  suscripcion_a_OR = msj_suscripcion_a_OR.format(a=account)

    msj = '{"type": "os", "snapshotOnlyActive":true}'

    wsClient.enviar_msg(msj)

    sleep(1)


    # 3- Enviamos una orden al mercado
    cantidad = 10
    price = 50
    side = "BUY"
    msj_new_order = '{{"type": "no","product":{{"symbol": "PAT","marketId": "ROFX"}},"price": "{p}","quantity": "{c}","side": "{s}","account": "{a}"}}'


    new_order = msj_new_order.format(p=price, c=cantidad, s=side, a=account)
    wsClient.enviar_msg(new_order)


    sleep(1)
    # 4- Si la orden fue aceptada y esta en estado NEW la cancelamos
    msj_cancel_order = '{{"type": "co","clientId": "{clOrdId}","proprietary": "api"}}'
    sleep(1)


    for ER in wsClient.ERActivas:
        print (ER["orderReport"]["status"])
        if ER["orderReport"]["status"] == "NEW":
            cancel_order = msj_cancel_order.format(clOrdId=ER["orderReport"]["clOrdId"])
            wsClient.enviar_msg(cancel_order )
        else:
            print ("No se pudo cancelar")

