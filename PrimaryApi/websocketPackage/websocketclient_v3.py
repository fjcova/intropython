#!/usr/bin/python
#coding=utf-8
import threading
import simplejson
from time import sleep

from websocket import WebSocketApp

import primaryAPI


class WebSocketClient():

    MSG_MDSuscription = '{{"type":"smd","level":1, "entries":[{entries}],"products":[{symbols}]}}'
    MSG_OSSuscription = '{{"type":"os","account":{{"id":"{a}"}},"snapshotOnlyActive":true}}'
    MSG_Symbol = '{{"symbol":"{symbols}","marketId":"ROFX"}}'
    MSG_Double_Quotes = '"{symbols}"'

    def __init__(self, url_login, user, password, account, url_ws):
        self.subsMDEvents = []
        self.subsOREvents = []

        # Nos autenticamos y pedimos el token

        primaryAPI.init(user, password, account, primaryAPI.Entorno.demo)
        primaryAPI.login()
        token = primaryAPI.token

        # Si el token es válido establecemos la conexión
        if token is not None:
            headers = {'X-Auth-Token:{token}'.format(token=token)}

            self.ws = WebSocketApp(url_ws,
                                                    on_message=self.on_message,
                                                    on_error=self.on_error,
                                                    on_close=self.on_close,
                                                    on_open=self.on_open,
                                                    header=headers)

            self.wst = threading.Thread(target=self.ws.run_forever, kwargs={"ping_interval":270})
            self.wst.start()

            # Esperamos a que la conexion ws se establezca
            conn_timeout = 5
            sleep(1)
            while not self.ws.sock.connected and conn_timeout:
                sleep(1)
                conn_timeout -= 1
        else:
            print ("Error al autenticarse...")

    def on_message(self, ws, message):
        try:
            # Valido Mensaje entrante
            msg = simplejson.loads(message)
            msgType = msg['type'].upper()

            if msgType == 'MD':
                name = 'MD_' + msg['instrumentId']["symbol"]
                for func in self.subsMDEvents:
                    func(msg)
            elif msgType == 'OR':
                name = 'OR_' + msg['orderReport']["accountId"]["id"]
                for func in self.subsOREvents:
                    func(msg)
            else:
                print ("Tipo de Mensaje Recibido No soportado: " + msg)
        except:
            print ("Error al procesar mensaje recibido: " + message)

    def on_error(self, ws, error):
        print (error)
        ws.close()
        print ("### connection closed ###")

    def on_close(self, ws):
        print ("### connection closed ###")

    def on_open(self, ws):
        print ("On Conection Open...")

    def md_subscription(self, function, symbols_list, entries_list):
        if symbols_list and entries_list:
            # Prepare Symbol Message Data
            symbols_msg = self.MSG_Symbol.format(symbols=symbols_list.pop(0))
            for symbol in symbols_list:
                symbols_msg = symbols_msg + "," + self.MSG_Symbol.format(symbols=symbol)

            # Prepare Entries Message Data
            entries_msg = self.MSG_Double_Quotes.format(symbols=entries_list.pop(0))
            for entrie in entries_list:
                entries_msg = entries_msg + "," + self.MSG_Double_Quotes.format(symbols=entrie)

            msg_enviar = self.MSG_MDSuscription.format(entries=entries_msg, symbols=symbols_msg)

            print (msg_enviar)
            self.ws.send(msg_enviar)

            # Subscriptores a MDEvents
            if function not in self.subsMDEvents:
                self.subsMDEvents.append(function)

    def or_subscription(self, function, accounts):
        for account in accounts:
            msg_enviar = self.MSG_OSSuscription.format(a=account)
            #print msg_enviar
            self.ws.send(msg_enviar)

        # Subscriptores a OREvents

        self.subsOREvents.append(function)


    def enviar_msg(self, msg_enviar):
        self.ws.send(msg_enviar)


    def cerrar(self):
        self.ws.close()

def printMD(msg):
    print (msg)

def printMD_2(msg):
    print(msg)

if __name__ == "__main__":
    # Creamos un Cliente WebSocket

    urlLogin = "http://api.remarkets.primary.com.ar/auth/getToken"
    urlWebSocket = "ws://api.remarkets.primary.com.ar/"
    #login("fernandojcova2501", "nmhjvU6)")
    wsClient = WebSocketClient(urlLogin, "fernandojcova2501", "nmhjvU6)", 'REM2501', urlWebSocket)

    # Nos suscribimos para recibir MD
    s = ['DOSep19']
    e = ['BI']
    sleep(1)
    wsClient.md_subscription(printMD, s, e)
    s = ['DOSep19']
    e = ['OF']
    wsClient.md_subscription(printMD_2, s, e)
    #wsClient.or_subscription(printMD)


