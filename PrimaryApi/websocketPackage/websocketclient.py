# coding: utf-8

import threading

import sys
import websocket
import simplejson
import requests
from time import sleep

ordenes_x_cuenta = {}

columns=["symbol", "BS", "BP", "OS", "OP", "LS", "LP"]


symbols = []
bidPrices = []
bidSizes = []
offerPrices = []
offerSizes = []
lastPrices = []
lastSizes = []




def on_message_md(ws, message):
    # Valido Mensaje entrante
    msg = simplejson.loads(message)
    instrumentId = msg["instrumentId"]
    marketData = msg["marketData"]
    symbols.append(instrumentId["symbol"])
    if "BI" in marketData and len(marketData["BI"]) > 0:
        bid = marketData["BI"][0]
        bidPrice = float(bid["price"])
        bidSize = float(bid["size"])
        bidSizes.append(bidSize)
        bidPrices.append(bidPrice)
    else:
        bidSizes.append(None)
        bidPrices.append(None)

    if "OF" in marketData and len(marketData["OF"]) > 0:
        offer = marketData["OF"][0]
        offerPrice = float(offer["price"])
        offerSize = float(offer["size"])
        offerSizes.append(offerSize)
        offerPrices.append(offerPrice)
    else:
        offerSizes.append(None)
        offerPrices.append(None)
    if "LA" in marketData and marketData["LA"] != None:
        last = marketData["LA"]
        lastPrice = float(last["price"])
        lastSize = float(last["size"])
        lastSizes.append(lastSize)
        lastPrices.append(lastPrice)
    else:
        lastSizes.append(None)
        lastPrices.append(None)

   # print("Msj Nuevo: " + str(msg))

def on_message_execution_report(ws, message):
    msg = simplejson.loads(message)
    orderReport = msg["orderReport"]
    cuenta = orderReport["accountId"]["id"]
    if(cuenta not in ordenes_x_cuenta):
        ordenes_x_cuenta[cuenta] = []
    ordenes_x_cuenta[cuenta].append(orderReport)
    print("ExecutionReport para la cuenta {cuenta} ".format(cuenta=cuenta) + str(orderReport))


def on_error(ws, error):
    print(error)
    ws.close()
    print("### connection closed ###")


def on_close(ws):

    print("### connection closed ###")
    import datetime
    print(datetime.datetime.now())


def on_open(ws):
    print("On Conection Open...")
    import datetime
    print(datetime.datetime.now())


class WebSocketClient():


    def __init__(self, urlLogin, user, password, urlWS, on_message, on_error, on_close, on_open):
        # Nos autenticamos y pedimos el token
        token = self.login(urlLogin, user, password)
        # Si el token es válido establecemos la conexión
        if token is not None:
            headers = {'X-Auth-Token:{token}'.format(token=token)}
            #creo un websocketApp para manejar la conexion websocket
            self.ws = websocket.WebSocketApp(urlWS,
                                                    on_message=on_message,
                                                    on_error=on_error,
                                                    on_close=on_close,
                                                    on_open=on_open,
                                                    header=headers)

            #Se pone el intervalo de ping para que no se desconecte el websocket
            self.wst = threading.Thread(target=self.ws.run_forever, kwargs={"ping_interval":295})
            self.wst.start()
            # Esperamos a que la conexion ws se establezca
            conn_timeout = 5
            sleep(1)
            while not self.ws.sock.connected and conn_timeout:
                sleep(1)
                conn_timeout -= 1

        else:
            print ("Error al autenticarse...")

    def login(self, url, user, password):

        headers = {'X-Username': user, 'X-Password': password}
        loginResponse = requests.post(url, headers=headers, verify=False)

        # Checkeamos si la respuesta del request fue correcta,
        # un ok va a ser un response code 200 (OK)
        if (loginResponse.ok):
            return loginResponse.headers['X-Auth-Token'];
        else:
            return None



    def enviar_msg(self, msg_enviar):
        self.ws.send(msg_enviar)

if __name__ == "__main__":

    # 1- Creamos un Cliente WebSocket
    urlLogin = "http://pbcp-remarkets.primary.com.ar/auth/getToken"
    urlWebSocket = "wss://pbcp-remarkets..primary.com.ar/"
    wsClientMD = WebSocketClient(urlLogin, "fzanuso", "pass", urlWebSocket, on_message_md, on_error, on_close, on_open)
    wsClientER = WebSocketClient(urlLogin, "fzanuso", "pass", urlWebSocket, on_message_execution_report, on_error, on_close, on_open)

    # 2- Creamos el Mensaje para suscribimos a MD

    symbol = '{"symbol":"DoNov18","marketId":"ROFX"}'
    symbol2 = '{"symbol":"DoDic18","marketId":"ROFX"}'

    msj_suscripcion_a_MD = '{{"type":"smd",' \
                        '"level":1, ' \
                        '"entries":["BI", "LA", "OF"],' \
                        '"products":[{s}]}}'

    msj = msj_suscripcion_a_MD.format(s=symbol)

    # 3- Enviamos el Mensaje por la conexión WebSocket
    wsClientMD.enviar_msg(msj)
  #  wsClientMD.enviar_msg(msj_suscripcion_a_MD.format(s=symbol2))

  #  msj_ex_report = '''{
  #      "type":"os",
  #      "account":{
  #      "id":"11015"
  #      }
  #  }'''

#    wsClientER.enviar_msg(msj_ex_report)


#    msj_new_order = '''{ "type":"no", "product":{ "symbol":"DONov18", "marketId":"ROFX" }, "price":"31.5", "quantity":"10", "side":"BUY", "account":"11015"}'''

#    wsClientER.enviar_msg(msj_new_order)

while(True):
    sleep(5)
    print(list[-1])



