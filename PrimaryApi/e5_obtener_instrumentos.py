#coding=utf-8
import simplejson

from e3_login_trading_api import login


def obtener_instrumentos(session):
    respuesta = session.get("http://api.remarkets.primary.com.ar/rest/instruments/all")
    if respuesta.ok:
        return (simplejson.loads(respuesta.content)["instruments"])
    else:
        print("Falló al obtener los instrumentos")


def obtener_instrumentos_detallados(session):
    respuesta = session.get("http://api.remarkets.primary.com.ar/rest/instruments/details")
    if respuesta.ok:
        return(simplejson.loads(respuesta.content)["instruments"])
    else:
        print("Falló al obtener los instrumentos")


def obtener_detalle_instrument(session, symbol, marketId="ROFX"):
    respuesta = session.get("http://api.remarkets.primary.com.ar/rest/instruments/detail?symbol={symbol}&marketId={marketId}".\
                            format(symbol=symbol, marketId=marketId))
    if respuesta.ok:
        return(simplejson.loads(respuesta.content)["instrument"])
    else:
        print("Falló al obtener el instrumento")


def obtener_instrumentos_por_cfiCode(session, cfiCode):
    respuesta = session.get("http://api.remarkets.primary.com.ar/rest/instruments/byCFICode?CFICode=" + cfiCode)
    if respuesta.ok:
        return(simplejson.loads(respuesta.content)["instruments"])
    else:
        print("Falló al obtener los instrumentos para el cfiCode " + cfiCode)


def obtener_instrumentos_por_segmento(session, segmento, marketId = "ROFX"):
    respuesta = session.get("http://api.remarkets.primary.com.ar/rest/instruments/bySegment?MarketSegmentID={segmento}&MarketID={marketId}"\
                            .format(segmento=segmento, marketId=marketId))
    if respuesta.ok:
        return(simplejson.loads(respuesta.content)["instruments"])
    else:
        print("Falló al obtener los instrumentos para el segmento " + segmento)


resultado = []
session = login("fernandojcova2501", "nmhjvU6)")
#resultado = obtener_instrumentos(session)
#resultado =  obtener_instrumentos_detallados(session)
resultado_unico =  obtener_detalle_instrument(session, "DoNov19")
resultado = obtener_instrumentos_por_cfiCode(session, "OCAFXS")
#resultado =obtener_instrumentos_por_segmento(session, "MERV")

print(resultado_unico)
for element in resultado:
    print (element)