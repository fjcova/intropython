#coding=utf-8

from e3_login_trading_api import login
import simplejson

session = login("fernandojcova2501", "nmhjvU6)")


url_md = "http://api.remarkets.primary.com.ar/rest/marketdata/get?symbol={symbol}" \
         "&marketId={marketId}&depth={depth}&entries=BI,OF,LA,TV"


pedido_1 = url_md.format(symbol="DoNov19", marketId="ROFX", depth=3)
pedido_2 = url_md.format(symbol="RFX20Sep19", marketId="ROFX", depth=2)


def pedir_market_data(session, url_md):
    response = session.get(url_md)
    market_data = simplejson.loads(response.content)["marketData"]
    print(type(market_data))
    print(market_data)
    bids = []
    offers = []
    last = None
    volumen = None
    minimo = None
    maximo = None
    apertura = None
    if "BI" in market_data:
        bids = market_data["BI"]
    if "OF" in market_data:
        offers = market_data["OF"]
    if "LA" in market_data:
        last = market_data["LA"]
    if "TV" in market_data:
        volumen = market_data["TV"]
    if "LO" in market_data:
        minimo = market_data["LO"]
    if "HI" in market_data:
        maximo = market_data["HI"]
    if "OP" in market_data:
        apertura = market_data["OP"]

    print("Bids = " + str(bids), "Offers= " + str(offers), "Last: " + str(last), "Vol: " + str(volumen), "Min: " +  str(minimo), "Max: " + str(maximo), "Open: " + str(apertura))


pedir_market_data(session, pedido_1)
pedir_market_data(session, pedido_2)
