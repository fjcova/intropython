#coding=utf-8
import sys

from e3_login_trading_api import login

session = login("fernandojcova2501", "nmhjvU6)")


base_url = "http://api.remarkets.primary.com.ar/rest/order/newSingleOrder?marketId={marketId}" \
           "&symbol={symbol}&account={account}&price={price}&orderQty={orderQty" \
           "}&ordType={ordType}&side={side}&timeInForce={timeInForce}" \
           "&cancelPrevious={cancelPrevious}"


#Orden comun de compra
url_ordenes = base_url.format(marketId = "ROFX", symbol="DoNov19", account="REM2501",
                              price="78", orderQty="3", ordType="limit",
                              side="Buy", timeInForce="Day", cancelPrevious="true")

response = session.get(url_ordenes)
print(response.content)

#orden inmediata
url_ordenes = base_url.format(marketId = "ROFX", symbol="DoNov19", account="REM2501", price="78", orderQty="5", ordType="limit",
                              side="Sell", timeInForce="IOC", cancelPrevious="false")
response = session.get(url_ordenes)


#orden iceberg
url_ordenes = base_url.format(marketId = "ROFX", symbol="DoNov19", account="REM420", price="70.30", orderQty="1000", ordType="limit",
                              side="Buy", timeInForce="Day", cancelPrevious="false")
url_ordenes += "&iceberg=true&displayQty=" + "100"
response = session.get(url_ordenes)
print(response.content)
sys.exit()