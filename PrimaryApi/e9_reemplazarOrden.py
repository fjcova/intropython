#coding=utf-8
import time
import decimal

from e3_login_trading_api import login
import simplejson
import sys


session = login("fernandojcova2501", "nmhjvU6)")


send_order_base_url = "http://api.remarkets.primary.com.ar/rest/order/newSingleOrder?marketId={marketId}&symbol={symbol}&account={account}&price={price}&orderQty={orderQty}&ordType={ordType}&side={side}&timeInForce={timeInForce}&cancelPrevious={cancelPrevious}"

replace_order_url = "http://api.remarkets.primary.com.ar/rest/order/replaceById?clOrdId={clientId}&proprietary={proprietary}&orderQty={orderQty}&price={price}"

#Orden comun de compra
url_ordenes = send_order_base_url.format(marketId ="ROFX", symbol="DoNov19", account="REM2501", price="71", orderQty="1", ordType="limit",
                                         side="Buy", timeInForce="Day", cancelPrevious="true")

response = session.get(url_ordenes)
response_obj = simplejson.loads(response.content)["order"]
print(response.content)
client_id = response_obj["clientId"]
proprietary = response_obj["proprietary"]

replace_response = session.get(replace_order_url.format(clientId=client_id, proprietary=proprietary, orderQty=2, price=78))
precio_base = 78.10

cantidad = 1
for x in range(20):
    time.sleep(1)
    response_obj = simplejson.loads(replace_response.content)["order"]
    print(replace_response.content)
    client_id = response_obj["clientId"]
    proprietary = response_obj["proprietary"]
    replace_response = session.get(
        replace_order_url.format(clientId=client_id, proprietary=proprietary, orderQty=cantidad, price=precio_base))
    cantidad +=1

#orden inmediata
url_ordenes = send_order_base_url.format(marketId ="ROFX", symbol="DoNov19", account="REM2501", price="71", orderQty="5", ordType="limit",
                                         side="Sell", timeInForce="IOC", cancelPrevious="false")

response = session.get(url_ordenes)
response_obj = simplejson.loads(response.content)["order"]
client_id = response_obj["clientId"]
proprietary = response_obj["proprietary"]
print(response.content)

replace_response = session.get(replace_order_url.format(clientId=client_id, proprietary=proprietary, orderQty=2, price=70.04))
print(replace_response.content)

#orden iceberg
url_ordenes = send_order_base_url.format(marketId ="ROFX", symbol="DoNov19", account="REM2501", price="70", orderQty="6", ordType="limit",
                                         side="Sell", timeInForce="Day", cancelPrevious="false")
url_ordenes += "&iceberg=true&displayQty=" + "2"
response = session.get(url_ordenes)
response_obj = simplejson.loads(response.content)["order"]
client_id = response_obj["clientId"]
proprietary = response_obj["proprietary"]
print(response.content)

replace_response = session.get(replace_order_url.format(clientId=client_id, proprietary=proprietary, orderQty=2, price=70.04))
print(replace_response.content)
sys.exit()