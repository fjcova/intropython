#coding=utf-8
import simplejson

from e3_login_trading_api import login



def obtener_reporte_cuenta(session, cuenta):
    respuesta = session.get("http://api.remarkets.primary.com.ar/rest/risk/accountReport/{accountName}".format(accountName=cuenta))
    if respuesta.ok:
        reporte = simplejson.loads(respuesta.content)
        return reporte
    else:
        print("Falló al obtener el reporte")


def obtener_reporte_posicion(session, cuenta):
    respuesta = session.get("http://api.remarkets.primary.com.ar/rest/risk/detailedPosition/{accountName}".format(accountName=cuenta))
    if respuesta.ok:
        reporte = simplejson.loads(respuesta.content)
        return reporte
    else:
        print("Falló al obtener el reporte")


session = login("fernandojcova2501", "nmhjvU6)")

print(obtener_reporte_cuenta(session, "REM2501"))
print(obtener_reporte_posicion(session, "REM2501"))
