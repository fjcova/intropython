#coding=utf-8
import simplejson

from e3_login_trading_api import login

session = login("fzanuso", "pass")

send_order_base_url = "http://demo-api.primary.com.ar/rest/order/newSingleOrder?marketId={marketId}&symbol={symbol}&account={account}&price={price}&orderQty={orderQty}&ordType={ordType}&side={side}&timeInForce={timeInForce}&cancelPrevious={cancelPrevious}"

url_ordenes = send_order_base_url.format(marketId ="ROFX", symbol="DoNov18",
                                         account="11015", price="28", orderQty="1",
                                         ordType="limit",
                                         side="Buy", timeInForce="Day", cancelPrevious="true")

response = session.get(url_ordenes)
content = response.content
order = simplejson.loads(content)["order"]
clientId = order["clientId"]
proprietary = order["proprietary"]


url_consulta = "http://demo-api.primary.com.ar/rest/order/id?clOrdId={clientId}&proprietary={proprietary}"

response_consulta = session.get(url_consulta.format(clientId=clientId, proprietary=proprietary))
print(response_consulta.content)

'''
{
"status":"OK",
"order":{
"clientId":"user14473450286174Cnl5",
"proprietary":"demo-api"
}
}'''