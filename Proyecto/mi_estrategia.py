#coding=utf-8
import numpy as np


class MiEstrategia():
    """
    Implementa una estrategia básica Moving Average Crossover.
    Por defecto las ventanas short/long son de 10/40 periodos respectivamente.
    """

    def __init__(self, symbol, tasa_ref):
        """
        symbol = ["MERV - XMEV - AY24 - 48hs", "MERV - XMEV - AY24 - CI"]
        """
        # suscribirme a MD
        # Ask CI
        # Bid 48hs
        # Variables con última MD recibida
        lasts = dict()
        lasts["MERV - XMEV - AY24 - 48hs"] = 0
        lasts["MERV - XMEV - AY24 - CI"] = 0

        pass

    def calculate_signals(self, MDevent):
        """
        """

        # Actualizo con la nueva MD

        # Calcular Nueva Tasa
        # Emitir Señal de Compra/Venta por la cantidad min entre bid y ask
        # Entrar en estado de Espera

        pass

    def update_estado(self, EREvent):

        pass