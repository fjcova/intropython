#coding=utf-8

import matplotlib.pyplot as plt
import numpy as np
from alpha_vantage.timeseries import TimeSeries
import pandas as pd

fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(9, 4))

# Fixing random state for reproducibility

API_KEY = 'W40ROM77UOEYGNF5'
timeSeries = TimeSeries(key=API_KEY, output_format='pandas', indexing_type='date')
SUPV, meta_data_SUPV = timeSeries.get_daily_adjusted(symbol='SUPV.BA', outputsize="compact")
bma, meta_data_bma = timeSeries.get_daily_adjusted(symbol='BMA.BA', outputsize="compact")
ggal, meta_data_ggal = timeSeries.get_daily_adjusted(symbol='GGAL.BA', outputsize="compact")

df = pd.DataFrame(index=SUPV.index)

df["SUPV"] = SUPV["5. adjusted close"]
df["BMA"] = bma["5. adjusted close"]
df["GGAL"] = ggal["5. adjusted close"]
df = df.drop(df[df["SUPV"] <= 0].index)
df = df.drop(df[df["BMA"] <= 0].index)
df = df.drop(df[df["GGAL"] <= 0].index)

df["returnBMA"] = df["BMA"].pct_change()
df["returnSUPV"] = df["SUPV"].pct_change()
df["returnGGAL"] = df["GGAL"].pct_change()
df = df.dropna()
df["diffReturns"] = df["returnGGAL"] - df["returnBMA"]


# plot violin plot
axes[0].violinplot([df["returnBMA"].values, df["returnGGAL"].values, df["diffReturns"].values],
                   showmeans=False,
                   showmedians=True)
axes[0].set_title('Violin plot')

# plot box plot
axes[1].boxplot([df["returnBMA"].values, df["returnGGAL"].values, df["diffReturns"].values])
axes[1].set_title('Box plot')



# adding horizontal grid lines
for ax in axes:
    ax.yaxis.grid(True)
    ax.set_xticks([y + 1 for y in range(3)])
    ax.set_xlabel('Two separate samples')
    ax.set_ylabel('Observed values')

# add x-tick labels
plt.setp(axes, xticks=[y + 1 for y in range(3)],
         xticklabels=['GGAL', 'SUPV', 'Diff'])
plt.show()