# coding: utf-8

import numpy as np
from numpy.random import randn
import pandas as pd
from pandas_datareader import data as web


def hurst(ts, min_lag_size=2 ,max_lag_size = 100):
    """Returns the Hurst Exponent of the time series vector timeSeries"""
    # Create the range of lag values
    #El tamaño de la ventana es a discreción
    lags = np.arange(min_lag_size, max_lag_size)
    # Calculate the array of the variances of the lagged differences
    #Calcula el arreglo de las variancias de las ventanas

    tau = [np.sqrt(np.std(np.subtract(ts[lag:], ts[:-lag]))) for lag in lags]

    '''
    tau2 = []
    for lag in lags:
        tau2.append(np.sqrt(np.std(np.subtract(ts[lag:], ts[:-lag]))))
    '''
    # Se estima por cuadrados minimos
    poly = np.polyfit(np.log(lags), np.log(tau), 1)
    # Return the Hurst exponent from the polyfit output
    #⟨|log(t+τ)−log(t)|2⟩∼τ2H
    return poly[0]*2.0

# Create a Gometric Brownian Motion, Mean-Reverting and Trending Series
gbm = np.log(np.cumsum(randn(100000))+1000)
mr = np.log(randn(100000)+1000)
tr = np.log(np.cumsum(randn(100000) + 1) + 1000)

# Output the Hurst Exponent for each of the above series
# and the price of Amazon (the Adjusted Close price) for
# the ADF test given above in the article
print("Hurst(GBM):%s" % hurst(gbm))
print("Hurst(MR):%s" % hurst(mr))
print("Hurst(TR):%s" % hurst(tr))


def getData(symbol):
    data = web.DataReader(symbol, data_source='yahoo', start='2017-05-01', end='2019-08-09')['Adj Close']
    data = pd.DataFrame(data)
    data.rename(columns={'Adj Close': 'price'}, inplace=True)
    return data


print("Hurst(MELI): %s" % hurst(getData("MELI")['price']))
print("Hurst(GGAL): %s" % hurst(getData("GGAL")['price']))
print("Hurst(SUPV): %s" % hurst(getData("SUPV.BA")['price']))
print("Hurst(YPF): %s" % hurst(getData("YPF")['price']))
print("Hurst(COME): %s" % hurst(getData("COME.BA")['price']))


a = np.arange(1, 50000)
b = np.arange(50000, 0,-1)
c = np.concatenate((a, b))
print("Hurst(Prueba): %s" % hurst(np.tan(c)))

