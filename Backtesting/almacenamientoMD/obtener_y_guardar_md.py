#coding=utf-8

from PrimaryApi.websocketPackage.websocketclient_v3 import WebSocketClient

import time

from almacenamientoMD.almacenadorMD import AlmacenarMarketDataSqLite, AlmacenarMarketDataArchivo, MarketData

start_time = time.time()
end_time = time.time()
# Inicializamos el cliente WebSocket
urlLogin = "http://api.remarkets.primary.com.ar/auth/getToken"
urlWebSocket = "ws://api.remarkets.primary.com.ar/"
wsClient = WebSocketClient(urlLogin, "fernandojcova2501", "nmhjvU6)", "REM2501", urlWebSocket)

symbols = ['DoNov19', 'MERV - XMEV - GGAL - 48hs', 'WTISep19', 'MERV - XMEV - AY24 - CI', 'MERV - XMEV - AY24D - 48hs']

conexion_db = AlmacenarMarketDataSqLite("mydb.db")
conexion_db.abrir_conexion()

conexion_archivo = AlmacenarMarketDataArchivo("market_data")
conexion_archivo.abrir_conexion("w")


def gestionar_market_data(message):
    instrument = message["instrumentId"]
    market_data = message["marketData"]
    ts = time.strftime("%Y-%m-%d %H:%M:%S")
    symbol = instrument["symbol"]
    market = instrument["marketId"]
    last_price, last_size, bid_price, bid_size, offer_price, offer_size = None,None,None,None,None,None
    print(message)
    if "LA" in market_data and market_data["LA"]:
        last_price = market_data["LA"]["price"]
        last_size = market_data["LA"]["size"]
    if "OF" in market_data and len(market_data["OF"]) != 0:
        offer_price = market_data["OF"][0]["price"]
        offer_size = market_data["OF"][0]["size"]
    if "BI" in market_data and len(market_data["BI"]) != 0:
        bid_price = market_data["BI"][0]["price"]
        bid_size = market_data["BI"][0]["size"]

    md = MarketData(ts, market, symbol, last_price, last_size, bid_price, bid_size, offer_price, offer_size)
    conexion_archivo.insertar_registro(md)
    conexion_db.insertar_registro(md)


wsClient.md_subscription(gestionar_market_data, symbols, ['BI', 'OF', 'LA'])
#dejo corriendo por una hora

try:
    time.sleep(3600)
except KeyboardInterrupt:
    conexion_db.cerrar_conexion()
    conexion_archivo.cerrar_conexion()





