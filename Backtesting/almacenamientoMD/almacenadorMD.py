#coding=utf-8
import sqlite3
import threading


class MarketData:

    def __init__(self, ts, market, symbol, last_price = None, last_size=None, bid_price =None, bid_size = None, offer_price =None, offer_size=None ):
        self.ts = ts
        self.market = market
        self.symbol = symbol
        self.last_price = last_price
        self.last_size = last_size
        self.bid_price = bid_price
        self.bid_size = bid_size
        self.offer_price = offer_price
        self.offer_size = offer_size


    def obtener_encabezados_csv(self, separador = ","):
        return "timeSeries" + separador + "market" + separador + "symbol"+ separador + "last_price"+ separador + "last_size" + separador + "bid_price"+ separador + "bid_size" + separador + "offer_price"+ separador + "offer_size"


    def obtener_valores_separados(self, separador = ","):
        return str(self.ts) + separador + self.market + separador + self.symbol+ separador + str(self.last_price) + separador + str(self.last_size) + separador + str(self.bid_price) + separador + str(self.bid_size) + separador + str(self.offer_price) + separador + str(self.offer_size)





class AlmacenadorMarketData:

    def __init__(self):
        pass


    def guardar_registro(self, market_data):
        pass


    def abrir_conexion(self):
        pass

    def cerrar_conexion(self):
        pass

    def insertar_registro(self, market_data):
        pass




class AlmacenarMarketDataArchivo(AlmacenadorMarketData):


    def __init__(self, nombre_archivo):
        self.nombre_archivo = nombre_archivo


    def abrir_conexion(self, modo):
        self.archivo = open(self.nombre_archivo, modo)


    def cerrar_conexion(self):
        self.archivo.close()


    def insertar_registro(self, market_data):
        self.archivo.write(market_data.obtener_valores_separados() + '\n')


class AlmacenarMarketDataSqLite(AlmacenadorMarketData):

    def __init__(self, db_name):
        self.db_name = db_name
        self.lock = threading.Lock()



    def abrir_conexion(self):
        self.db = sqlite3.connect(self.db_name, check_same_thread=False)


    def cerrar_conexion(self):
        self.db.close()


    def insertar_registro(self, market_data):

        cursor = self.db.cursor()
        sql = 'INSERT INTO market_data(timeSeries, market, symbol, last_price, last_size, bid_price,' \
              ' bid_size, offer_price, offer_size) VALUES ("%s", "%s", "%s", "%s", "%s", "%s", "%s", ' \
              '"%s", "%s")' % \
              (market_data.ts, market_data.market, market_data.symbol, market_data.last_price,
               market_data.last_size, market_data.bid_price,  market_data.bid_size, market_data.offer_price,
               market_data.offer_size)
        self.lock.acquire()
        try:
            # Execute the SQL command
            cursor.execute(sql)
            # Commit your changes in the database
            print("Se ejecutó sin problemas")
            self.db.commit()
        except:
            # Rollback in case there is any error
            print("Hubo un problema, hago rollback")
            self.db.rollback()
            raise
        finally:
            self.lock.release()



def create_database(db):
    conn = sqlite3.connect(db)
    cursor = conn.cursor()
    cursor.execute('''CREATE TABLE `market_data` (
       id INTEGER PRIMARY KEY AUTOINCREMENT,
      `timeSeries` datetime NOT NULL,
      `market` varchar(45) NOT NULL,
      `symbol` varchar(45) NOT NULL,
      `last_price` decimal(16,6) NOT NULL,
      `last_size` decimal(16,6) NOT NULL,
      `bid_price` decimal(16,6) NOT NULL,
      `bid_size` decimal(16,6) NOT NULL,
      `offer_price` decimal(16,6) NOT NULL,
      `offer_size` decimal(16,6) NOT NULL
      
    );''')

    conn.commit()
    conn.close()



if __name__ == "__main__":

    db = "mydb.db"
    #create_database(db)
    conexion_db_sqlite = AlmacenarMarketDataSqLite(db)
    conexion_db_sqlite.abrir_conexion()


    conexion_archivo = AlmacenarMarketDataArchivo("market_data")
    conexion_archivo.abrir_conexion("w")


    md = MarketData('2017-10-31 14:21:50', "ROFX", "BMA", 90, 10, 89, 3, 91, 5)

    conexion_db_sqlite.insertar_registro(md)

    conexion_db_sqlite.cerrar_conexion()


    conexion_archivo.insertar_registro(md)
    conexion_archivo.cerrar_conexion()

