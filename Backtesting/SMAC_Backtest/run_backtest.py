# coding: utf-8
import pandas as pd
from Backtesting.SMAC_Backtest.movingAverageCrossover import MovingAverageCrossStrategy
from Backtesting.SMAC_Backtest.portfolio import Portfolio
import pandas_datareader.data as web

def cargar_datos(symbol):
    datos = pd.io.parsers.read_csv(
        symbol + ".csv",
        header=0, index_col=0, parse_dates=True,
        names=[
            "timestamp", "open", "high", "low",
            "close", "adjusted_close", "volume","dividend_amount", "split_coefficient"
        ])
    datos = datos.sort_index()
    return datos


def getPerformance(symbol):
    start_date = '2019-01-01'
    end_date = '2019-08-26'
    data_g = cargar_datos_web(symbol)
    ventana_corta = 10
    ventana_larga = 30
    # Dividimos los datos para inicializar la estrategia y para el backtest
    init_strategy = data_g[:ventana_larga]["Adj Close"].tolist()
    backtest_data = data_g[ventana_larga:]
    # Precio de Referencia inicial de los activos
    initial_market_data = dict()
    initial_market_data[symbol] = backtest_data["Adj Close"][0]
    # Creamos el portfolio y la Estrategia
    portfolio = Portfolio([symbol], start_date, initial_market_data, 1, 1000000)
    estrategia = MovingAverageCrossStrategy(symbol, portfolio, short_window=ventana_corta,
                                            long_window=ventana_larga, datos=init_strategy)
    # Ejecutamos la estrategia por cada md
    for index, row in backtest_data.iterrows():
        estrategia.calculate_signals(row["Adj Close"])
        portfolio.actualizar_portfolio(index, symbol, row["Adj Close"])
    portfolio.mostrar_performance()



def cargar_datos_web(symbol):
    f = web.DataReader(symbol, 'yahoo', '2019-01-01', '2019-06-20')
    return f


if __name__ == "__main__":


    # Cargamos los datos desde el archivo

    getPerformance("COME.BA")
    #getPerformance("AAPL")
    #getPerformance("GOOGL")
    #getPerformance("PAMP")

