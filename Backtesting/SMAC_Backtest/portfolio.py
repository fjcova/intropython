#coding=utf-8

import math
import pprint
import pandas as pd
import matplotlib.pyplot as plt

from SMAC_Backtest.performance import create_drawdowns, create_sharpe_ratio


class Portfolio():
    """
    Esta clase portfolio maneja la posición y el valor de mercado de todos los instrumentos.
    El DataFrame de posición almacena por fecha las
    cantidad de la posición tomada en cada instrumento.
    El DataFrame del portfolio almacena el efectivo y
    el valor del portafolio total y por instrumento.
    """

    def __init__(self, symbols, start_date, md_inicial, comision=0, capital_inicial=1000000.0):
        """
        Inicializa el portfolio.
        Parameters:
        symbols - simbolos
        start_date - fecha inicial del portfolio.
        md_inicial - diccionario con los precios de referencia iniciales.
        comision - comision por operacion.
        capital_inicial - capital inicial de la estrategia.
        """

        self.symbol_list = symbols
        self.start_date = start_date
        self.capital_inicial = capital_inicial
        self.comision = comision

        # Inicializo precio inicial de los instrumentos
        # (para luego calcular el valor del portafolio)
        self.last_market_data = dict()
        for s in self.symbol_list:
            if s in md_inicial.keys():
                self.last_market_data[s] = md_inicial[s]
            else:
                self.last_market_data[s] = 0

        self.posicion_historica = self.construir_posicion_historica()
        self.posicion_actual = dict((k, v) for k, v in [(s, 0) for s in self.symbol_list])

        self.portfolio_historico = self.construir_portfolio_historico()
        self.portfolio_actual = self.construir_portfolio_actual()

    def construir_posicion_historica(self):
        """
        Creamos la lista con las posiciones historicas.
        La primer posicón la creamos con la fecha inicial
        y todas las posiciones en 0.
        """

        d = dict((k, v) for k, v in [(s, 0) for s in self.symbol_list])
        d["datetime"] = self.start_date
        return [d]

    def construir_portfolio_historico(self):
        """
        Creamos la lista con los portfolios historicos.
        El primer portfolio se inicializa con el valor
        de cada instrumento en 0.0, la fecha inicial,
        0 comisiones pagadas, cash y el total
        del valor del portfolio inicial igual al capital inicial
        """

        d = dict((k, v) for k, v in [(s, 0.0) for s in self.symbol_list])
        d["datetime"] = self.start_date
        d["cash"] = self.capital_inicial
        d["comisiones"] = 0.0
        d["total"] = self.capital_inicial
        return [d]

    def construir_portfolio_actual(self):
        """
        Construimos el portfolio actual que almacena el valor de
        nuestra cartera durante el periodo.
        """

        d = dict((k, v) for k, v in [(s, 0.0) for s in self.symbol_list])
        d["cash"] = self.capital_inicial
        d["comisiones"] = 0.0
        d["total"] = self.capital_inicial

        return d

    def actualizar_portfolio(self, fecha, symbol, last):
        """
        Adds a new record to the positions matrix for the current
        market data bar. This reflects the PREVIOUS bar, i.entries. all
        current market data at this stage is known (OHLCV).
        Makes use of a MarketEvent from the events queue.
        """

        # Actualizamos la posicion
        # ================
        self.last_market_data[symbol] = last

        dp = dict((k, v) for k, v in [(s, 0) for s in self.symbol_list])
        dp["datetime"] = fecha
        for s in self.symbol_list:
            dp[s] = self.posicion_actual[s]

        # Agregamos la posición actual al historico
        self.posicion_historica.append(dp)

        # Actualizamos el portfolio
        # ===============
        dh = dict((k, v) for k, v in [(s, 0) for s in self.symbol_list])
        dh["datetime"] = fecha
        dh["cash"] = self.portfolio_actual["cash"]
        dh["comisiones"] = self.portfolio_actual["comisiones"]
        dh["total"] = self.portfolio_actual["cash"]
        for s in self.symbol_list:
            # Aproximamos al valor real de la posicion
            # Valuada con el last del instrumento
            market_value = self.posicion_actual[s] * \
                           self.last_market_data[s]
            dh[s] = market_value
            dh["total"] += market_value
        # Agregamos al portfolio historico
        self.portfolio_historico.append(dh)

    def actualizar_posicion_por_trade(self, symbol, side, cantidad):
        """
        Actualiza la posicion actual en base a los datos del trade.
        Parametros:
        side - BUY o SELL
        symbol - simbolo
        cantidad - cantidad del trade
        """

        # Verificamos si el trade fue una compra o venta
        fill_dir = 0
        if side == "BUY":
            fill_dir = 1
        elif side == "SELL":
            fill_dir = -1
        # Actualizamos la posicion con la nueva cantidad
        self.posicion_actual[symbol] += fill_dir * cantidad

    def actualizar_portfolio_por_trade(self, symbol, side, cantidad, precio):
        """
        Actualiza el portfolio actual en base a los datos del trade.
        Parametros:
        side - BUY o SELL
        symbol - simbolo
        cantiad - cantidad del trade
        precio - precio del trade
        """

        # Check whether the fill is a buy or sell
        fill_dir = 0
        if side == "BUY":
            fill_dir = 1
        elif side == "SELL":
            fill_dir = -1
        # Actualizamos el portfolio con los nuevos datos
        monto = fill_dir * precio * cantidad
        self.portfolio_actual[symbol] += monto
        self.portfolio_actual["comisiones"] += self.comision
        self.portfolio_actual["cash"] -= (monto + self.comision)

    def sim_trade(self, signal, symbol, precio):
        """
        Simula la ejecución de un trade.
        Parametros:
        signal - señal de ir long o salir del mercado
        symbol - simbolo
        precio - precio del trade
        """

        cantidad = 0
        cur_quantity = self.posicion_actual[symbol]
        side = ""
        if signal == "LONG" and cur_quantity == 0:
            side = "BUY"
            cantidad = math.floor(self.portfolio_actual["cash"]/precio)
        elif signal == "EXIT" and cur_quantity > 0:
            side = "SELL"
            cantidad = cur_quantity

        # Actualizamos porfolio y posicion actuales con los datos del trade
        self.actualizar_posicion_por_trade(symbol, side, cantidad)
        self.actualizar_portfolio_por_trade(symbol, side, cantidad, precio)

    def crear_dataframe_curva_capital(self):
        """
        Creamos un DataFrame a partir del portfolio historico
        Agregamos los retornos por periodo y acumulados
        """

        curva = pd.DataFrame(self.portfolio_historico)
        curva.set_index("datetime", inplace = True)
        curva["retornos"] = curva["total"].pct_change()
        curva["curva_capital"] = (1.0 + curva["retornos"]).cumprod()
        self.curva_capital = curva

    def output_summary_stats(self):
        """
        Creamos las estadisticas de performace del portafolio.
        """

        pnl = self.curva_capital["curva_capital"]

        total_return = pnl[-1]
        returns = self.curva_capital["retornos"]

        # Calculamos Ratio de Sharpe y Drawdown
        sharpe_ratio = create_sharpe_ratio(returns)
        drawdown, max_dd, dd_duration = create_drawdowns(pnl)
        self.curva_capital["drawdown"] = drawdown
        stats = [("Retorno Total", "%0.2f%%" % \
                  ((total_return - 1.0) * 100.0)),
                 ("Sharpe Ratio", "%0.2f" % sharpe_ratio),
                 ("Max Drawdown", "%0.2f%%" % (max_dd * 100.0)),
                 ("Drawdown Duration", "%d" % dd_duration)]
        self.curva_capital.to_csv("curva_capital.csv")
        return stats

    def mostrar_performance(self):
        """
        Muestra la performance del backesting de la estrategia.
        """

        print("Creamos la curva de capital...")
        self.crear_dataframe_curva_capital()
        print("Creamos las estadisticas...")
        stats = self.output_summary_stats()
        print(self.curva_capital.tail(100))

        print(stats)
        pprint.pprint(stats)

        # Graficamos la Curva de Capital
        plt.plot(self.curva_capital["curva_capital"])
        plt.legend("Curva de Capital")
        plt.xlabel('Tiempo')
        plt.ylabel('Valor')
        plt.title('Curva de Capital')
        plt.show()
