#coding=utf-8
import numpy as np


class MovingAverageCrossStrategy():
    """
    Implementa una estrategia básica Moving Average Crossover.
    Por defecto las ventanas short/long son de 10/40 periodos respectivamente.
    """

    def __init__(self, symbol, portfolio, short_window=10, long_window=40, datos = []):
        """
        Inicializa la Moving Average Cross Strategy.
        Parametros:
        symbol - ticker del activo a utilizar
        portfolio - objeto portfolio utilizado para generar las ordenes
        short_window - periodo corto para cacular la media movil
        long_window - periodo largo para cacular la media movil
        datos - arreglo con datos hitoricos
        """

        self.symbol = symbol
        self.portfolio = portfolio
        self.datos = datos
        self.tamanio_corta = short_window
        self.tamanio_larga = long_window
        # Esta estrategia tiene 2 estados OUT y LONG
        self.estado = "OUT"

    def calculate_signals(self, last):
        """
        Genera un señales "LONG"/"EXIT" basado en
        la media movil simple.
        Parametros
        last - último precio del instrumento.
        """

        self.datos.append(last)
        if len(self.datos) >= self.tamanio_larga:

            media_corta = np.mean(self.datos[-self.tamanio_corta:])
            media_larga = np.mean(self.datos[-self.tamanio_larga:])

            if media_corta > media_larga and self.estado == "OUT":
                sig_dir = "LONG"
                self.portfolio.sim_trade(sig_dir, self.symbol, last)
                self.estado = "LONG"
            elif media_corta < media_larga and self.estado == "LONG":
                sig_dir = "EXIT"
                self.portfolio.sim_trade(sig_dir, self.symbol, last)
                self.estado = "OUT"
