#coding=utf-8

import numpy as np
import pandas as pd


def create_sharpe_ratio(returns, periods=252):
    """
    Creamos el Ratios de Sharpe para los retorno, suponemos que el
    benchmark es cero.
    Parametros:
    returns - Serie que representa los retornos por periodo.
    periods - Por día (252), Por hora (252*6.5), Por minuto (252*6.5*60) etc.
    """
    return np.sqrt(periods) * (np.mean(returns)) / np.std(returns)

def create_drawdowns(pnl):
    """
    Calculamos el periodo de caida más grande de la curva de retornos
    así como la duración.
    Parameters:
    pnl - Serie pandas que representa retornos porcentuales.
    Returns:
    drawdown, duration - Caída más grande del retorno y duración.
    """

    # Creamos el High Water Mark - lista con los pisos del retorno max
    hwm = [0]

    # Creamos las series drawdown y duration
    idx = pnl.index
    drawdown = pd.Series(index = idx)
    duration = pd.Series(index = idx)

    # Recorremos la curva de retornos
    for t in range(1, len(idx)):
        hwm.append(max(hwm[t-1], pnl[t]))
        drawdown[t]= (hwm[t]-pnl[t])
        duration[t]= (0 if drawdown[t] == 0 else duration[t-1]+1)
    return drawdown, drawdown.max(), duration.max()