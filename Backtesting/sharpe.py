# -- coding: utf-8 --

import numpy as np
from alpha_vantage.timeseries import TimeSeries


API_KEY = '9VSB693EOW5IOZ6V'

'''El ratio de sharpe se calcula restando la rentabilidad de un activo sin riesgo 
 a la rentabilidad de nuestra inversión 
y dividendo el resultado entre el riesgo, 
calculado como la desviación típica de la rentabilidad de la inversión.'''



def annualised_sharpe(returns, N=252):
    """
    Calculamos el Sharpe Ratio anualizado de los retornos
    basandonos en N periodos de trading anuales
    """
    return np.sqrt(N) * returns.mean() / returns.std()




def equity_sharpe(ticker):

    ts = TimeSeries(key=API_KEY, output_format='pandas', indexing_type='date')
    pdf, meta_data = ts.get_daily_adjusted(symbol=ticker, outputsize="full")
    #Genera una nueva columna con el % de variacion de un campo con respecto al anterior
    pdf['daily_ret'] = pdf['5. adjusted close'].pct_change()
    pdf['excess_daily_ret'] = pdf['daily_ret'] - 0.02/252
    pdf = pdf.dropna()

    return annualised_sharpe(pdf['excess_daily_ret'])

if __name__ == '__main__':
    print (equity_sharpe('GOOGL'))