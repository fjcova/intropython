import pandas as pd
from pandas_datareader import data as web
import matplotlib.pyplot as plt

data = web.DataReader('MELI', data_source='yahoo', start='2018-05-01', end='2019-08-09')['Adj Close']
data = pd.DataFrame(data)
data.rename(columns={'Adj Close': 'price'}, inplace=True)
data['SMA1'] = data['price'].rolling(5).mean()
data['SMA2'] = data['price'].rolling(12).mean()

data.plot(title='GGAL stock price | 42 & 252 days SMAs', figsize=(10,6))

plt.show()

