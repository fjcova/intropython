#coding=utf-8
from alpha_vantage.timeseries import TimeSeries

from base.BackTestBase import BackTestingBaseClass
from base.BackTestLongShort import BacktestLongShort

API_KEY = 'KJJY75LNBILR2L1I'
timeSeries = TimeSeries(key=API_KEY, output_format='pandas', indexing_type='date')
ggal, meta_data_ggal = timeSeries.get_daily_adjusted(symbol='GGAL.BA', outputsize="compact")

class EstrategiaSesgada(BacktestLongShort):

    def run_strategy(self):
        msg = '\n\nRunning strategy '
        msg += '\nFixed costs %.2f | ' % self.ftc
        msg += 'proportional costs %.4f' % self.ptc
        print(msg)
        print('=' * 55)
        self.position = 0  # initial neutral position
        self.amount = self._amount  # reset initial capital

        self.go_long(0, amount='all');
        self.position = 1

        for bar in range(len(self.data)):
            if self.position in [0, -1]:
                if self.data['price'].ix[bar] <= 80:
                    self.go_long(bar, amount='all')
                    self.position = 1  # long position
            elif self.position in [0, 1]:
                if self.data['price'].ix[bar] >= 138.00:
                    self.go_short(bar, amount='all')
                    self.position = -1  # short position
        self.close_out(bar)


if __name__ == "__main__":

    def run_strategies(lobt):
        lobt.verbose = True
        lobt.run_strategy()

    lobt = EstrategiaSesgada('GGAL', '2019-01-01', '2019-09-01', 10000, 10.0, 0.005) # 10 USD fix, 0.5% variable
    run_strategies(lobt)
