#coding=utf-8
from websocketclient_v3 import WebSocketClient


symbol_short = 'DOSep19'
symbol_long = 'DOOct19'
symbol_spread = 'DOP 09/10 19'


symbols = [symbol_short, symbol_long, symbol_spread]

urlLogin = "http://api.remarkets.primary.com.ar/auth/getToken"
urlWebSocket = "ws://api.remarkets.primary.com.ar/"

wsClient = WebSocketClient(urlLogin, "fernandojcova2501", "nmhjvU6)", "REM2501", urlWebSocket)

df = {'SBPrice': None, 'SBSize': None, 'SOPrice': None, 'SOSize': None,
                           'LBPrice': None, 'LBSize': None, 'LOPrice': None, 'LOSize': None,
                           'SPBPrice': None, 'SPBSize': None, 'SPOPrice': None, 'SPOSize': None}
entries = ['BI', 'OF']

fixed_cost = 1

retorno = 0
costos_transaccion = 0
tamaño_contratos = 1000


def resume():
    print("Retorno sin costos de transaccion: " + str(retorno))
    print("Costos de transaccion: " + str(costos_transaccion))
    print("Retorno con costos de transaccion: " + str(retorno - costos_transaccion))



def on_market_data_update(message):
    global retorno, costos_transaccion
    print (message)
    symbol = message['instrumentId']['symbol']
    market_data = message["marketData"]
    bids = market_data["BI"]
    offers = market_data["OF"]
    validar = False
    if (symbol == symbol_short):
        if (len(bids) > 0):
            if (df['SBPrice'] != bids[0]['price']):
                df['SBPrice']= bids[0]['price']
                validar = True
            df['SBSize'] = bids[0]['size']
        else:
            df['SBPrice'] = None
            df['SBSize'] = None
        if (len(offers) > 0):
            if (df['SOPrice'] != offers[0]['price']):
                df['SOPrice']= offers[0]['price']
                validar = True
            df['SOSize'] = offers[0]['size']
        else:
            df['SOPrice'] = None
            df['SOSize'] = None
    elif (symbol == symbol_long):
        if (len(bids) > 0):
            if (df['LBPrice'] != bids[0]['price']):
                df['LBPrice']= bids[0]['price']
                validar = True
            df['LBSize'] = bids[0]['size']
        else:
            df['LBPrice'] = None
            df['LBSize'] = None
        if (len(offers) > 0):
            if (df['LOPrice'] != offers[0]['price']):
                df['LOPrice']= offers[0]['price']
                validar = True
            df['LOSize'] = offers[0]['size']
        else:
            df['LOPrice'] = None
            df['LOSize'] = None
    elif (symbol == symbol_spread):
        if (len(bids) > 0):
            if (df['SPBPrice'] != bids[0]['price']):
                df['SPBPrice']= bids[0]['price']
                validar = True
            df['SPBSize'] = bids[0]['size']
        else:
            df['SPBPrice'] = None
            df['SPBSize'] = None
        if (len(offers) > 0):
            if (df['SPOPrice'] != offers[0]['price']):
                df['SPOPrice']= offers[0]['price']
                validar = True
            df['SPOSize'] = offers[0]['size']
        else:
            df['SPOPrice'] = None
            df['SPOSize'] = None
    #Comprar Corto, vender Largo, Comprar Pase
    #Offer Corto, Bid Largo, Offer Pase
    #SOPrice, LBPrice, SPOPrice
    if(validar):
        SOPrice = df['SOPrice']
        LBPrice = df['LBPrice']
        SPOPrice = df['SPOPrice']
        SBPrice = df['SBPrice']
        LOPrice = df['LOPrice']
        SPBPrice = df['SPBPrice']
        if (SOPrice and LBPrice and SPOPrice):
            delta = LBPrice - SOPrice > SPOPrice
            if (delta):
                quantity = min(df['SOSize'], df['LBSize'], df['SPOSize'])
                retorno += abs(LBPrice - SOPrice - SPOPrice) * quantity * tamaño_contratos
                #Hago 4 transacciones por arbitraje
                costos_transaccion += fixed_cost*4
                print("Precio Corto: {0}, Precio Largo: {1}, Precio Pase: {2}".format(SOPrice,LBPrice, SPOPrice))
                print("Comprar {0}, Vender {1}, Comprar {2}. Cantidad : {3}".format(symbol_short, symbol_long, symbol_spread, quantity))
        if (SBPrice and LOPrice and SPBPrice):
            # Vender Corto, Comprar Largo, Vender Pase
            # Bid Corto, Offer Largo, Bid Pase
            delta = LOPrice - SBPrice < SPBPrice
            if (delta):
                quantity = min(df['LOSize'], df['SBSize'], df['SPBSize'])
                retorno += abs(SPBPrice +SBPrice - LOPrice) * quantity * tamaño_contratos
                #Hago 4 transacciones por arbitraje
                costos_transaccion += fixed_cost*4
                print("Precio Corto: {0}, Precio Largo: {1}, Precio Pase: {2}".format(SBPrice, LOPrice, SPBPrice))
                print("Vender {0}, Comprar {1}, Vender{2}. Cantidad : {3}".format(symbol_short, symbol_long, symbol_spread, quantity))
    else:
        print("Llego market data con mismo precio no hago nada")


try:
    wsClient.md_subscription(on_market_data_update, symbols, entries)
    while(True):
        pass
except Exception:
    pass
finally:
    wsClient.cerrar()
    resume()



