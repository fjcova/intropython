#coding=utf-8

#
# Python Script
# with Tick Data Server
#
# Python for Algorithmic Trading
# (c) Dr. Yves J. Hilpisch
# The Python Quants GmbH
#
import zmq
import math
import time
import random

context = zmq.Context()
socket = context.socket(zmq.PUB)
socket.bind('tcp://127.0.0.1:8080')


random.seed(100000)

class StockPrice:

    def __init__(self, symbol, value, sigma, r):
        self.symbol = symbol
        self.t = time.time()
        self.value = value
        self.sigma = sigma
        self.r = r


    def simulate_value(self):
        ''' Generates a new, random stock price.
        '''
        t = time.time()
        dt = (t - self.t) / (252 * 8 * 60 * 60)
        dt *= 100
        self.t = t
        self.value *= math.exp((self.r - 0.5 * self.sigma ** 2) * dt +
                               self.sigma * math.sqrt(dt) * random.gauss(0, 1))
        return self.value


sp2 = StockPrice('AAPL', 100, 0.4, 0.05)
sp = StockPrice('SUPV', 100, 0.6, 0.02)

spggal = StockPrice('GGAL', 60, 0.3, 0.04)
while True:
    msg = '%s %s' % (sp.symbol, sp.simulate_value())
    print(msg)
    socket.send_string(msg)
    msg = '%s %s' % (sp2.symbol, sp2.simulate_value())
    print(msg)
    socket.send_string(msg)
    msg = '%s %s' % (spggal.symbol, spggal.simulate_value())
    print(msg)
    socket.send_string(msg)
    time.sleep(random.random() *5)