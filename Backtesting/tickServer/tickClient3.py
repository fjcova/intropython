#coding=utf-8


#
# Python Script
# with Tick Data Client
#
# Python for Algorithmic Trading
# (c) Dr. Yves J. Hilpisch
# The Python Quants GmbH
#
import zmq
context = zmq.Context()
socket = context.socket(zmq.SUB)
socket.connect('tcp://127.0.0.1:8080')
socket.setsockopt_string(zmq.SUBSCRIBE, 'DONov19')




while True:
  data = socket.recv_string()
  print(data)