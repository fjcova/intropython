#!/usr/bin/python
# -*- coding: utf-8 -*-

import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import pandas as pd
from alpha_vantage.timeseries import TimeSeries
import pprint
import statsmodels.api as sm
import statsmodels.tsa.stattools as ts


def plot_price_series(df, ts1, ts2):
    months = mdates.MonthLocator() # every month
    fig, ax = plt.subplots()
    ax.plot(df.index, df[ts1], label=ts1)
    ax.plot(df.index, df[ts2], label=ts2)
    ax.xaxis.set_major_locator(months)
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%b %Y'))
    #ax.set_xlim(datetime.datetime(2016, 9, 1), datetime.datetime(2017, 9, 1))
    ax.grid(True)
    fig.autofmt_xdate()
    plt.xlabel('Month/Year')
    plt.ylabel('Price ($)')
    plt.title('%s and %s Daily Prices' % (ts1, ts2))
    plt.legend()
    plt.show()


def plot_scatter_series(df, ts1, ts2):
    plt.xlabel("%s Price ($)" % ts1)
    plt.ylabel("%s Price ($)" % ts2)
    plt.title("%s and %s Price Scatterplot" % (ts1, ts2))
    plt.scatter(df[ts1], df[ts2])
    plt.show()


def plot_residuals(df):
    months = mdates.MonthLocator() # every month
    fig, ax = plt.subplots()
    ax.plot(df.index, df["res"], label="Residuals")
    #ax.xaxis.set_major_locator(months)
    #ax.xaxis.set_major_formatter(mdates.DateFormatter("%b %Y"))
#    ax.set_xlim(datetime.datetime(2016, 9, 1), datetime.datetime(2017, 9, 1))
    ax.grid(True)
    fig.autofmt_xdate()
    plt.xlabel("Month/Year")
    plt.ylabel("Price ($)")
    plt.title("Residual Plot")
    plt.legend()
    plt.plot(df["res"])
    plt.show()

if __name__ == "__main__":
    API_KEY = '9VSB693EOW5IOZ6V'
    timeSeries = TimeSeries(key=API_KEY, output_format='pandas', indexing_type='date')
    SUPV, meta_data_SUPV = timeSeries.get_daily_adjusted(symbol='SUPV.BA', outputsize="compact")
    bma, meta_data_bma = timeSeries.get_daily_adjusted(symbol='BMA.BA', outputsize="compact")
    df = pd.DataFrame(index=SUPV.index)

    df["SUPV"] = SUPV["5. adjusted close"]
    df["BMA"] = bma["5. adjusted close"]
    df = df.drop(df[df["SUPV"] <= 0].index)
    df = df.drop(df[df["BMA"] <= 0].index)
    df["returnBMA"] = df["BMA"].pct_change()
    df["returnSUPV"] = df["SUPV"].pct_change()
    df.dropna()

    # Plot the two time series
    plot_price_series(df, "SUPV", "BMA")

    # Display a scatter plot of the two time series
    plot_scatter_series(df, "returnBMA", "returnSUPV")
    # Calculate optimal hedge ratio "beta"
    res = sm.OLS(endog=df["SUPV"], exog=df["BMA"]).fit()
    beta_hr = res.params[0]
    print (beta_hr)

    # Calculate the residuals of the linear combination
    df["res"] = df["SUPV"] - beta_hr*df["BMA"]
    df.dropna()
    # Plot the residuals
    #plot_residuals(df)

    # Calculate and output the CADF test on the residuals
    cadf = ts.adfuller(df["res"])
    pprint.pprint(cadf)
