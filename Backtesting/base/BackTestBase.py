#coding=utf-8

import requests
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

URL_ALPHA = "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&symbol={symbol}&apikey=KJJY75LNBILR2L1I&datatype=csv"

class BackTestingBaseClass:


    def __init__(self, symbol, start, end, amount, ftc=0.0, ptc=0.0):
        self.symbol = symbol
        self.start = start
        self.end = end
        #monto inicial
        self._amount = amount
        #plata disponible
        self.amount = amount
        #fixed transaction costs
        self.ftc = ftc
        #proportional transaction costs
        self.ptc = ptc
        #cantidad inicial del contrato
        self.units = 0
        #posicion con respecto al mercado
        self.position = 0
        #cantidad de trades
        self.trades = 0
        #Full output o no
        self.verbose = True
        #metodo que va a obtener la data inicial
        self.get_data()


    def get_data(self):
        raw = pd.io.parsers.read_table(URL_ALPHA.format(symbol=self.symbol + ".BA"), ",",
                                       header=0, index_col=0, parse_dates=False,names=[
            "timestamp", "open", "high", "low",
            "close", "adjusted_close", "volume","dividend_amount", "split_coefficient" ])
        raw = pd.DataFrame(raw).filter(['adjusted_close'])
        raw.rename(columns={'adjusted_close': 'price'}, inplace=True)
        raw['return'] = pd.Series(np.log(raw['price'] / raw.shift(1)['price']))
        raw['return perc'] = raw['price'].pct_change()
        self.data = raw.dropna()
        self.data = self.data.sort_index()
        self.data = self.data[self.start:self.end]




    def plot_data(self):
        self.data['price'].plot(figsize=(10, 6), title=self.symbol)
        plt.show()
        print("plotting")

    def print_balance(self, date=''):
        ''' Print out current cash balance info.
        '''
        print('%s | current cash balance %8d' % (date[:10], self.amount))

    def get_date_price(self, bar):
        ''' Return date and price for bar.
        '''

        date = str(self.data.index[bar])
        price = self.data.price.ix[bar]
        return date, price

    def place_buy_order(self, bar, units=None, amount=None):
        ''' Place a buy order.
        '''

        date, price = self.get_date_price(bar)
        if units is None:
            units = np.math.floor(amount / price)
        self.amount -= (units * price) * (1 + self.ptc) + self.ftc
        self.units += units
        self.trades += 1
        if self.verbose:
            print('%s | buying %4d units at %7.2f' %
              (date[:10], units, price))
            self.print_balance(date)

    def place_sell_order(self, bar, units=None, amount=None):
        ''' Place a sell order.
        '''

        date, price = self.get_date_price(bar)
        if units is None:
            units = np.math.floor(amount / price)
        self.amount += (units * price) * (1 - self.ptc) - self.ftc
        self.units -= units
        self.trades += 1
        if self.verbose:
            print('%s | selling %4d units at %7.2f' %
                  (date[:10], units, price))
        self.print_balance(date)

    def close_out(self, bar):
        ''' Closing out a long or short position.
        '''

        date, price = self.get_date_price(bar)
        self.amount += self.units * price
        if self.verbose:
            print('%s | inventory %d units at %.2f' % (date[:10],
                                                       self.units, price))
        if self.verbose:
            print('=' * 55)
        print('Final balance [$] % 13.2f' % self.amount)
        print('Net Performance [%%] %13.2f' % ((self.amount - self._amount) /
                                               self._amount * 100))
        print('=' * 55)


if __name__ == '__main__':

    bb = BackTestingBaseClass('GGAL', '2010-1-1', '2019-10-31', 10000)

    print(bb.data.info())

    print(bb.data.tail())

    bb.plot_data()