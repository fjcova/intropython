# coding=utf-8

# Palabras claves como argumentos
def loro(tension, estado='muerto', accion='explotar', tipo='Azul Nordico'):
    print( "-- Este loro no va a", accion,)
    print( "si le aplicas", tension, "voltios.")
    print( "-- Gran plumaje tiene el", tipo)
    print( "-- Esta", estado, "!")


loro(1000)
loro(accion='EXPLOTARRRRR', tension=1000000)
loro('mil', estado='boca arriba')
loro('un millon', 'rostizado', 'saltar')

#loro()                      # falta argumento obligatorio
#loro(tension=5.0, 'muerto') # argumento nombrado seguido de uno posicional
#loro(110, tension=220)      # valor duplicado para argumento
#loro(actor='Juan Garau')    # palabra clave desconocida
import sys


def ventadequeso(tipo, *argumentos, **palabrasclaves):
    print( "-- ¿Tiene", tipo, "?")
    print( "-- Lo siento, nos quedamos sin", tipo)
    for arg in argumentos:
        print( arg)
    print( "-" * 40)
    #claves = list(palabrasclaves.keys())
    #claves.sort()
    for c in palabrasclaves.keys():
        print( c, ":", palabrasclaves[c])



a = {"cliente":"Juan Garau", "puesto":"Vendedor", "empresa":"Primary"}
print(a["puesto"])

ventadequeso("Limburger", **a)


def fun(a, b, c, *d, **e):
    print( a, b, c)
    for x in d:
        print("lista"+ str(x))
    for x in e.keys():
        print("diccionario", e[x])


fun(1, 2, 2, 3, 4, 'param1', m='pat1', n='pat2', o='pat3', q='pat4', p='pat5')
