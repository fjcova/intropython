# coding:utf-8

# Genera una iterador conteniendo progresiones aritméticas
for x in range(10):
    print(x)

# Hacer que el rango empiece con otro número
for x in range(5, 10):
    print(x)

# Especificar un incremento diferente
for x in range(0, 10, 3):
    print(x)

# Rangos Negativos
for x in range(-10, -100, -30):
    print(x)

# Combinar range() y len() para iterar sobre los índices de una secuencia
a = ['Intro', 'Programacion', 'python', 'corderito']
for index in range(len(a)):
    print (index, a[index])


#crear una lista
x = list(range(5))
print (x)