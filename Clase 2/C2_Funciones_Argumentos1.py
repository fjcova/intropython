# coding=utf-8

# Argumentos con valores por omisión
def pedir_confirmacion(prompt, otro, reintentos=4, queja='Si o no, por favor!'):
    #El parametro otro existe pero no se usa. Si no se usa tiene sentido tenerlo?
    while True:
        ok = input(prompt)
        if ok in ('s', 'S', 'si', 'Si', 'SI'):
            return True
        if ok in ('n', 'no', 'No', 'NO'):
            return False
        reintentos = reintentos - 1
        if reintentos < 0:
            raise IOError('usuario duro')
        print (queja)


i = 5


def f(arg=i):
    print (arg)


i = 6
f()


def f(a, L=[]):
    L.append(a)
    return L


print (f(1))
print (f(2))
print (f(3))


def f(a, L=None):
    if L is None:
        L = []
    L.append(a)
    return L


a = pedir_confirmacion("Ingrese Si o No:", 1, queja='Pone un valor correcto por favor!!!')
a = pedir_confirmacion("", "", 1, )N

