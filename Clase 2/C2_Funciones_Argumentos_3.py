# coding=utf-8

# Listas de argumentos arbitrarios
def muchos_items(archivo, separador, *args):
    archivo.write(separador.join(args))


# Desempaquetando una lista de argumentos
for x in range(3, 6):
    print(x)# llamada normal con argumentos separados
args = [3, 6]
for x in range(*args):
    print(x)# llamada con argumentos desempaquetados de una lista



def f(a, b='rostizado', c='explotar'):
    print ("-- Este loro no va a", a,)
    print ("si le aplicas", b, "voltios.",)
    print ("Esta", c, "!")


d = {"a": "valor1", "b": "valor2", "c": "valor3"}
f(**d)
