#coding=utf-8
'''
13 - Escribir dos funciones que resuelvan los siguientes problemas:
a) Dado un número entero n , indicar si es par o no.
b) Dado un número entero n , indicar si es primo o no.

'''


def esPar(numero):
    return numero%2 == 0

def esImpar(numero):
    return not esPar(numero)

print (esPar(2))
print (esPar(18))
print (esPar(27))

def esPrimo(numero):
    if(numero == 1 or numero == 2):
        return True
    elif(numero <= 0):
        return False
    else:
        for x in range(2,numero,1):
            if(numero%x == 0):
                return False
        return True

for x in range(101):
    print (x,esPrimo(x))


