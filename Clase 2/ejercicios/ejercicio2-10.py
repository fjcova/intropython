# coding: utf-8

'''
10 -
Usando las funciones del ejercicio anterior, escribir un programa
que pida al usuario dos intervalos
expresados en horas, minutos y segundos,
sume sus duraciones, y muestre
por pantalla la duración total en horas, minutos y segundos.
'''


def duracion_segundos(horas, minutos, segundos):
    return horas * 60 * 60 + minutos * 60 + segundos


def duracion_hms(seg):
    horas = int(seg / (60 * 60));
    minutos = int((seg - horas * 60 * 60) / 60)
    segundos = seg - horas * 60 * 60 - minutos * 60
    return [horas, minutos, segundos]


def calcular_segundos():
    horas1 = int(input("Ingrese el Primer Intervalo: \nHoras: "))
    minutos1 = int(input("Minutos: "))
    segundos1 = int(input("Segundos: "))
    duracion1 = duracion_segundos(horas1, minutos1, segundos1)

    horas2 = int(input("\n\nIngrese el Segundo Intervalo: \nHoras: "))
    minutos2 = int(input("Minutos: "))
    segundos2 = int(input("Segundos: "))
    duracion2 = duracion_segundos(horas2, minutos2, segundos2)

    return duracion_hms(duracion1 + duracion2)


print (calcular_segundos())
