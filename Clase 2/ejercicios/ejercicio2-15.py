#coding=utf-8

'''
15 - Escribir una función que reciba por parámetro una dimensión n , e imprima la
matriz identidad correspondiente a esa dimensión.
'''


def imprimir_matriz_identidad(dimension):
    matriz = ''
    for x in range(dimension):
        for y in range(dimension):
            if ( x == y):
                matriz += '1 '
            else:
                matriz += '0 '

            if(y == dimension-1):
                matriz += '\n'
    print(matriz)


imprimir_matriz_identidad(5)
imprimir_matriz_identidad(3)