#coding=utf-8
'''
14 - Escribir una implementación propia de la función abs , que devuelva el valor absoluto de cualquier valor que reciba.
'''

def my_abs(numero):
    if(numero < 0):
        return numero*-1
    return numero

print (my_abs(2))
print (my_abs(-2))

print (abs(-2))
print (abs(2))