#coding=utf-8
'''
6​ ​ - ​ ​ Escribir​ ​ un​ ​ programa​ ​ para​ ​ tome​ ​ una​ ​ cantidad​ ​ m ​ ​ de​ ​ valores​ ​
ingresados​ ​ por​ ​ el​ ​ usuario,​ ​ a cada​ ​ uno​ ​ le​ ​ calcule​ ​ el​ ​ factorial​ ​ e ​ ​ imprima​ ​ el​ ​ resultado​ ​
junto​ ​ con​ ​ el​ ​ número​ ​ de​ ​ orden correspondiente.
'''
def factorial(n):
   fact = 1
   for i in range(1,n+1):
       fact = fact * i
   return (fact)

def ingreseValores(*items):
   for i, valor in enumerate(items):
       fact = factorial(valor)
       print (i, fact)


def ingreseValores2(*items):
   a = list(items)
   a.sort()
   for i in a:
       print (factorial(i), items.index(i))


a = [2,5,6,7]

ingreseValores(*a)