# coding: utf-8
'''
9 - Escribir dos funciones que permitan calcular:
a) La duración en segundos de un intervalo dado en horas, minutos y segundos.
b) La duración en horas, minutos y segundos de un intervalo dado en segundos.
c) Utilizar valores por omisión para los argumentos.
'''


def duracion_segundos(horas = 0, minutos = 0, segundos = 0):
    return horas * 60 * 60 + minutos * 60 + segundos


def duracion_hms(seg):
    horas = seg / (60 * 60);
    minutos = (seg % (60 * 60)) / 60
    segundos = seg - horas * 60 * 60 - minutos * 60
    print [horas, minutos, segundos]


#print duracion_segundos(1, 2, 3)

def func1(*arg):
    suma = 0.0
    for num in [1,2,3,4]:
        suma = suma + num
    return suma/len(arg)

print (func1(1,2,3,4,5,6))



#print duracion_segundos(5,30)
#print duracion_segundos()