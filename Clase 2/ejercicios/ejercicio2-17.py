#coding=utf-8
'''
17​ ​ - ​ ​ Escribir​ ​ funciones​ ​ que​ ​ resuelvan​ ​ los​ ​ siguientes​ ​ problemas:
a)​ ​ Dado​ ​ un​ ​ año​ ​ indicar​ ​ si​ ​ es​ ​ bisiesto.
Nota:​ ​ un​ ​ año​ ​ es​ ​ bisiesto​ ​ si​ ​ es​ ​ un​ ​ número​ ​ divisible​ ​ por​ ​ 4,​ ​
pero​ ​ no​ ​ si​ ​ es​ ​ divisible​ ​ por 100, excepto​ ​ que​ ​ también​ ​ sea​ ​ divisible​ ​ por​ ​ 400.
b)​ ​ Dado​ ​ un​ ​ mes,​ ​ devolver​ ​ la​ ​ cantidad​ ​ de​ ​ días​ ​ correspondientes.
c)​ ​ Dada​ ​ una​ ​ fecha​ ​ (día,​ ​ mes,​ ​ año),​ ​ indicar​ ​ si​ ​ es​ ​ válida​ ​ o ​ ​ no.
d)​ ​ Dada​ ​ una​ ​ fecha,​ ​ indicar​ ​ los​ ​ días​ ​ que​ ​ faltan​ ​ hasta​ ​ fin​ ​ de​ ​ mes.
e)​ ​ Dada​ ​ una​ ​ fecha,​ ​ indicar​ ​ los​ ​ días​ ​ que​ ​ faltan​ ​ hasta​ ​ fin​ ​ de​ ​ año.
f)​ ​ Dada​ ​ una​ ​ fecha,​ ​ indicar​ ​ la​ ​ cantidad​ ​ de​ ​ días​ ​ transcurridos​ ​ en​ ​ ese​ ​ año​ ​ hasta​ ​ esa​ ​ fecha.
g)​ ​ Dadas​ ​ dos​ ​ fechas​ ​ (día1,​ ​ mes1,​ ​ año1,​ ​ día2,​ ​ mes2,​ ​ año2),​ ​ indicar​ ​ el​ ​ tiempo
transcurrido​ ​ entre​ ​ ambas,​ ​ en​ ​ años,​ ​ meses​ ​ y ​ ​ días.
'''

def isBisiesto(anio):
    #vamos a ver el año si es divisible segun las reglas
    #divisible por 4, divisible por 400 pero no divisible por 100
    #primero me fijo si es divisible por 4
    if(anio % 4==0):
        if(anio%100==0 and not anio%400==0):
            return False
        return True
    return False

print(isBisiesto(2000))
print(isBisiesto(2004))
print(isBisiesto(2005))
print(isBisiesto(2010))
print(isBisiesto(2014))

#b)​ ​ Dado​ ​ un​ ​ mes,​ ​ devolver​ ​ la​ ​ cantidad​ ​ de​ ​ días​ ​ correspondientes.

meses = ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio','agosto','septiembre','octubre','noviembre','diciembre']
dias = [31 , 28 , 31, 30,31,30,31,31,30,31,30,31]

def cantidad_de_dias(mes):
    return(dias[meses.index(mes)])

dias[1]
meses.index('febrero')

cantidad_de_dias('enero')
cantidad_de_dias('febrero')
cantidad_de_dias('octubre')
cantidad_de_dias('diciembre')

#c)​ ​ Dada​ ​ una​ ​ fecha​ ​ (día,​ ​ mes,​ ​ año),​ ​ indicar​ ​ si​ ​ es​ ​ válida​ ​ o ​ ​ no.

#si el mes es menor a 1 o mayor a 12 esta mal

def fecha(dia,mes,anio):
    if  not meses.__contains__(mes):
        print("mes no valido")
        return
    ultDiaMes = cantidad_de_dias(mes)
    if dia > ultDiaMes or dia<1:
        print("Ese mes no mes no tiene esa cantidad de dias")
        return
    if anio < 0:
        print("año incorrecto")
        return
    print("fecha valida capo")

fecha(31,"enero",1568)
