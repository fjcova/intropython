#coding=utf-8


import pandas as pd


class InstrumentPositions():

    def __init__(self, symbol):
        self.symbol = symbol
        self.positions = pd.DataFrame(columns=['price', 'size'])
        self.add_empty_row("bookedBuy")
        self.add_empty_row("bookedSell")
        self.add_empty_row("filledBuy")
        self.add_empty_row("filledSell")
        self.add_empty_row("initialBuy")
        self.add_empty_row("initialSell")

    def add_empty_row(self, name):
        row = pd.Series({'size': 0, 'price': 0}, name=name)
        self.positions = self.positions.append(row)

    def add_booked_position(self, side, size, price):
        if(side == 1):
            self.addPosition('bookedBuy', price, size)
        else:
            self.addPosition('bookedSell', price, size)

    def remove_booked_position(self, side, size, price):
        if(side == 1):
            self.removePosition("bookedBuy", price, size)
        else:
            self.removePosition('bookedSell', price, size)


    def add_filled_position(self, side, size, price):
        if(side == 1):
            self.addPosition('filledBuy', price, size)
        else:
            self.addPosition('filledSell', price, size)

    def add_initial_position(self, side, size):
        if (side == 1):
            self.addPosition('initialBuy', 0, size)
        else:
            self.addPosition('initialSell', 0, size)


    def addPosition(self, status, price, size):
        position = self.positions.loc[status]
        newSize = position['size'] + size
        newPrice = (position['size'] * position['price'] + size * price) / newSize
        position['size'] = newSize
        position['price'] = newPrice
        self.positions.loc[status] = position

    def removePosition(self, status, price, size):
        position = self.positions.loc[status]
        newSize = position['size'] - size
        if (newSize == 0):
            newPrice = 0
        else:
            newPrice = (position['size'] * position['price'] - size * price) / newSize
        position['size'] = newSize
        position['price'] = newPrice
        self.positions.loc[status] = position


    def get_consolidated(self):
        filledBuy = self.positions.loc['filledBuy']['size']
        filledSell = self.positions.loc['filledSell']['size']
        initialBuy = self.positions.loc['initialBuy']['size']
        initialSell = self.positions.loc['initialSell']['size']
        return (filledBuy + initialBuy)-(initialSell+filledSell)

    def has_stock_to_sell(self, size):
        consolidated = self.get_consolidated()
        available = consolidated - self.positions.loc["bookedSell"]['size'] - size
        return available >= 0


    def get_booked_buy_amount(self):
        booked = self.positions.loc['bookedBuy']
        return booked['size']*booked['price']




class PortfolioManagerStocks():

    def update_reference_price(self, symbol, new_price):
        self.reference_prices[symbol] = new_price


    def __init__(self, symbols, initial_cash, account, min_cash):
        self.symbols = symbols
        self.initial_cash = initial_cash
        self.account = account
        self.current_cash = initial_cash
        self.instrumentPositions = {}
        self.reference_prices = {}
        self.min_cash = min_cash
        for symbol in self.symbols:
            self.instrumentPositions[symbol] = InstrumentPositions(symbol)
            self.reference_prices[symbol] = 0

    def add_initial_position(self, side, size, symbol):
        self.instrumentPositions[symbol].add_initial_position(side, size)


    def add_filled_position(self, size, price,  symbol):
        side = 1
        if size < 0:
            side = -1
        self.instrumentPositions[symbol].remove_booked_position(side, size, price)
        self.instrumentPositions[symbol].add_filled_position(side, size, price)
        self.current_cash += size * price * side * -1

    def cancel_booked(self, size, price, symbol):
        side = 1
        if size < 0:
            side = -1
        self.instrumentPositions[symbol].remove_booked_position(side, size, price)

    def add_booked(self, size, price, symbol):
        side = 1
        if size < 0:
            side = -1
        self.instrumentPositions[symbol].add_booked_position(side, size, price)

    def calculate_portfolio_value(self):
        total = 0
        for symbol in self.symbols:
            total += (self.reference_prices[symbol] * self.instrumentPositions[symbol].get_consolidated())
        return total

    def has_stock_to_sell(self, size, symbol):
        return self.instrumentPositions[symbol].has_stock_to_sell(size)


    def has_cash_to_buy(self, size, price):
        money_needed = size*price
        for symbol in self.symbols:
            bookedBuy = self.instrumentPositions[symbol].get_booked_buy_amount()
            money_needed += bookedBuy
        return self.current_cash > money_needed + self.min_cash


    def add_symbol(self, symbol):
        if symbol not in self.symbols:
            self.symbols.append(symbol)
            self.instrumentPositions[symbol] = InstrumentPositions(symbol)
            self.reference_prices[symbol] = 0



if __name__ == "__main__":
    a = PortfolioManagerStocks(['GGAL', 'APBR'], 30000, "11015", 10000)
    a.update_reference_price('GGAL', 100)
    a.add_initial_position(1, 10, "GGAL")
    a.add_booked(5, 99, 'APBR')
    a.add_filled_position(5,99, 'APBR')
    value = a.calculate_portfolio_value()
    a.update_reference_price('APBR', 200)
    print(value)
    value = a.calculate_portfolio_value()
    print(value)
    print(a.has_stock_to_sell(5, 'GGAL'))
    print(a.has_cash_to_buy(10, 90))
    a.add_symbol('TS')