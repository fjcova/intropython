#coding=utf-8
import time

from MovingAverageCrossOver.medias_moviles import MediasMoviles
from portfolio.Portfolio import PortfolioManagerStocks
from websocketclient_v3 import WebSocketClient

urlLogin = "http://api.remarkets.primary.com.ar/auth/getToken"
urlWebSocket = "ws://api.remarkets.primary.com.ar/"

wsClient = WebSocketClient(urlLogin, "fzanuso", "pass", "11015", urlWebSocket)

symbols=['MERV - XMEV - GGAL - 48hs','MERV - XMEV - BMA - 48hs']
#symbols = ['RFX20Dic18', 'RFX20Sep18']
initial_cash = 30000
min_cash = 10000

portfolio = PortfolioManagerStocks(symbols, initial_cash,"11015", min_cash)


def actualizar_portfolio(message):
    if message["marketData"]["LA"]:
        last_price = message["marketData"]["LA"]["price"]
        symbol = message["instrumentId"]["symbol"]
        portfolio.update_reference_price(symbol, last_price)

def actualizar_posiciones(message):
    estado = message["orderReport"]['status']
    print(estado)
    if estado == "FILLED" or estado == "PARTIALLY_FILLED":
        portfolio.add_filled_position(message["orderReport"]["lastQty"], message["orderReport"]["lastPx"], message["orderReport"]["instrumentId"]["symbol"])
    elif estado == "CANCELLED" or estado == "REJECTED":
        portfolio.cancel_booked(message["orderReport"]["lastQty"], message["orderReport"]["lastPx"], message["orderReport"]["instrumentId"]["symbol"])
    elif estado == "ACCEPTED":
        portfolio.add_booked_position(message["orderReport"]["lastQty"], message["orderReport"]["lastPx"], message["orderReport"]["instrumentId"]["symbol"])

datos = [30, 31.1, 30.3, 32.4, 32, 32.05, 32.1, 32.2, 32.3, 32.3, 32.3, 32.4, 32.3, 32.2, 32.1]

mediasMoviles = {}
mediasMoviles["MERV - XMEV - GGAL - 48hs"] = MediasMoviles(datos)
mediasMoviles["MERV - XMEV - BMA - 48hs"] = MediasMoviles(datos)


def actualizar_medias(message):
    print (message)
    market_data = message["marketData"]
    if(market_data["LA"] !=  None):
        last_price = market_data["LA"]["price"]
        mediaMovil = mediasMoviles[message["instrumentId"]["symbol"]]
        resultado = mediaMovil.agregar_valor(last_price)
        print ("Media corta: {0}, Media larga: {1}".format(mediaMovil.media_corta, mediaMovil.media_larga))
        if resultado == "COMPRAR":
            print ("Reviso si puedo comprar " +  str(portfolio.has_cash_to_buy(1, last_price)))
            #enviar Orden compra

        elif resultado == "VENDER":
            #enviar orden venta
            print("Reviso si puedo vender" + str(portfolio.has_stock_to_sell(1, message["instrumentId"]["symbol"])))
            pass
        print (resultado)


wsClient.md_subscription(actualizar_medias, symbols, ['LA'])
wsClient.md_subscription(actualizar_portfolio, symbols, ['LA'])
wsClient.or_subscription(actualizar_posiciones, accounts=["11015"])
